---
title: State of the Art
period: # 03 October 2018 - 19 December 2018
date: 2019-01-24 12:00:00
term: 2
published: true
---
A collection of references and projects from different individuals and organisations related to the themes of interest for my masters project including artificial intelligence, machine learning, creativity and digital literacy.

## littleBits

[<u>littleBits</u>](https://littlebits.com/education/) is a system electronic building blocks inspired by the Lego block which make it really easy for teachers and students to engage in STEAM activities. Each block has a specific function (motion, light sensors) and snap together through magnets so no soldering is required, allowing circuits to be built really easily and intuitively. The blocks are modular so as students’ learn more the complexity of circuits can increase. Activities can also be formed around making using simple materials such as paper and cardboard.

![]({{site.baseurl}}/littlebits.jpeg)
<br>
<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/bFlKaf00zAk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Experiments with Google

[Experiments with Google](https://experiments.withgoogle.com/) is a platform with a library of experiments communicating ideas behind different technologies in an accessible manner, including AI. ‘[AI Experiments](https://experiments.withgoogle.com/collection/ai)’ showcase simple experiments that make it very easy for anyone to start exploring machine learning through pictures, drawings, music and more. ‘Teachable Machine’ is an experiment which teaches some concepts of machine learning in an in-browser environment using a webcam.

![]({{site.baseurl}}/teachable.jpg)

## BlockyTalky

[BlockyTalky](https://www.playfulcomputation.group/blockytalky.html): A Prototyping Toolkit for Digital Musical Interfaces is a programming environment designed to make it easy for beginner programmers to make interactive physical computing devices. It was developed as part of the Laboratory for playful computation (University of Colorado) which design playful and programmable technologies for learning.

![]({{site.baseurl}}/blocky.jpg)

## Dadamachines

The Automat from [Dadamachines](https://dadamachines.com/)  is a plug & play MIDI-controller and accessories kit that allows anyone to build music machines using the real world as their instrument. Custom made instruments would be connected to a computer via OSC so they could be controlled using gestures using machine learning using the Wekinator software.

![]({{site.baseurl}}/dada.jpg)

## Papier Machine

[Papier Machine](https://panoplie.co/) is a series of 6 DIY toys that are packaged within a book which cover different themes such as music which combine paper, electricity, art, fun and narrative. Circuits and objects are easily made through folding paper, conductive ink and a series of components that come with the kit such as piezos.

![]({{site.baseurl}}/pmachine.jpg)

## PlayBots

[PopBots](https://www.media.mit.edu/projects/pop-kit/overview/) is a project developed at MIT in the Personal Robotics Group by Randi William, Cynthia Breazeal and Hae Won Park. The project is aimed at a slightly younger audience than The Puerta Project but has influenced the project in terms of incorporating physical devices into activities for children to play and experiment with.

<!-- add image -->

## Cognimates

[Cognimates](http://cognimates.me/home/) is a platform built on the Scratch interface for building games, training AI models and programming robots. The project offers a set of extensions providing access to speech recognition, object recognition and robot control APIs, to build a range of AI generated projects. The project was developed by Stefania Druga whilst studying at MIT and continues the work that has supported many of the projects both historically and contemporary that have influenced my project.

## Machine Learning for Kids

[Machine Learning for Kids](https://machinelearningforkids.co.uk/) is a platform which has a website (machinelearningforkids.co.uk) and interface built on the Scratch platform (like Cognimates) where users can create and train their own machine learning models from a series of example projects. Labelled buckets can be filled with text, images numbers and sounds and after they have been filled the IBM Watson servers are used to generate a model for classification. Scratch is then incorporated through the Machine Learning for Kids extension where games can be created such as making a Tourist Information guide or playing Noughts and Crosses. The work of Dale Lane and Machine Learning for Kids has informed and inspired much of the development and work of my project.

<!-- include link for presentation etc -->

<!-- Magenta Project - how ml has a role in art and expressivity and how it can extend the human creative process. Demo - engage the community. Performance RNN. Generative models - where is this heading.

## Finland & Democratic AI

https://www.youtube.com/watch?v=4g6srBYeDag -->

  <!-- Koka Nicoladze, Moritz, Kelly Snook & Concordia, Google NSynth, Felix Machines, Rebecca Fiebrink, Trevor Paglen, Gene Kogan, Joana Moll, MTG UPF, face values LDB, Susan Rogers, https://www.youtube.com/watch?v=Dc8zNscLl6A, look at toby etc for all references add links to the different projects Really seriously listen to Jeff Mills music
add links to all of this
  -->
