---
title: Reading
period: # 02 October 2018 - 19 December 2018
date: 2019-01-24 12:00:00
term: 2
published: true
---
<!-- Put in pictures / quotes here -->

A collection of books, papers and publications which are informing the research of my final project:

* Machine learning as meta-instrument: Human-machine partnerships shaping expressive instrumental creation by Rebecca Fiebrink
* BlockyTalky: A Physical and Distributed Computer Music Toolkit for Kids by R. Benjamin Shapiro, Annie Kelly, Matthew Ahrens & Rebecca Fiebrink
* Machine Learning of Musical Gestures by Baptiste Caramiaux & Atau Tanaka
* Machine Learning within Ableton Live by Giuseppe Torre
* Real-time Human Interaction with Supervised Learning Algorithms for Music Composition and Performance by Rebecca Fiebrink
* The Machine Learning Algorithm as Creative Musical Tool by Rebecca Fiebrink & Baptiste Caramiaux
* Borsch Magazine edited by Jeff Mills
* Radical Technologies: The Design of Everyday Life by Adam Greenfield
* Mars by 1980: The Story of Electronic Music by David Stubbs
* The Creativity Code: How AI Is Learning to Write, Paint and Think by Marcus du Sautoy
* Inclusive AI literacy for kids around the world by Stefania Druga, Sarah T.Vu, Eesh Likhith, Tammy Qiu
* Getting Smart about the Future of AI - MIT Technology Review
* GROWING UP WI TH AI, Cognimates: from coding to teaching machines by Stefania Druga
* Kids’ Relationships and Learning with Social Robots by Jacqueline Kory Westlund
* PopBots: An early childhood AI Curriculum - MIT Media Lab
* PopBots: A Hands-on STEAM Platform for the AI Generation - MIT Media Lab
* Should Children form Emotional Bonds with Robots? - MIT Media Lab
* What Kids Need to Learn to Succeed in 2050 by Yuval Noah Harari
* Envisioning AI for K-12: What should every child know about AI? by David Touretzky, Christina Gardner-McCune, Fred Martin & Deborah Seehorn
* The Children's Machine: Rethinking School In The Age Of The Computer by Seymour Papert
* Mindstorms: Children, Computers, and Powerful Ideas by Seymour Papert
* The Society of Mind by Marvin Minsky
* The Vestigial Heart: A Novel of the Robot Age by Carme Torras
* How Smart Machines Think by Sean Gerrish
* Situating Constructionism by Seymour Papert and Idit Harel

<!-- include links!!! To the books

some of the other books...in onetab? write in proper harvard style, add articles as well-->
