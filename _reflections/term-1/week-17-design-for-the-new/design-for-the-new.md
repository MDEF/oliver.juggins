---
title: Design for the New
period: 26 April - 17 May 2019
date: 2019-06-02 12:00:00
term: 1
published: true
---
*Tutors: Markel Cormenzana & Mercè Rua*


Design for the New led by Markel Cormenzana and Mercè Rua of [<u>Holon</u>](http://www.holon.cat/en/) based in Barcelona was a course the third term which complemented the other course Design for 2050 in that its focus was about imagining and developing a future context for the [<u>main project</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/). I understand the work of Holon to be ‘transitional design’ which is driven by the notion of designing interventions that relate the present and future practices for a given project. More on this will be explained through the explanation and reflection on the different activities.

This documentation will be formed of two parts: one of the main activities, learnings and reflections throughout the course and the other will document more tangible intervention prototypes that I have created for my project and analyse the points of improvement for future iterations of the intervention.

## Mapping ‘Learning’

One of the exercises carried out in the seminar was to identify a social practice that is linked to the project and carry out a series of mappings. The idea of these mappings help to identify the ‘everydayness’ of the intervention. They analyse the ‘skills’, ‘stuff’ and ‘images’ (Shove & Pantzar, 2005). Skills refer to the “learned bodily and mental routines, including know-how, levels of competence and ways of feeling and doing” (Shove, Pantzar, & Watson, 2012). An important point to highlight about ‘skills’ is that appreciating different things and situations is considered part of the practice.

‘Stuff’ relates to the material elements that form part of the practice. These can be summarised as “objects, infrastructures, tools, hardware and the body itself” (Shove et al., 2012). ‘Images’ are less tangible than ‘stuff’ and refer to socially shared concepts related to the practice that give it meaning, reasons to engage with it or “the social and symbolic significance of participation at any one moment” (Shove et al., 2012).

In the context of the current practice this means children learning about AI. In the future practice however, I imagine the practice also to be learning, but the relationship of humans and machines to be more interdependent so the AI constantly learns about humans as we continue to learn about it.

<u>Present Mapping</u>

![]({{site.baseurl}}/social-mapping-present.jpg)

<u>Future Mapping (2050)</u>

![]({{site.baseurl}}/social-mapping-future.jpg)

<u>Practice Mapping - Discussion & Reflection</u>

The mappings are a useful tool to define where the main practice of the project is currently, and creating a collection of desired ‘skills’. ‘images’ and ‘stuff’ for the future. There are some repetitions of the practices in the future map, as there are some variables that I am projecting, or I would like to remain in the practice of learning in relation to The Puerta Project. For example a skill that appears in both mappings is ‘communicating ideas’ as I believe this will remain an important skill for children to be able to do in the year 2050.

A difference between the mappings that relates to the timeline that projected identified [<u>weak signals</u>](https://mdef.gitlab.io/oliver.juggins/reflections/atlas-of-weak-signals/) that relate to the project. Human-machine creative collaboration for example appears under ‘images’. Other notable differences appear in ‘stuff’ as imagining the means of human-machine communication is likely to be very different from today - the human body as an interface for example is one idea.

I found it really useful to see these differences as I had been struggling to imagine a future context for my project. This is due to the difficulty faced when projecting how an emergent technology like machine learning may develop because exponential technologies make it hard to project what they will be like in 3 years let alone 30. This exercise however made me realise that I don’t need to let this hold me back in imagining a future for the project, because I could be really specific around the selected social practice, and project how I would *like* learning to be in the year 2050.

The exercise also made me think about how artificial intelligence may change learning in the future. Conversations with teachers I have had about this topics have touched on the idea of personalised learning due to AI, and how it could be used as a tool for children to develop a learning process which is suited to their needs. As a result, I think that how the social practice of learning will change is a really important area of study for me moving forward and these exercises have been a really valuable step.


## Transitional Pathway

Despite the mappings serving a useful purpose of imagining how I would like ‘learning’ to function in the year 2050, they are two separate entities. A way to form a relationship and devise a route or make a transition can be done through creating a ‘transitional pathway’. This exercise maps requires the mapping out interventions, situations, or milestones which contribute to a piece of the journey between the two practices. The transitional pathway I created for the project aims to link the practice mappings and locates the interventions according to how desirable they are, with the interventions at the top if the map signifying the projected ‘best case’ scenarios (open image in new tab to see in more detail):

![]({{site.baseurl}}/transitional-pathway.jpg)

Making a route between the practices is useful to imagine tangible events and activities which can be done to try and reach the desired future state of the intervention. I found it particularly useful to imagine the next five years - some of the activities in the short term are things I have done or in the process of doing. Then thinking about where this action could lead to helps to make a bit of a plan for the project as it is easy to get caught up in day-to-day activities and not think about why you are doing things and the opportunities they may bring.

An example on the map is carrying out workshops for teachers at a Scratch meetup and then selling these workshops to CosmoCaixa. Teaching the teachers is a really useful activity in the short term, as it may provide the opportunity for further workshops in their schools and a chance to create a business. At the same time, this activity of doing a workshop for teachers would provide the opportunity to test and refine it with a group of individuals who are professionally trained with teaching children. This could then result in receiving feedback from teachers, and a higher quality product to try and sell to organisations such as CosmoCaixa.

## Intervention Prototype 1 - Documentation

This section consists of an intervention prototype and reflections from a workshop carried out for my project which formed the first intervention. For this intervention I produced a series of elements which formed the intervention including:

* Introductory presentation covering basic principles of artificial intelligence and machine learning.
* Lesson plan.
* Teacher guide.
* Student guide.
* Extra activity for students.
* Evaluation & Reflection.

Prototype Contents:

<u>Lesson Plan</u>

![]({{site.baseurl}}/lesson-plan.jpg)

<u>Presentation (snaphot)</u>

![]({{site.baseurl}}/presentation-football.jpg)

![]({{site.baseurl}}/presentation-ml.jpg)

<u>Student Guide</u>

![]({{site.baseurl}}/student-guide.png)

The activity which was planned for students was to create a Tourist Guide in the Scratch Application using the [<u>Machine Learning for Kids</u>](https://machinelearningforkids.co.uk/) platform.

The conditions of the intervention were as follows:

* Date: 26/03/2019
* School: Collegi Sant Andreu, Badalona
* No. of participants: 20
* Age of students: 13-14
* Duration of workshop: 2 hours 15 mins
* Workshop requirements: Internet connection, Ipad, ML4K login, ‘Standard’ API Key for IMB’s Watson Assistant. • Number of instructors: 2

The plan for the intervention took the following format:

The overall structure of the workshop was as follows (total duration: 2 hours):

<u>Introduction (15 mins)</u>

* Brief introduction to artificial intelligence and machine learning using presentation
with slides and objectives for the day’s session. (10 mins).
* Brief introduction to the activity of creating a Tourist Information guide. (5 mins).

<u>Main Activity (1 hour - 1.5 hours)</u>

* Reading instructions for the activity and starting session in Machine Learning for
Kids (10 mins).
* Testing project in Scratch 3.0 without training machine learning model (5-10 mins).
* Break (15 mins).
* Creating labels and training data (15 mins).
* Training the model and opening Scratch with ML4K plugin (10 mins).
* Creating the program in Scratch (5 - 10 mins).
* Testing the model and retraining if necessary. Trading Ipads between students to see
if there are differences in the models created by the class (20 - 30 mins).

<u>Reflection & Evaluation (15 mins)</u>

* Discussion with students about learning outcomes and comments about activity.
Asking them if their models worked or not, and why they might not have worked
very well (10 mins).
* Filling out evaluation form for feedback (5 mins).

![]({{site.baseurl}}/girls-ipad.png)

Students using the ipad to input data to their model.

Most of the students were able to create a Tourist Guide the experience was very positive. Part of what I want to achieve in the workshops is students not just learning about the technical side of machine learning but to also discuss associated ethical issues with this technology. During this exercise the students were very engaged and started asking more general questions about artificial intelligence which interested them. One student asked if it would be possible to program a robot that has feelings and the possibility of a superintelligent AI in the future. The theme of robots and jobs was an issue that seemed to interest the students which was echoed in the evaluation activity at the end of the workshop.

![]({{site.baseurl}}/label-data.png)

The labels and data that students created in the Machine Learning for Kids platform.

One of the ways which I was able to learn from the activity was to analyse the evaluation forms the students carried out. I could assess learnings and identify topics for future workshops. Here are the quantitative and qualitative results from the workshops in the form of pie charts and word clouds respectively. The colour and size of the words in the word clouds highlight the common key words in the written answers the students gave:

![]({{site.baseurl}}/evalution-results-quant.jpg)

![]({{site.baseurl}}/word-cloud-topic.jpg)

![]({{site.baseurl}}/evaluation-anything-else.jpg)

## Intervention Prototype 1 - Reflection & Learnings

The first step I took to analyse the intervention was to analyse the results of the data I gathered. Here I could identify if the students enjoyed the activity and topics for future workshops.

The next step was to carry out some personal reflection and talk to the people who I have been developing the workshop with from the Fab Lac Barcelona’s Future Learning Unit. We identified that the iPad was not the ideal tool as although the activity worked relatively well, more exciting projects would be possible using a laptop and it is easier to carry out the programming with a keyboard.

The student guide worked and was useful for the students to have a step-by-step, but this would be better in a digital format as it is wasteful to print out so much paper for a workshop which is not always reusable.

Another point of improvement is the activity itself. I was carrying out an activity which had been designed by someone else to test how Machine Learning for Kids works and if the children like it. The next step would be to create my own activity and to incorporate some additional skills such as electronics and physical computing to make the content more engaging and exciting.

I also think that there could have been more engagement with the ethical, societal and philosophical issues around artificial intelligence. This could feature in the introductory presentation but also reappear in the final reflection.

## Intervention Prototype (New Version)

The new version of the prototype consists of another introductory workshop to machine learning concepts but with some revised content and learnings from the previous experience. Having carried out some research and experiments the BBC Micro:Bit can be used with Scratch which means that the machine learning plugin can also be incorporated. I decided to combine the elements of the intervention into a single presentation so that students could follow the activity at their own pace and have access to content before and after the workshop if they want to learn about what they were doing or wanted to continue at home or in another class. The activity required students to use a laptop, and is another activity taken from the machine learning for kids platform, but remixed and altered by the addition of physical computing.

I am planning to carry out workshops for the final exhibition so this serves as a prototype for this purpose too. It also includes an interactive element that will feature in the exhibition which asks participants to write their definition of intelligence on a website. This will form part of the introduction with the idea of setting the scene and not presenting artificial intelligence as something unrelated to our own intelligence.
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR2emBL2YX1B1IdJQV6Yckz7iX8S_jRF1O1LDjXGLgh79si0gA8XSuh0ceP2kx9moD9DfzKg3ChWsmG/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="360" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Iterative Process

What I have realised about the workshops I am creating as a result of this activity is that I am undergoing an iterative process. I should try and always build on the results and feedback from the last. For my project I have been developing a series of workshops for different ages of children which cover different topics and themes relating to artificial intelligence and machine learning.

A reflection which has came about through this seminar is that this iterative process means that no two workshops should be the same, as even in Barcelona, children from different parts of the city will be different and have different levels of knowledge about the themes I am talking about. As such, something that I could do for future workshops which could improve the experience for the beneficiaries would be to ask them to carry out small questionnaire which would help me address there base knowledge and requirements and interests. Part of the narrative behind my project is that I am using design methodologies to enable machine learning driven problems to be realised in the physical world which children have designed, so learning about them beforehand could be incredibly useful and lead to more interesting outcomes.

## Final Reflection

The seminar overall really helped to not only develop the future scenario for my project but my future thinking skills in general. For my project it has helped me build a narrative around a character I have been developing for the project, which tells the story of how a child learning with AI in a workshop then develops and how their learning process will change as a result of artificial intelligence. The projection mapping exercises and transitional pathway have also helped with an interactive timeline about my project which will feature in the end of year exhibition.

I feel that the exercises in design fictions in general throughout the masters have been really enjoyable, but the connection between the present and future was really made clear in this seminar to me. I was able to really recognise and orchestrate this relationship in a way through the transitional pathway exercise. Being able to make this connection really helped me to imagine a future for my project and is an exercise and tool I can use to make thoughtful visions for future projects beyond the masters.

## References

* Shove, E., & Pantzar, M. (2005). Consumers, Producers and Practices. Journal of Consumer Culture, Vol. 5, pp. 43–64. https://doi. org/10.1177/1469540505049846
* Shove, E., Pantzar, M., & Watson, M. (2012). The Dynamics of Social Practice: Everyday Life and How it Changes. https://doi. org/10.4135/9781446250655
