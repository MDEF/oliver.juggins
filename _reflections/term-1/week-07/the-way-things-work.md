---
title: The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---
*Tutors: Guillem Camprodon, Victor Barberan & Oscar Gonzalez*

The Way Things Work was a highly enjoyable week of prototyping and delving into the world of electronics and networks which culminated in a final project which combined the skills and knowledge gained during the week. The course was split into classes which explained the theory of a particular topic, and then working in groups we were able to prototype in practical sessions under the guidance and help of the tutors. A lot of the material was new to me, and cleared up a lot of the uncertainty I had about some of the terminology I had heard come across but not fully understood.

### Week Summary

The documentation will be formed of the process describing the final group project, but before I go into that here is a brief outline of some of the different activities and learnings from the week:

* Hacking an everyday objects, performed ‘teardown’ of an inkjet printer and learning its different components, sensors and motors. Discovered how to research pcbs using resources online and process of reverse engineering a circuit to find out how things are connected.
* Digital vs analog information, key differences and properties. Digitizing information and understanding digital and analog read / write through some simple Arduino exercises.
* Understanding programming flow, the binary nature of computers, if statements, decisions and sketching logics.
* Sensors interfacing the real world, introduction to sensors through real-world examples and some Arduino exercises.
* Outputs and actuators, experimentation with simple circuits and Arduino.
* Information theory, history of internet including internet protocols, exchange points, bits and resolution. Learnt about network communications through setting up network and server in classroom using Raspberry Pis (MQTT).
* Linux and the notion of ‘copy -left’, open source and intellectual property.
* ESP8266 board introduction and experimentation, connecting to the class network.
* Connecting the inputs and outputs of each of the class projects over the network with ESP boards using Node-Red.

![]({{site.baseurl}}/break10.png)

### Input or Output?

The main activity of the week consisted of working in a group and developing a project involving either and input or an output and putting some of the theory from the classes into practice. I was in a group with Emily, Jess, Fifa and Ilja and early on we decided on working with plants and began experimenting with different sensors acting as an input being used with an Arduino Uno board. We began by using a moisture sensor to measure the water content in the soil of our plant and made simple setup adapting example code and projects we researched online. As a next step we adapted the setup to work with an RGB LED to have a visual cue of whether our plant needed watering using figures found online as a guideline and read the values on the serial monitor to make sure it was working correctly.

![]({{site.baseurl}}/moisture.jpg)

![]({{site.baseurl}}/break10.png)

### Capacitance

As the week progressed we decided to explore another input to measure the capacitance of our plant. We found that the moisture sensor worked well and that it would be interesting to be able to use the plant with multiple inputs, and that the plant itself could provide a nice interface to interact with. Reaching the point of measuring the plants capacitance proved to be a lot more difficult than measuring the moisture content of the soil. Capacitance as we learned is a lot more sensitive and even with the vast amount of resources online it was a lengthy process to get the setup working correctly even though the circuit was a lot simpler. The main issue that arose with sensing the plants capacitance was the resistor values, and through a process of trial and error found that values of both 10M and 750K Ohms worked depending on the board and computer used.

![]({{site.baseurl}}/cap.jpg)

![]({{site.baseurl}}/break10.png)

### ESP WiFi Module

A goal of the group project was to be able to connect all of the inputs and outputs together over the network which was created in the classroom. As the Arduino Uno board does not have a WIFI chip built-in, we used a similar board with WIFI capabilities called the ESP8266. This board can be used within the Arduino IDE with the correct libraries installed and uses the same coding language. In theory this meant it was quite straightforward to get started with the ESP boards,. We found out that although these boards are extremely powerful for the price, they can be a lot more sensitive and required a lot more attention especially regarding capacitance.

There was considerably more trial and error with these boards, changing the resistors, rewiring the circuit, changing usb cables and head scratching in order to get the plant to successfully respond to someone touching it. Another issue which required more attention was the remapping of the values of the capacitance between 0-255 (so that our plant could communicate with the projects involving outputs in the class over the network). This  was done through analysing the values given on the serial monitor and trying different resistors to achieve a change in values great enough to be felt. Another important lesson from using the ESP board was that its pins do not correspond to the pin values in the Arduino IDE. This took some time to work out, but luckily the online community for the Arduino and ESP boards is vast so it was easy to find the correct values for the pins.

Through using a WiFi enabled board we were able to record the values and keep a log of the plants moisture and capacitance values. Ilja setup a [<u>dashboard</u>](https://wetmyleaf.github.io/) dedicated to these values, taking measurements every 5 seconds with a graph to show the change over time. The main reason however of using these boards was to be able to connect to the network and send our data to the server and communicate with the other projects in the class. This part of the process was a lot simpler than getting the capacitance to work and was made straightforward through a workshop in class.

![]({{site.baseurl}}/site.jpg)

![]({{site.baseurl}}/break10.png)

### Debugging

Although the issues we ran into with capacitance were frustrating, I was able to learn a huge amount from Guillermo,Victor and Oscar about the debugging process which was probably the most valuable experience of the week for me. I discovered that even with relatively simple code and circuits, there are still many opportunities for error messages to appear and general confusion to ensue. I have tried to distill the main phases of the debugging process which related to this project but can be applied to others in the future for my own reference.

* Check the correct board and port are selected in the Arduino IDE.
* Run example code to make sure the board is working (blink test for example).
* Run example code for the sensor being used.
* Check that the circuit is correctly connected and grounded.
* Change usb cable used (especially in case of ESP boards).
* If ESP board not responding, check code works on Arduino.
* Check correct pins are being used (especially for ESP board).
* Change resistors used, and check their values with multimeter.
* Change breadboard, cables, components one by one.
* Go to reference section and forums for particular component or project on Arduino website for information and guidance.

![]({{site.baseurl}}/break10.png)

### Inputs & Outputs

If a plant is touched on one side of room 201 does it cause a balloon to explode on the other? If they are connected over the same network and Node-Red is used to connect the output (inflating balloon) and the input (plants capacitance) then the answer is yes. Once every group had connected their input or output device to the network the aim was to be able to trigger the different devices. We decided that capacitance would be a more interesting input to use than the soil moisture content as our input as it provided the opportunity for physical interaction and more of a direct response that could be repeated multiple times easily. It was a great end to the week to see the projects all link together and made the issues that we overcame regarding the ESP boards worth it.

![]({{site.baseurl}}/break10.png)

### Digitial Literacy

The week was not only an opportunity to learn more about physical computing, sensors and networks but a chance to experience first-hand some of the themes I am interested in during the masters. I am interested in the relationship between humans and machines and how we interact with technology. The accessibility and resources available to start experimenting and exploring the world of electronics and coding has changed the way I understand machines and interact with technology even in the short time I have been using devices such as the Arduino and ESP board. The Arduino has developed my own technological literacy and can help me understand how others can interact with electronics and the learning process of confronting new technologies.

Coding and electronics is often presented as something which is easy and fun, which although might engage people initially with addressing their own digital literacy but might actually be an unhelpful position. Walter Vannini argues this point in an [essay](https://aeon.co/ideas/coding-is-not-fun-it-s-technically-and-ethically-complex) arguing that it is much better to present coding as something ethically and technically complicated. I think this is a really important point, especially given what I learnt last week during [Design with Extended Intelligence](https://mdef.gitlab.io/oliver.juggins/reflections/designing-with-extended-intelligence/). Humans and machines interpret information in totally different ways and it is important to understand this when entering the world of computer science. As Vannini puts it ‘the machine does what you say, not what you mean’. I have had a lot of fun using the Arduino, but I wouldn’t say it has been easy. It has been rewarding and satisfying to learn new skills and understand some of the logic which computers operate under, but am only just beginning this journey and have a lot more to learn about the way things work and appreciate that it takes time to develop skills in this area.

As I move forward with developing my project and ideas relating to digital literacy and the human - machine relationship I want to keep in mind the context in which I am working and how communities can be involved. The [Making Sense](http://making-sense.eu/) project is a great example of how a community can learn about new technologies and positively transform a neighbourhood and serves a good way for me to think about how people interact with machines on a wider scale. The tutors this week are also involved in the [Smart Citizen](https://smartcitizen.me/) project which connects some of the themes in the week such as the significant role of open source in the development and deployment of these projects and is something I will keep in mind when developing my final project.

![]({{site.baseurl}}/break10.png)

### Reflection

I learnt that great things can be done with just a few jumper wires, affordable microcontrollers and electrical components and the week demonstrated what can be achieved when working quickly using the available equipment to its limit. At the beginning of the week I had no conception of how we would create a network in the classroom, or what this really meant and now can have an understanding of not only how it was created but the theory behind it. I feel a lot more confident using the Arduino and ESP boards now as well which will help with my own projects and the Fab Academy. I also have a greater appreciation for the kinds of projects they can be used for and how I can address the gaps in my knowledge using the resources available online and in IaaC in terms of classmates and tutors. I also feel a lot more confident when things don’t go to plan and that having a debugging process and plan can be really helpful.

The week also changed my perception of how to we use electrical items through the activity of dismantling a printer. I thought there was a parallel with [Design for the Real Digital World](https://mdef.gitlab.io/oliver.juggins/reflections/design-for-the-real-digital-world/) in the way it showed me that found electrical items can also have a second life and the reuse mindset can and should be applied with the right skills and knowledge.

![]({{site.baseurl}}/led.jpg)

![]({{site.baseurl}}/break10.png)

### References

* [The Way Things Work](https://hackmd.io/ys7lcCDJSeq69meSeGnIXg?view) course guide and resources
* [Plant moisture & capacitance readings](https://wetmyleaf.github.io/)
* [Arduino Reference](https://www.arduino.cc/reference/en/)
* [ESP Reference](https://hackmd.io/ihNOXMFGQTOTLYwlz8_Y4w?view#)
* Final capacitance code [here](https://github.com/iljapanic/planty/blob/master/planty-local-2-sensors/planty-local-2-sensors.ino)
* [Coding is not ‘fun’, it’s technically and ethically complex](https://aeon.co/ideas/coding-is-not-fun-it-s-technically-and-ethically-complex) by Walter Vannini
