---
title: Material Driven Design
period: 24 January - 21st February 2019
date: 2019-01-30 12:00:00
term: 1
published: true
---
![]({{site.baseurl}}/finalselectionabove.jpg)

This page is dedicated to the documentation of the [<u>Material Driven Design</u>](https://mdef.gitlab.io/landing/seminar2/03-Material%20Driven%20Design.html) course led by [<u>Mette Bak Anderson</u>](https://twitter.com/mettebakand?lang=da) from the [<u>Material Design Lab KEA</u>](http://materialdesignlab.dk/) and designer [<u>Thomas Duggan</u>](http://thomasdugganstudio.com/). The brief for the project was to develop a prototype for a product that would be 100% biodegradable from a biomass waste product. The project was short and intense and framed within the context of designing for a circular economy and challenged traditional design methodologies through presenting an approach where a 'dialogue' is formed with the material in order to develop its form, rather than treating material choice as an 'add on' after the design phase. Both a phenomenological and scientific approach were encouraged as a way to learn about the material and ultimately deliver a final product.

Initially I opted to work with leaves as my material for the project, but had to change material after the first week which will be explained following my initial research and project ideas relating to leaves and sound. The remainder of the documentation consists of the process I went through to develop my material and final product. I end with some final reflections which discuss how I can relate the learnings from the Material Driven Design course to my final project and myself as a designer more generally.

## Leaves & Sound

![]({{site.baseurl}}/leaf1.jpg)

As well as being a readily available source of material on the street, my reason for choosing leaves as a material to work with links to my main project for the masters due to the relation to sound. Trees in urban environments are often used as acoustic barriers to reduce noise pollution in cities through the absorption, diversion reflection and refraction of sound. The sound leaves make themselves is also significant, whether its the crunch of stepping on leaves or the rustling of leaves in a tree. The sound leaves make can also give information about the environmental conditions, and the presence of leaves on the ground tells you what time of year it is.
<br>
<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/qVFasUmGztw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Material Source

The source of the raw material I am using are leaves from the street, but ultimately the source are the trees in Barcelona. The leaves I have collected are seasonal and is only available at certain times of the year, so collection is limited within a specific timeframe. The diagram below shows a trees basic physiology and the function of the leaves. Transpiration and evapotranspiration take place in the leaves, and are responsible for the production of organic matter.

{% figure caption: "*Source: Street Tree Management in Barcelona*" %}
![]({{site.baseurl}}/tree.jpg)
{% endfigure %}

I am currently unsure of the species of tree that the leaves I have collected are from. It is most likely a mixture as I have collected leaves from some different areas of the city so far, but most are probably from a Plane tree, a large deciduous tree from the Platanus genus.

{% figure caption: "*Source: Street Tree Management in Barcelona*" %}
![]({{site.baseurl}}/treetypes.jpg)
{% endfigure %}

## Technical Information

Leaves are the primary sites for photosynthesis, meaning that they provide food for plants. They typically have a broad blade known as the lamina which attaches to the stem. A leaf's veins support the lamina which transport material to and from the leaf tissue, and the type of venation (arrangement of veins) is characteristic of different plant types for example. Chlorophyll is the substance that gives plants their green colour and absorbs light energy and its internal structure is protected by the leaf epidermis, a continuation of the stem epidermis. Chlorophyll is the molecule that uses sunlight energy to turn water (H2O) and carbon dioxide gas (CO2) into sugar and oxygen gas (O2).

Chlorophyll production slows down in the autumn as the days get cooler and shorter resulting in the fading of the green pigments (chlorophylls) revealing the other pigments present in the leaf. These include xanthophyll (pale yellow), carotene (yellow), anthocyanin (red if the sap is slightly acidic, bluish if it is slightly alkaline, with intermediate shades between), and betacyanin (red). Tannins give oak leaves their dull brown colour. The fall of leaves happens due to the weakening of the abscission layer at the base of the petiole which happens due to lower light levels in the autumn. A zone of cells across the petiole becomes softened until the leaf falls and a healing layer then forms on the stem and closes the wound, leaving the leaf scar.

The leaves I have collected are palmately lobed as they are divided into leaflets that come from a central point, radiating from the end of the petiole. The leaf edge, or margin is parted (the margin has an indention or indentions that go more than halfway to the leaf midline) and the venation in the leaves I collected mostly exhibit pinnate venation (the veins extend from the midrib to the leaf margin).

![]({{site.baseurl}}/leaf.jpg)

## Initial Project Ideas

I am keen to link the experiments with leaves to my final project in the masters so will be driven by exploring the sonic qualities of leaves, and using sound as a way to potentially shape the material itself. I would like to record how sound passes through leaves when in different states, observing how sound energy might change the state of the leaves and investigate if sound could inform the design and form of the material. I would also like to take a series of field recordings of leaves and record the sounds produce as I experiment with the material as it changes state, or is in the process of changing state which could then be replayed through the material and to visualise these findings in some way. My main project is also looking at how machine learning can be applied to the creation of sound and music, so beyond the initial experiments I would like to look into if the data collect on the sound collected by leaves could be put into a machine learning model.

An initial reference relating to material and sound I came across is Tessa Spierings' tactile sound system which explores the acoustic properties of different materials. The project is described as a 'soundscape that makes acoustics visible and tactile' and the system combines custom made speakers with removable tubes which are made of different materials to alter the property of the music played. Some of the materials used to make the tubes to carry the sound include ash wood, fabric-covered foam and brass.

Spierings explains 'every material has its own characteristic (like density), which lets the sound resonate through the tube in a different way', exploring how will sound resonate through leaves could be a good starting point.

<br>

<iframe width="640" height="360" src="https://www.youtube.com/embed/EEGZPI2A_2I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Machine Learning Leaves

In researching the history of leaves I came across a link between leaf identification and machine learning. The complexity and variety of vein and shape patterns make the task of identification an extremely difficult task, and results in gaps in knowledge regarding the evolutionary history of green life. To address these gaps, researchers have turned to computer science to decode leaf information. Thomas Serre and other researchers have used machine learning to train a computer to teach itself to identify leaves, addressing the hidden evolutionary history of many plants. Applying machine learning and computer vision techniques to the identification of fossilised leaves can open up this hidden history and ultimately change how scientific research is carried out.

{% figure caption: "*Source: Science Node*" %}
![]({{site.baseurl}}/fossil.jpg)
{% endfigure %}

## References

* [Street Tree Management in Barcelona](https://climate-adapt.eea.europa.eu/metadata/case-studies/barcelona-trees-tempering-the-mediterranean-city-climate/11302624.pdf) - Ajuntament de Barcelona
* [Plant leaf anatomy](https://www.britannica.com/science/leaf-plant-anatomy)
* [The Invisible History of Leaves](https://sciencenode.org/feature/seeing-the-invisible-history-of-leaves.php)
* [Tactile Sound System](https://www.dezeen.com/2019/01/26/tessa-spierings-tactile-sound-system-design/)

## Change of Direction

After presenting the choice of leaves as a material to work with for the project, it turned out to be an unsuitable material to work with. This is due to the lack of moisture in the leaves which would make it hard for them to bind together, as the idea was to use as much of the primary material as possible in the final product. As a result I decided to use the spent grain waste which is what is left over from the brewing process for beer as this was a material which was easy to get hold of from local breweries in the surrounding area which some other members of the class had sourced.

Brewers spent grain is rich in carbohydrates, lignin, proteins and lipids. The spent grain seemed like a much more promising material to work with than leaves due to these properties and the fact that the grain itself would be able to hold liquid and could provide a lot structural integrity which would lead to more interesting results.

It also was a good material for the project as it related to the brief for the project quite strongly in that it is a waste material from industry which is created in vast amounts all over the world. [<u>Gabor</u>](https://mdef.gitlab.io/gabor.mandoki/materialdesign/) had originally found a source for the material at [<u>Edge Brewing</u>](http://edgebrewing.com/) which was very close to the university which made transporting the grain convenient.

{% figure caption: "*Collecting grain from Edge Brewing*" %}
![]({{site.baseurl}}/1edge1.jpg)
{% endfigure %}

I visited a number of other breweries to try and source different types of grain. I visited [<u>BrewDog</u>](https://www.brewdog.com/) and although I couldn't use any of their waste due their breweries being located in Scotland I gained an interesting insight. I learnt that this is a brewery that has already adopted a circular approach to their waste material through giving half of the material to farmers as feed for animals (in return they receive meat for their restaurants) and the other half into the production of vodka, rum and gin which they serve in their bars.

With the other members of the class using spent grain for the project I visited [<u>Birra 08</u>](https://birra08.com/) to collect more grain and yeast and learn about the brewing process in more detail. We learnt that the brewery creates 300kg of waste per week and that the yeast produced can be reused for making more beer or thrown away.

{% figure caption: "*Inside the Birra 08 microbrewery*" %}
![]({{site.baseurl}}/2birra08.jpg)
{% endfigure %}

## Process (Approach)

Once I had enough material it was time to start working with the grain and try and learn as much about it as quickly as possible. I initially started working in quite an experimental manner, adopting a 'phenomenological' approach, using touch as the primary sense to gain information. I felt that from an early stage I could react to the material this way quite well and knew when I needed to add more of a certain ingredient to change the consistency of the mass I was working with. I made a decision to start working in 3D from the start of the project and had some interesting outcomes from the offset.

Having said that I soon adopted a more methodological and scientific approach as I realised that through not keeping a more rigorous track of exactly what I was doing I wouldn't be able to replicate what I had done or make improvements. Finding this balance took some time to get used to as it was very easy to get carried away and start making and experimenting without keeping a proper record.

I worked on the project between the Fab Lab and the my kitchen at home and each space had its advantages. In the Fab Lab I had access to more equipment and space but had less control over certain things such as the oven temperature. At home I could control the oven temperature a lot more accurately, but was at times limited by space and was conscious of trying to keep the kitchen in a usable state for my housemates, who were thankfully very understanding and interested in the project. As a result over the course of the project I developed a way of splitting tasks between the two spaces.

## Process (Making)

Before I could begin to work with the grain I collected from the brewery there was a lengthy drying out process as the grain still contained a lot of moisture. We put some grain on the rooftop which took a couple of days to dry out, but to speed up the process put some in the oven at between 50-100 degrees centigrade (depending on the space I was drying the grain out). This took about 4 hours, but learnt that at home I could speed up this process by placing the tray on the bottom of the oven so the grain received a more intense heat from below.

I will outline the exact recipes of the different samples and some brief reflections on what I took into the next experiment, but here are the general steps I took when working with the grain:

* Dry out the grain in the oven.
* Grind the grain to desired density using available tools.
* Add liquid to grain (water or yeast) and mix together.
* Add binder (I used cornstarch).
* Mix together until consistency is slightly sticky.
* Remove excess moisture from mixture.
* Using available mold and place mixture around mold.
* If mixture doesn't stay around mold, return to mixing process.
* Once mixture and mold is ready, place in oven.
* Leave the mold in oven until mixture has dried enough to hold its shape out of the mold.
* Place the formed mixture back in the oven so it can fully dry out, rotating when necessary so it receives heat from all sides.
* Once fully dried remove from oven.

When choosing the temperature of the oven, lower temperatures and longer time is preferred to avoid cracking. Depending on size and thickness of forms, the oven the time in the oven varies but to fully dry the samples usually took 4-5 hours at 50 degrees centigrade. Another important point is knowing when to remove the mixture from the mold to dry out properly. This can only be done through carrying out the process a couple of times and getting a feel for when the structure is strong enough as a couple of my samples collapsed through not letting them form for enough time around the molds.

I used a number of methods for creating my samples experimenting with different types of mold and mixtures. Here are some photographs which outline some of the different tools and techniques I adopted over the course of the project:

{% figure caption: "*Spreading out the grain thinly on the oven tray for drying process*" %}
![]({{site.baseurl}}/ovendry.jpg)
{% endfigure %}

{% figure caption: "*The addition of a blender to the Fab Lab saved a lot of time in the grinding process*" %}
![]({{site.baseurl}}/blender.jpg)
{% endfigure %}

{% figure caption: "*Adapting the hand blender at home to keep a clean workspace*" %}
![]({{site.baseurl}}/tooling.jpg)
{% endfigure %}

{% figure caption: "*Coffee grinder used to make the grain smaller. This was a long process but made the grain into a nice density for working with*" %}
![]({{site.baseurl}}/coffeegrinder.jpg)
{% endfigure %}

{% figure caption: "*Grinding the grain into a powder. This took a very long time to just make a small amount of powder*" %}
![]({{site.baseurl}}/grinder.jpg)
{% endfigure %}

{% figure caption: "*Sieving the ground grain into a powder*" %}
![]({{site.baseurl}}/sieve.jpg)
{% endfigure %}

{% figure caption: "*Powder left over from sieve*" %}
![]({{site.baseurl}}/sieve2.jpg)
{% endfigure %}

{% figure caption: "*Using a plastic container as mold for first test*" %}
![]({{site.baseurl}}/3firsttest.jpg)
{% endfigure %}

{% figure caption: "*Taking mold out of mixture*" %}
![]({{site.baseurl}}/4firsttest2.jpg)
{% endfigure %}

{% figure caption: "*Desired consistency of mixture: holds is shape without cracking*" %}
![]({{site.baseurl}}/dmass.jpg)
{% endfigure %}

{% figure caption: "*Using off-cut pieces of wood from Fab Lab for tests*" %}
![]({{site.baseurl}}/woodmold1.jpg)
{% endfigure %}

{% figure caption: "*Dried mold after being in the oven for 3 hours*" %}
![]({{site.baseurl}}/woodmold2.jpg)
{% endfigure %}

{% figure caption: "*Inspired by Peter Zumthor's [<u>Bruder Klaus Field Chapel</u>](https://www.archdaily.com/106352/BRUDER-KLAUS-FIELD-CHAPEL-PETER-ZUMTHOR) I tried to burn the material out of the mold, but was unsuccessful*" %}
![]({{site.baseurl}}/woodmold3.jpg)
{% endfigure %}

{% figure caption: "*Using 3D printed mold*" %}
![]({{site.baseurl}}/moldfablab.jpg)
{% endfigure %}

{% figure caption: "*Pressing the mixture into shape with the mold*" %}
![]({{site.baseurl}}/moldinsue.jpg)
{% endfigure %}

{% figure caption: "*Removing the inner mold leaving desired shape*" %}
![]({{site.baseurl}}/moldinuse2.jpg)
{% endfigure %}

## Recipes

I kept a record of my findings throughout the project and the different quantities and the process I went through for the different experiments. The image below serves as an overview for the different experiments together:

![]({{site.baseurl}}/overallheadon.jpg)

<u>Experiment #1</u>

*Ingredients:*
Spent grain, cornstarch
Note: Exact quantities unknown (experimental approach adopted)

*Method:*
* Using dried grain, ground using hand coffee grinder (5 passes to get as small as possible)
* Combine mix of some different grain sizes in bowl and water and mix.
* Add cornstarch and mix until consistency is similar to bread dough, adding more of each ingredient until the mixture doesn’t fall apart in your hands.
* Pour olive oil around mould and sculpt mixture around mould.
* Put in oven around mould for 40 mins at 50 degrees centigrade.
* When bowl looks like drying has begun on the outside, remove from oven and take out mould carefully.
* Place bowl back in oven and leave until fully dry (around 3-4 hours)

*Reflection:*
Shape holds well, and over time has held together well, but quantities should be known to be able to reduce level of cornstarch in product. Muld works well, but need to develop a better way of removing structure from mould to avoid any damage.

*Result:*

![]({{site.baseurl}}/recipe1.jpg)

<u>Experiment #2</u>

*Ingredients:*
Spent grain (15 tbsp), yeast (15tbsp), cornstarch (9 tbsp) ~ 5 parts grain to 5 parts yeast to 3 parts cornstarch

*Method:*
* Using coffee grinder, grind grain as small as possible.
* Mix yeast and grain first, adding enough of each so that the mixture holds it shape as much as possible.
* Then add in cornstarch until mixture holds shape when molded around a mold.
* Use olive oil around molds and place mixture around molds and put in oven at 50 degrees centigrade.
* Keep in oven between 30 minutes - 1 hour before taking of mold.
* Place back in oven until form is fully dried out.

*Reflection:*
Some of the shapes hold well, but generally fell apart. Mixture not suitable yet for complex shapes. Wood also not ideal mold as mixture sticks to wood more when moist.

*Result:*

![]({{site.baseurl}}/recipe2.jpg)

<u>Experiment #3</u>

*Ingredients:*
Spent grain (6 parts), yeast (3 parts), cornstarch (1 part)

*Method:*
* Grind spent grain into powder, using sieve to obtain fine powder.
* Combine powder and yeast and mix.
* Add cornstarch until mixture holds together well in a ball.

*Reflection:*
The finish is nicer than previous experiments, but sample is much too brittle. Larger grain is needed for the structure to bind together more successfully. A little more cornstarch is also needed. Try different molds e.g. silicone as it is difficult successfully removing mixture from mould in drying process because it is so fragile. Time to make powder is also too great, need electric grinder to make the process feasible on larger scale. Try to reduce liquid content also.

*Result:*

![]({{site.baseurl}}/recipe3.jpg)

<u>Experiment #4</u>

*Ingredients:*
Spent grain (3 parts), yeast (2 parts), cornstarch (1 parts), Glycerol (1 tsp), vinegar (1 tsp)
Note: 1 tsp = 10¼ ‘part’
Note: Spent grain made up of different densities (2 parts ‘powder’, 8 parts ‘fine grain’ (blended) 3 parts ‘large grain’).

*Method:*
* Grind spent grain into powder, using sieve to obtain fine powder.
* Use different grinders and blenders to create different densities of grain.
* Combine different densities of grain and yeast and then mix.
* Add cornstarch until mixture holds together well in a ball.
* Use sieve as mold so moisture can escape when in oven.
* Place in oven for a couple of hours and remove when fully dry.

*Reflection:*
Different densities working better, but mixture is still not right. Need to use more larger grains for structural integrity. When thicker, structure holds well, but when thin crumbles apart.

*Result:*

![]({{site.baseurl}}/recipe4.jpg)

<u>Experiment #5</u>

*Ingredients:*
Spent grain (10 parts), cornstarch (2 parts), yeast (16 parts), glycerol (2 tsp)

*Method:*
* Heat up yeast in pan and add large grain.
* Boil off the yeast until consistency is thick and starts to stick to pan.
* Blend mixture using hand blender.
* Add in cornstarch and mix well.
* Sprinkle medium density grain into mixture until desired consistency is reached.
* Form around mould and place in 50 degree centigrade oven.
* Once mixture is dried on exterior of mould, remove mould and put back in over until fully dried out.

*Reflection:*
Applying heat to the mixing process makes the mixture combine a lot more successfully and less cornstarch can be added. Mold worked well in most cases, most important thing is to leave in oven as long as possible before taking mixture out of mold. Glass mold not suited to industrial process, but paper makes maixture easier to remove from mold. Contrast of exterior / interior forms left over from this technique is nice though.

![]({{site.baseurl}}/finalselection.jpg)

{% figure caption: "*Removing the mold when the mixture wasn't dried properly*" %}
![]({{site.baseurl}}/broken.jpg)
{% endfigure %}

## Final Product

The final product followed the same recipe as experiment #5 but used with the 3D printed mold. It worked well as I made sure to wait enough time before taking the mold off in the drying process. Removing the mold was a two step process, removing the interior part, putting back in the oven and then the out section with a final couple of hours in the oven to fully dry out.

![]({{site.baseurl}}/finalwithprint.jpg)

![]({{site.baseurl}}/final.jpg)

{% figure caption: "*Densities of grain used in the final mixture*" %}
![]({{site.baseurl}}/densitiesfinal.jpg)
{% endfigure %}

## Industrial process

As part of the project was to think about the material and process in an industrial context, I gave some thought to what would be required to use the material on a larger scale. Due to the fact that the material is already produced in a brewery, the breweries themselves could become a location for manufacture within the same premises which would save transportation time and cost.

The material would then have to be dried and ground in large quantities which would be easily achievable in a factory setting and then mixed in large vats of grain, yeast and binder. Silicon molds would then have to be developed for the creation of different objects and packaging and then would have to be dried out in larger ovens until they were ready and could be easily removed without breaking.

## Final Presentation Feedback & Next Steps

![]({{site.baseurl}}/overallangle.jpg)

During the final presentation the feedback I received was based around pushing my experimentation further in trying to reduce the amount of binder even more, trying some other binders to see what happens and to try some more complex shapes as a mold. Mette suggested that it could be a suitable material as packaging for eggs which I thought was an interesting idea, as the final product's main qualities were that it was light and relatively strong. A lot more experimentation would be required to see how thin I could get the material, and if it would be possible to achieve such a complex shape.

Although I am happy with the final result, I feel I still have some way to go before I am fully in control of the material so would have to continue my experiments to build up more confidence to make more complicated forms. I would continue down the path of trying to exploit the light and strong qualities of my final product and think about further applications that might be suitable.

I think the idea of packaging is a good one however as the possible positive environmental impact could be really large. I also think it would be worth testing the thermal qualities of the material and the potential for it to be used as a replacement for the single use Styrofoam material used for transporting hot food. I would have to do various liquid and heat tests and find something suitable to treat the material with to make it safe for food and make it less brittle and add some flexibility.

## Final Reflections

Since the talk Mette gave during [<u>Biology Zero</u>](https://mdef.gitlab.io/oliver.juggins/reflections/biology-zero/) the idea that materials can influence the physical form of objects has interested me. This is because of my background making architecture models and prototypes of products where the traditional model making materials are used such as blue foam, wood and cardboard where the form is often predetermined before the making process, through sketching and producing CAD models. This course was a chance to challenge this approach completely through learning about the material and letting its qualities guide the making process.

As a result I was keen to make the most of this approach and try and be led by the process and my learnings along the way. In this project the idea of embodied cognition was really important as it wasn’t helpful to have assumptions about how a material might react. I think this idea of having a more open attitude and approach during the design process is one of the most important things that I can take away from the course and apply to my own main project. I often find myself starting to work on something with an idea of how it will turn out, or the results I gain from an experiment which is not always helpful. I will now try and avoid falling into this trap and try and embrace the unexpected, adopt a more playful attitude and seek ‘happy accidents’ which was often the case during Material Driven Design.

Another important aspect of the project which I can take forward into my own practice and work was the idea of fully learning about a material before trying to be able to control it. This is true for working in the physical world - when I am 3D printing something for example I should understand the qualities of PLA or when I am working with a PCB I should understand how solder reacts with the different components in order to be able to control the process and not be controlled by it. I also feel this is true for working in the digital realm. I can think of programming as a kind of material which changes the actions of a computer, which can only be done through a certain level of understanding.

This brings me onto my reflection about physical / digital timescales. As I have been working digitally so much during the masters, I got really used to the instant feedback this workflow allows. At first I grew frustrated with the time I had to wait for the grain to dry, or time in the oven the mixture took to dry out, and it made me realise that although these things take time, slowing things down forces you to think. Furthermore the payoff can be much greater when working with physical objects, as holding something three dimensional gives you so much more information than a CAD model on a screen ever could. I think this is true across all design disciplines; a poster looks totally different when printed than on a screen for example.

This process of making made me realise I was missing this activity from my final project. Model making was my favourite part of my architecture degree and have decided to try and bring physical prototyping into my final project more where possible. I think this will be important as I am dealing with sometimes quite abstract concepts relating to AI, creativity and education, so having a physical point of reference could be helpful. Although I feel I didn’t quite reach the stage of having a ‘material dialogue’ in the project, there were brief moments where I worked in an ‘offline’ state, and will try and recreate the conditions to have these moments when working on my final project.
