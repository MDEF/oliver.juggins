---
title: Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---
*Tutors: Ramon Sangüesa & Lucas Peña*

Designing with Extended Intelligence was a week about learning the underlying concepts and processes of artificial intelligence and the necessary considerations when designing with artificial intelligence. During [Navigating the Uncertainty](https://mdef.gitlab.io/oliver.juggins/reflections/navigating-uncertainity/) I learnt about some of the dangers society may face due to automation, so I was intrigued to learn more about how the technology actually works and its current and projected applications. The documentation this week consists of the aspects I found most interesting during the classes, and the themes I wanted to investigate which are relevant to my vision and lines of enquiry for the year.

![]({{site.baseurl}}/break10.png)

### Intelligence

Discussing intelligence is a good place to start, as how can we apply it to a synthetic being without defining what it is? What makes humans more intelligent than other species? Is the fact that we have been asking this question of ourselves for millennia a sign of intelligence or is that something else? After struggling to get to grips with this issue during a discussion in class, I looked into what people who worked in the field of artificial intelligence were using as a definition of intelligence as a place to start.

[Eliezer Yudkowsky](http://yudkowsky.net/) is a computer scientist working at the [Machine Intelligence research Institute](https://intelligence.org/author/eliezer/) who has written a number of books relating to AI and most recently [decision making](https://equilibriabook.com/). [In conversation with Sam Harris](https://samharris.org/podcasts/116-ai-racing-toward-brink/) they discuss what intelligence looks like before a wide ranging discussion particularly on risks associated with AI and the notion of a superintelligent AI. The idea of reaching goals is an important part of their definition, and the ability to fulfil these goals across a range of contexts and environments by rote. They go further and articulate what makes human intelligence different to that of other species concluding that it is the way that our intelligence is domain general, operating over a broad range for tasks that natural selection did not pre-programme us to do. This is an important distinction to make as when discussing AI we need to define the type of intelligence with which we want to equip it with.

Two thought experiments which address applying intelligence to machines are the [Turing Test](https://en.wikipedia.org/wiki/Turing_test) and [The Chinese Room Experiment](https://en.wikipedia.org/wiki/Chinese_room) devised by Alan Turing and John Searle respectively in the latter half of the 20th century. The Turing test establishes that if a computer program can convince a human that they are in communication with another human then it can be considered intelligent. Searle however disagrees and the experiment he devised suggests that however well you program a computer, when it responds it is only simulating knowledge with no understanding of what it is actually doing, therefore not demonstrating intelligence.

<u>References and Further Links</u>

* [Sam Harris in conversation with Eliezer Yudkowsky](https://samharris.org/podcasts/116-ai-racing-toward-brink/)
* [Intelligence Explosion Microeconomics](https://intelligence.org/files/IEM.pdf) by Eliezer Yudkowsky
* [Alan Turing Institute](https://www.turing.ac.uk/)
* [Passing the Turing Test?](https://ai.googleblog.com/2018/05/duplex-ai-system-for-natural-conversation.html) - Google Duplex

![]({{site.baseurl}}/break10.png)

### The Image

Having explored a definition of intelligence, exposing some of the ambiguities when discussing what it is (particularly in relation to AI) I can explain the format of the documentation a little more clearly. I have chosen to discuss the main themes and ideas that interested me during Designing with Extended Intelligence through discussing the image - specifically image recognition and computer vision. I have chosen to do this primarily due to the ubiquity of the image and how AI is becoming an increasingly powerful and influential force in the production and classification of the millions of images published daily. Secondly, the image provides a good vehicle to discuss the main themes touched on throughout the week including the social, conceptual, technological and ethical issues regarding AI. Lastly, I am just curious as to what an artificial intelligence algorithm sees when it looks at the world, and how it sees the world in the first place. I feel this understanding is important due to their increasing deployment in managing the growing mass of visual content, internet traffic and their implications on a vast range of fields such as surveillance and medicine.

<u>Ways of Seeing</u>

*The relation between what we see and what we know is never settled* - John Berger

{% figure caption: "[*John Berger in his BBC Documentary Ways of Seeing*](https://www.youtube.com/watch?v=0pDE4VX_9Kk)" %}
![]({{site.baseurl}}/berger.jpg)
{% endfigure %}

In order to understand how an algorithm sees the world it is important to briefly discuss how we see the world as ultimately human input is necessary to whatever computer vision algorithm is created, thus containing a part of us. I have looked to the past to consider how people have thought about the image. John Berger’s essay [*Ways of Seeing*](https://www.penguin.co.uk/books/564/56465/ways-of-seeing/9780141035796.html) came to mind when thinking about the influence and perceptions of images. His work was concerned with how we see and the underlying political meaning and messages within images. The essay opened up questions surrounding the influence of the image on daily life, presenting looking as a political act and the importance of context when looking. He argues that the way we see things is directly linked to our knowledge and beliefs, and that images themselves offer a glimpse into how the creators of images see the world and their ideologies which are embedded within images, whether or not they are aware of it. How are we seen, what do we see and might we see differently are some questions the text grapples with and are particularly pertinent when thinking about artificial intelligence and the act of seeing.

![]({{site.baseurl}}/benjamin1.jpg)

Berger’s essay is influenced by the ideas of Walter Benjamin, a German philosopher associated with the Frankfurt School, in particular his works [*The Work of Art in the Age of Mechanical Reproduction*](https://www.penguin.co.uk/books/564/56491/the-work-of-art-in-the-age-of-mechanical-reproduction/9780141036199.html) and [*A Short History of Photography*](https://academic.oup.com/screen/article-abstract/13/1/5/1730936) which discusses the consequences on global culture due to the invention of photography. *The Work of Art in the Age of Mechanical Reproduction* is more concerned with the implications of mechanically reproducing art on the original piece, arguing that any reproduction is lacking presence in time and space which he describes as its *aura*. Berger discusses how these reproductions of the artwork (and image) offer novel and a more democratic understanding through their representation and circulation in a wider contexts, allowing for new interpretations and meanings to be discovered.

It is with  this understanding of the image that I will discuss it in relation to artificial intelligence. I think questions and themes explored by Berger and Benjamin are extremely relevant today when not only thinking about the impact of images on society, but their classification and reproduction as a result of the technology and processes that form computer vision algorithms, because the algorithms that carry out these classifications learn from humans.

The exhibition [*All I Know Is What’s On The Internet*](https://thephotographersgallery.org.uk/whats-on/exhibition/all-i-know-is-whats-on-the-internet) currently showing at The Photographers’ Gallery, London, presents the work of contemporary artists exploring 21st century image culture and is a “meditation on what has happened to photography in the 21st century” as described by curator Katrina Sluis. [*Machine Readable Photography*](https://thephotographersgallery.org.uk/viewpoints/machine-readable-photography) also presents a number of projects relating to some different applications and concerns raised by computer vision algorithms.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/2K6Xbo6Y07c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<u>References</u>

* [Ways of Seeing](https://www.penguin.co.uk/books/564/56465/ways-of-seeing/9780141035796.html) by John Berger
* [Ways of Seeing (four part documentary, BBC)](https://www.youtube.com/watch?v=0pDE4VX_9Kk)
* [The Work of Art in the Age of Mechanical Reproduction](https://www.penguin.co.uk/books/564/56491/the-work-of-art-in-the-age-of-mechanical-reproduction/9780141036199.html)
* [A Short History of Photography](https://academic.oup.com/screen/article-abstract/13/1/5/1730936)
* [All I Know Is What’s On The Internet](https://thephotographersgallery.org.uk/whats-on/exhibition/all-i-know-is-whats-on-the-internet)
* [Machine Readable Photography](https://thephotographersgallery.org.uk/viewpoints/machine-readable-photography)

![]({{site.baseurl}}/break10.png)

### How does a machine see?

*AI is akin to building a huge rocket ship. You need a huge engine and a lot of fuel. The rocket engine is the learning algorithms but the fuel is the huge amounts of data we can feed to these algorithms.* - Andrew Ng in Wired Magazine, May 5 2015

To try and explain how a machine actually sees and some of the processes related to artificial intelligence and images and I will attempt to clarify some terms. The classes explaining the technical aspects of artificial intelligence helped tremendously with my understanding and enabled further reading to make a lot more sense and demystified a lot of the terms I had heard but not fully understood. Having said that my technical knowledge is still very superficial and I will provide only a broad overview of some of the processes relating to computer vision. I came across a lot of useful resources and descriptions of the processes, and learnt that there is a great community sharing knowledge and ideas. [Kaggle](https://www.kaggle.com/), a platform for data science competitions and a resource for machine learning is a great example of this community. A lot of the resources I came across exposed large gaps in my knowledge but and have included some links below for reference and further reading.

The intelligence machines use to classify and recognise images has to be taught, just like human intelligence has to be taught. The explosion in big data over the past couple of years has produced a big breakthrough in the field, as data is what trains an artificial intelligence algorithm. Humans are able to learn very quickly what a bottle is for example, but the process is a lot more complicated for machines. Here are is a brief description of some of the key terms regarding computer vision.

<u>Machine Learning</u>

Machine Learning concerns applying and training algorithms on a given set of data in order to discover patterns and make predictions. These algorithms learn from examples and build their own logic without being explicitly programmed, thus providing insight without having to create custom code specific to whatever you are investigating. Some of the key steps involved in using and creating a machine learning algorithm include data processing, fitting a model, making predictions and visualisation and evaluation of the results.

Click [here](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/) for a visual introduction to machine learning from [R2D3](http://www.r2d3.us/).

{% figure caption: "*A Visual Introduction to Machine Learning
by Stephanie Yee & Tony Chu*" %}
![]({{site.baseurl}}/ml2.png)
{% endfigure %}

<u>Neural Networks</u>

Neural Networks are a subset of machine learning that uses a model inspired by the brain’s architecture, through interconnected neurons or nodes that transmit data to one another. The idea is that through this model decisions can be made in a way which simulates that of a human; it learns by itself without having to be programmed explicitly, just like the human brain. It is important to note that neural networks are made by programming standard computers resulting in software simulations in a network of mathematical functions.

For the classification of images, a set of images is provided with labels, which forms the training data. Over a series of layers, the neural network helps the model learn to be able to classify unlabelled images and provides an accuracy score (a figure between 0 and 1).

These layers are highly connected layers with input, hidden and output layers. Each layer receives the output from the previous layer with each node having its own set of rules it has been programmed with. The output layer produces values which can be read and evaluated which the model learns from. It does this through self-modification, through undergoing a series of passes, with each pass providing more information. The connections between each node are represented by a weight which depending on its value signifies its level of influence on the other nodes.

{% figure caption: "[*An Introduction to Neural Networks (source: Kaggle)*](http://blog.kaggle.com/2017/11/27/introduction-to-neural-networks/)" %}
![]({{site.baseurl}}/nn2.png)
{% endfigure %}

<u>Deep Learning</u>

Deep learning can be thought of as a deep, more complex neural network with a higher number of layers. A neural network could have between 3 and 6 hidden layers whereas a deep neural network could have 1000 layers, with billions of nodes.

<u>Convolution Neural Networks</u>

Convolutional neural networks are a type of neural network which is used for image classification as it is able to use an array of pixel values as an input for the network. This is an advanced type of neural network that has applications outside of image recognition such as language processing and text digitization which means that description of images can be provided also.

Click [here](http://terencebroad.com/convnetvis/vis.html) for an interactive 3D visualisation of the network topology of a convolutional neural network by artist [Terence Broad](http://terencebroad.com/).

<u>References and Further Links</u>

* [Why 'Deep Learning' is a Mandate for Humans not just Machines](https://www.wired.com/brandlab/2015/05/andrew-ng-deep-learning-mandate-humans-not-just-machines/) - Andrew NG in Wired Magazine, May 5 2015
* [The Inevitable](https://kk.org/books/the-inevitable/) by Kevin Kelly (Chapter 2, Cognifying)
* [A visual introduction to machine learning](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/) by [R2D3](http://www.r2d3.us/)
* [Terence Broad](http://terencebroad.com/)
* [Machine Learning terminology](https://developers.google.com/machine-learning/glossary/#s )
* [Kaggle](https://www.kaggle.com/) - Data science projects and competitions
* [Fast.ai](https://www.fast.ai/) - Democratizing Deep Learning
* [Fast.ai Datasets](https://course.fast.ai/datasets)
* [AWS Dataset](https://aws.amazon.com/opendata/public-datasets/)
* [ImageNet](http://www.image-net.org/index) - Academic benchmark for Computer Vision
* [How neural networks build up their understanding of images](https://distill.pub/2017/feature-visualization/)
* [Build your own facial recognition system](https://github.com/ageitgey/face_recognition)
* [Faceter](https://faceter.io/) - Smart surveillance system
* How [AlphaGo Zero](https://deepmind.com/blog/alphago-zero-learning-scratch/) works
* [MIT Media Lab’s Camera Culture Group](http://cameraculture.media.mit.edu/)
* [UC Berkeley CS188 Intro to AI](http://ai.berkeley.edu/project_overview.html)

![]({{site.baseurl}}/break10.png)

### Artists and Extended Intelligence

I am continuing the theme from [Navigating the Uncertainty](https://mdef.gitlab.io/oliver.juggins/reflections/navigating-uncertainity/) of looking at the work of artists to understand and discuss a subject. I have found that artists are particularly good at communicating not only the social implications of artificial intelligence but also the technical, exploring the capabilities of the technology and relaying this in a clear and visual format. Artists have responded to technological change in different ways and media throughout history. Futurist artists used painting to come to terms with the energy associated with technology in the early 20th Century and Cubist paintings responded to the birth of photography, offering a novel interpretation of objects. Artists have explored the potential of electricity for decades which freed them from the domain of the physical world, and now there is a new medium to explore and come to terms, intelligence.

{% figure caption: "*77 Million Paintings by Brian Eno*" %}
![]({{site.baseurl}}/eno1.jpg)
{% endfigure %}

<u>Trevor Paglen</u>

I will be revisiting the work of [Trevor Paglen](http://www.paglen.com/), focusing on his exhibition entitled [*A Study of Invisible Images*](https://www.metropictures.com/exhibitions/trevor-paglen4) at the Metro Pictures Gallery in New York shown in 2017. As discussed in Navigating the Uncertainty, his work is concerned with learning how to see the world, particularly the unseen and the hidden. In this exhibition he explores artificial intelligence, investigating what happens when photography and image making becomes occupied by machines. What does an artificial intelligence algorithm see when it views the world is a question Paglen tries to uncover in the exhibition. He investigates the mechanics of the algorithms used and what this means for society tackling the troublesome issue of algorithmic bias.

Paglen calls this process of computer vision and the world of machine-to-machine image creation ‘invisible images’ because machines' way of seeing is inaccessible to humans. In the piece *Behind These Glorious Times* the process of how an algorithm is trained on a dataset is communicated in a video to an opera-style soundtrack, which is also produced by an algorithm. Images are broken down into basic forms into black and white grids and areas of shading which the AI is trained to divide and analyse in parts in order to make sense of the whole. These thousands of images which teach the algorithm form the training set and dictate how the algorithm sees and understands the world.

{% figure caption: "*Behind These Glorious Times!*" %}
![]({{site.baseurl}}/behind.jpg)
{% endfigure %}

In producing these images, Paglen becomes part of the system which creates the algorithm through the training sets used and the final images he pulls out which form the output. This is what it looks like to use intelligence as a medium, making use of a totally new set of tools in image production. However this is not an intellectual exercise for Paglen, he creates these works because he believes the processes which form these images should not remain concealed due to their potentially serious consequences in the real-world. I will discuss these consequences further when I discuss ethics and bias in more detail, but Paglen very successfully sheds light on the problematic nature of machine vision systems during the exhibition.

His work *The Interpretation of Dreams* explores the value judgements that are made when building a training set of images. He uses literature as the input data for an algorithm which selects symbols from the Sigmund Freud book of the same title which it considers important for interpreting dreams. The algorithm then sees the world through these psychoanalytic symbols, exposing the way in which when a training set is built, it is not only about what is included but what is excluded. The underlying messages and questions Paglen is asking in his work are what are the politics of building these training sets, and how does this play out in society as a whole?

{% figure caption: "*The Interpretation of Dreams*" %}
![]({{site.baseurl}}/dreams.jpg)
{% endfigure %}

Trevor Paglen is just one of many artists working with artificial intelligence as a means of image production and reflection of working with intelligence as a medium. I have found the work of [Terence Broad](http://terencebroad.com/), [Kyle McDonald](http://www.kylemcdonald.net/) and [Robbie Barrat](https://robbiebarrat.github.io/) of interest and have included some work samples below.

<u>Autoencoding Blade Runner by Terence Broad</u>

For this piece Terence Broad trained a neural network called an autoencoder to reconstruct the individual frames from Blade Runner. Once trained, he got the network to reconstruct every frame and then recreate the entire film. Read more about the project [here](https://medium.com/@Terrybroad/autoencoding-blade-runner-88941213abbe).

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/3zTMyR-IE4Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<u>Sharing Faces by Kyle McDonald</u>

Sharing Faces is an installation which connects people from around the world using expressions. Participants are invited to stand in front of a screen which contains a camera capturing and storing expressions. The screen then changes the image on to someone else who has the same expression as the participant in real time, acting as a mirror of expressions.

<br>
<iframe src="https://player.vimeo.com/video/96549043?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<u>Landscape Paintings by Robbie Barrat</u>

Robbie Barrat uses generative adversarial networks which are algorithms used in machine learning which are introduced in a system of neural networks in competition with one another. Tens of thousands of images of oil painting landscapes are used from [WikiArt](https://www.wikiart.org/) with these algorithms which create new landscape imagery. At first glance the results look like quite ordinary landscape paintings, but a closer look reveals a surreal side.

![]({{site.baseurl}}/barrat.png)

<u>The Artist and the Machine</u>

Researching the work of various artists has made me reflect on their potential role as agents to shedding light on the relationship between humans and machines and AI in general. They have the ability to engage people about how algorithms work and what they see, bridging the gap between computer science and fields such as ethics. As a designer I am interested in how I can learn from the work of artists dealing with these themes and applying it to my own practice. Designers are already working with extended intelligence, and I think that the discipline can be influenced by the working methods of artists in terms of communicating how technology can be applied. Paglen does this through showing sensitivity and awareness to issues such as machine bias for example, which designers should be aware of when designing with intelligence.

The idea that artificial, or extended intelligence can be considered more of a collaborator rather than a tool is something I find interesting that artists are doing with AI. However for this collaboration a solid understanding of what the technology can do and how it works is necessary. Google’s [AMI](https://ami.withgoogle.com/) programme does exactly that through bringing together artists and engineers to work on projects that involve artificial intelligence.

<u>References and Further Links</u>

* [Trevor Paglen](http://www.paglen.com/)
* [Terence Broad](http://terencebroad.com/)
* [Kyle McDonald](http://www.kylemcdonald.net/)
* [Robbie Barrat](https://robbiebarrat.github.io/)
* [Artists and AMI](https://medium.com/artists-and-machine-intelligence)
* [AMI](https://ami.withgoogle.com/) - Programme at Google that brings artists and engineers together to realize projects using Machine Intelligence
* [Art in the Age of Machine Intelligence](https://medium.com/artists-and-machine-intelligence/what-is-ami-ccd936394a83)
* [Quick Draw](https://quickdraw.withgoogle.com/) - Game that uses neural network to identify doodles

![]({{site.baseurl}}/break10.png)

### Bias and Ethics

*We imbue technology with the ideals of the people who have created it, rather than those who use it.* - Jon Ardern, Superflux

One of the main issues relating to computer vision are the biases that built into machine learning systems and was a theme that appeared a lot during the week. Biases can be built into the training sets used, labels or from the developer creating the algorithm. A model can only be as good as the data that is used to carry out its learning so it therefore really important to be aware of where these biases come from in order to address them, and for the people using the training sets to understand the biases within the data they are using. Because computer vision algorithms are deployed in a vast array of applications and can be spread really quickly through developers using the same training sets the negative impacts of machine bias within these algorithms can be widespread and lead to discrimination on a really large scale.

Bias however is a necessary part of machine learning algorithms, which can be called statistical bias as this is bias is used to make predictions about new data that is entered into a particular model. Machine or algorithmic bias is different  and encompasses the biases within the programming from the developer or input data and is an important distinction to make.

The need for reflection and critique from humans is therefore crucial, as machines are not able to do this task for us. We have to challenge visual stereotypes and make room for these debates for programmers to engage in. Trevor Paglen explains some of the grave consequences of not dealing with this bias in an interview with Brian Boucher; ‘imagine the first time a self-driving car has to choose between two children who have run out into the road, one white and one black. If the computer “sees” the black child as a small animal, due to the bias of the training sets it has been given, its choice will be clear.’

There are ways to deal with biases however, which begins with acknowledging that it exists in the first place. Nicole Shadowen in her [essay](https://becominghuman.ai/how-to-prevent-bias-in-machine-learning-fbd9adf1198) on bias prevention in machine learning describes the role of ethics in navigating this subject from political, technical and social perspectives. She suggests experimenting with different data sets, using political pressure to promote the importance of ethical machine learning creation and raising social awareness through educating people about how and why machine learning is used. I also think that there is an opportunity for developers to implement ethical standards and increase transparency on data they have used in their training sets.

Joy Buolamwini is also working to promote issues surrounding algorithmic bias which she discusses in her [Ted Talk](https://www.ted.com/talks/joy_buolamwini_how_i_m_fighting_bias_in_algorithms/transcript?language=en#t-512023). She explains how more inclusive coding practices have to come from people, and that a diverse range of people should code and check each other's work when programming which would lead to more diversity in the creation of training sets. She is a researcher at the [MIT Media Lab](https://www.media.mit.edu/) and launched the [Algorithmic Justice League](https://www.ajlunited.org/) to raise awareness about algorithmic bias.

From this research into machine bias is clear that it is a social issue to be solved which starts with people. Technologically the field is advancing so fast that it is really important to open up conversations about these issues sooner rather than later. A merging of disciplines needs to take place and computer science as a field needs to be informed by ethics and anthropology as more advanced machine learning systems are created. We need to ‘foster a culture of interaction’ between those communities as [Joi Ito](https://joi.ito.com/) puts it. I am interested in the role of the designer and how they might be able to help bridge the gap between the technical and social aspects of artificial intelligence.

<u>References and Further Links</u>

* [Trevor Paglen Interview](https://news.artnet.com/art-world/trevor-paglen-interview-1299836) with Brian Boucher, 11 June 2018
* [How to Prevent Bias in Machine Learning](https://becominghuman.ai/how-to-prevent-bias-in-machine-learning-fbd9adf1198)
* [Google’s AI Principles](https://www.blog.google/technology/ai/ai-principles/)
* [Algorithmic Justice League](https://www.ajlunited.org/the-coded-gaze)
* [Artificial Intelligence: Challenges of Extended Intelligence](https://www.youtube.com/watch?v=KF9ZqnEiSzU), Joi Ito
* [DeepMind Ethics & Society](https://medium.com/@Ethics_Society)
* [Tackling the Ethical Challenges of Slippery Technology](http://superflux.in/index.php/ethical-challenges-of-slippery-technology/#), Superflux
* [The Ethics of Artificial Intelligence](https://nickbostrom.com/ethics/artificial-intelligence.pdf) by Nick Bostrom & Eliezer Yudowsky

![]({{site.baseurl}}/break10.png)

### Extended Intelligence and the Future

Attempting to discuss the future of artificial intelligence may seem a little ambitious, but I want to discuss some of the concepts and ideas I came across from my research which I found interesting in relation to where the underlying technology behind artificial intelligence is heading. In the previous section I touched on a key future issue for designing with extended intelligence in ethical issues and bias, which is not only relevant to designers but society as a whole. Another general point is that I think general access and availability to AI will increase with everything we own becoming embedded with intelligence, in the same way that products transformed and evolved due to electricity. Like electricity artificial intelligence will become something that completely surrounds us which we take for granted and run behind everything, so it is something I think we should understand and know how to use. Some issues and areas I think will be important in relation to artificial intelligence in the future include the evolving human - machine relationship and idea of a superintelligent artificial intelligence which I will briefly discuss.

<u>Human - Machine Relationship</u>

I think that the relationship between humans and machines will be critical to develop and understand for the benefit of society. The idea of working with machines, in a collaborative manner will aid humans and could help solve some of the complex problems in society. The advancements in medicine thanks to machine learning have been tremendous and companies such as [LabGenius](https://www.labgeni.us/) are merging the field of biology and artificial intelligence. I also think it will be an important skill to be able to work with intelligent machines as their presence increases in the workplace, replacing a lot of the jobs currently carried out by humans. Automation is likely to disrupt many industries beyond recognition, so being able to adapt and work with machines will potentially enable humans to carry out more interesting work and tasks. The outcome and implications of this increase in automation remains to be seen as I discussed during [Navigating the Uncertainty](https://mdef.gitlab.io/oliver.juggins/reflections/navigating-uncertainity/) and is a subject of its own with a lot of debating happening in this space currently.

Personal assistants found in homes around the world such as the Amazon Alexa device will increase in capability and usefulness and could start occupying more of our virtual and physical space. Intelligent machines as carers will take the personal assistant to the next level and fundamentally change how we interact with machines. Countries with an aging population such as Japan (it is projected that by 2050 39% of the population will be over 65 years old) are leading the development of these types of machines as there simply will not be enough young people to care for the elderly. China, the United States, South Korea and Germany are the other nations heavily investing in robot developments.

Physically connecting humans and machines will advance this relationship even more, and is already beginning to happen. Neural meshes which could help treat neurological disorders such as Parkinson’s disease through creating an interface between the brain and machine. This [paper](https://www.nature.com/articles/nmeth.3969) published in 2016 by a group of scientists explains the technology.

<u>AI in a Box</u>

A much debated issue in artificial intelligence is the possibility of a general and superintelligent artificial intelligence. Whether creating this kind of intelligence is possible is unknown, but I think it is important to be aware of what this intelligence might be like and what we need to be aware of. The AI in a Box thought experiment was devised by Eliezer Yudkowsky and communicates some of the key issues in the ongoing debate surrounding the issue. It is a roleplaying exercise where a human guards a box containing an AI which is trying to trick or convince the human to release it, resulting in having access to the intenet and the potential to cause harm to humanity. Yudkowsky has played the game various times acting as the AI in the box and you can see the rules of the experiment [here](http://yudkowsky.net/singularity/aibox/).

<u>Paperclip Maximizer</u>

A thought experiment relating to a general artificial intelligence which demonstrates the risk of the dangers of an intelligence that may not even be designed with malice. The paperclip maximizer is an AGI whose sole aim is to increase the number of paperclips in its collection, ultimately taking over the world and destroying humanity due to its obsession over collecting paper clips. The experiment demonstrates the dangers of not integrating human values within artificial intelligence systems and that an AGI that is not specifically programmed to be kind to humans will be almost as dangerous as if it were designed to be malicious. The experiment is thought to be devised by Nick Bostrom, who like Eliezer Yudkowsky is concerned with the dangers of creating a super or general artificial intelligence.

If you would like to know what it is like to be a paperclip maximizer click [here](http://www.decisionproblem.com/paperclips/index2.html) to play a game simulating the experience.

![]({{site.baseurl}}/paperclips.jpg)

<u>References and Futher Links</u>

* <https://www.labgeni.us/>
* [The Industries of the Future](http://www.simonandschuster.com/books/The-Industries-of-the-Future/Alec-Ross/9781476753676) by Alec Ross, Chapter 1
* [The Digital Industrial Revolution](https://www.npr.org/programs/ted-radio-hour/522858434/the-digital-industrial-revolution?t=1542355966104) - TED Radio Hour
* [Edge Question 2015](https://www.edge.org/annual-question/what-do-you-think-about-machines-that-think): What do you think about machines that think?
* [The Artificial Intelligence Revolution](https://waitbutwhy.com/2015/01/artificial-intelligence-revolution-1.html) - Tim Urban, Wait but Why blog post
* [Paperclip Maximizer Game](http://www.decisionproblem.com/paperclips/index2.html)
* [Future of Humanity Institute](https://www.fhi.ox.ac.uk/ - F)
* [Review the Future Podcast](http://reviewthefuture.com/)

![]({{site.baseurl}}/break10.png)

### Reflections

I found Designing with Extended Intelligence a really enjoyable week and succeeded in demystifying a lot of the terms and concepts surrounding artificial intelligence. The applications and possibilities of working with extended intelligence really excite me but I am aware of the caution and care required, particularly regarding algorithmic bias. Although I had heard about a lot of the terminology and some of the ideas before the week, nearly all of the technical content was new to me. I was also aware of some of the ethical issues, but had never meaningfully engaged with them and the discussions in the classes with classmates and tutors really developed my thinking through exposure to a range of different viewpoints and ideas I had not encountered. The classes provided a really good base for further investigation into the different topics and the activities within the classes really revealed some of the complexities of implementing artificial intelligence into design practices. Designing with extended intelligence in a sensitive and ethical manner will be really important in the coming years so feel it is an area I would like to investigate further and learn more about during the masters.

I have become really interested in the technology and processes that form artificial intelligence and the associated ethical issues that I was exposed to during the week. As a result the course was important for me in developing my vision and has helped me focus in on a topic to explore. I will continue looking at the work of different artists and designers that work with AI as inspiration for the development of my project which involves designed experiences communicating issues surrounding new technologies, now with a view to explore the human - machine relationship in more detail.
