---
title: Shenzhen Research Trip
period: 08-14 January 2019
date: 2019-01-24 12:00:00
term: 1
published: false
---
The second term of MDEF started with a research trip to Shenzhen, China and was a chance to experience a part of the maker culture and get a sense of one of the fastest growing cities in history. The week was filled with visits to design studios, manufacturers and factories such as [<u>SZOIL</u>](https://www.szoil.org), [<u>Seeed Studio</u>](https://www.seeedstudio.com/),  [<u>Chaihuo Makers</u>](http://www.chaihuo.org/) and [<u>X Factory</u>](http://www.chaihuo.org/xfactory/) were some notable visits. There were also visits to other parts of the city such as the urban villages which demonstrate the rapid change that is happening in the city, and the new direction certain parts of Shenzhen are taking.

### Huaqiang Bei

The electronics market at Huaqiang Bei was one of the places I was looking forward to most to visit during my time in Shenzhen and it didn’t disappoint. I went back multiple times and brought back with me a range of electronics boards and components I plan to use in my final project. I had never been to anything remotely similar to Huaqiang Bei and I could have easily spent a week there visiting the numerous markets and getting lost in the different levels filled with literally anything electronic you could imagine. Whilst spending time in the market there was a bit of a language barrier, but with some planning, photographs and some phone translations communication was surprisingly smooth given my level of Mandarin. Having this resource in the city makes the development and deployment of new products and tech no surprise, and is somewhere I would be keen to return to in order to get a greater sense of the Shanzhai culture.

![]({{site.baseurl}}/hb.jpg)
<br>
<br>
![]({{site.baseurl}}/hb2.jpg)

### Seeed Studio

Visiting Seeed Studio, X.Factory and Chaihuo Makers and hearing Eric Pan the founder and CEO if Seeed was a really interesting experience in many ways. Seeing how a company like Seeed fit into a global ecosystem of production and making was not something I had considered before and witnessing the production of pcbs first hand was quite an experience. The interest and commitment to connecting makeres to industry (X.Factory and Chaihuo Makers) was something I was really struck by, and in the coming years I would be really interested to see how this relationship develops, the direction it takes and the impact on the maker movement in China.

![]({{site.baseurl}}/factory.jpg)

### Urban Villages

Before Shenzhen’s massive development and growth in population (only around 40 years ago) the city was composed of a series of smaller communities known as urban villages. These have now been engulfed by the upward expansion of the city, but are still visible and some retain an interesting character and are at the heart of the community. This was true for the urban village which myself and some others were staying adjacent to for the duration of the trip. A visit with Jason Hilgefort who gave us a tour and some history into the phenomenon of the urban village and a glimpse of a more developed urban village with high end accommodation with bars, cafes and restaurants which would be more commonly found in a European city than the heart of Shenzhen. I would be interested to see the impact on the larger area and community if more urban villages get developed in this way and how the identity of the urban villages and city as a whole might shift.

![]({{site.baseurl}}/view.jpg)
<br>
<br>
![]({{site.baseurl}}/urbanv.jpg)

### AI and Cognitive Systems

As part of the trip the group was split into the different areas in the masters with the aim of investing this topic during the time in Shenzhen. I was in the group which encompassed AI and Cognitive systems with Gabor, Silvia and Maite. As an outcome for the trip we presented a video and some initial reflections about the time we spent in Shenzhen.

<br>
<iframe src="https://player.vimeo.com/video/312961246" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

The video is framed around three main topics which formed our main ideas and group discussions whilst on the trip which were infrastructure, data and trust and innovation. I think we were all amazed at the ubiquity and use of the mobile phone in China, and how it is the centre of daily life in China through the WeChat platform. We were interested in the data something like WeChat collects, and how this could contribute to the development of more advanced AI systems. Furthermore, given the development of how fast physical infrastructure is able to be deployed in China (the high speed rail for example) then what does this mean for the development of AI? During Shenzhen we were exposed to the rapid development of tech and products, where a learning by doing methodology is employed as a way of learning, but perhaps this approach isn’t best suited to AI development due to the possible negative consequences.

Our reflections served as a series of questions which are summarised as:

<u>Infrastructure</u>

* How will the attitude of doing and creating which has made Shenzhen the city it is today be reflected in the development of AI?
* Should this attitude be applied to the development of AI, is a more reflective approach needed?
* Do we need global ethical standards in the development of AI?

<u>Data & Trust</u>

* What is the role of companies in contrast to the government?
* Does mass data collection imply control of some degree?
* Does the willingness to give away data sacrifice free will & trust?

<u>Innovation</u>

* Is the meaning of innovation invention, or improving what is already existing? Or is it implementation into society?
* What will the effect of the personification of AI be in society?
* How will children relate to AI in the coming year, will they have AI friends?
* What is the role of AI in the next decade?

### Reflections

The trip to Shenzhen had a large impact on me as an individual and a designer. Being immersed in a culture different from my own is always something I gain a lot from, and is something that brings me a lot of joy, and being in Shenzhen for a week had this effect on me. There were a number of things that I will take away that will impact on my practice as a designer which I will be able to implement into my work this year but also beyond the masters. I learnt a lot from the attitude of makers, designers and professionals in general in Shenzhen - the idea of making and producing as a way of learning is something I have tried to do in my own work for years, but something I know I am not doing with sufficient enthusiasm and energy. Now that I have seen the results of such focus and rapid development and prototyping of ideas this should be the inspiration I need to push my final project forward and apply to the Fab Academy over the next 6 months.  

The research trip also had an impact on my final project thoughts and ideas. The visit to Seeed Studio and learning about the grove branch of their work was important in making me think about a project which I could actual develop and bring into the real world. As digital literacy has been a theme in my work from the beginning of the masters I am thinking about a modular kit for the creation of custom experimental instruments which explore the production of sound in less conventional means. The discussions around data were also interesting for my project due to the connection my project has to machine learning and the arts. This got me thinking about how I could incorporate ideas about changing people’s perception and relationship to data they produce and if there could be an engaging way to have this conversation through music, as machine learning when applied to creative fields has to potential to use data in a variety of really interesting ways.
