---
title: Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---
*Tutors: Heather Corcoran, Bjarke Calvin, Tomas Diez, Mariana Quintero*

Engaging Narratives was a week dedicated to the art of storytelling and various techniques that can be used to engage an audience. The course was split up into a series of seminars and workshops led by tutors at IaaC and guests [<u>Heather Corcoran</u>](https://twitter.com/heathercorc) the Outreach Lead at [<u>Kickstarter</u>](https://www.kickstarter.com/) and [<u>Bjarke Calvin</u>](https://twitter.com/bjarkecalvin) who is a social entrepreneur and founder of [<u>Duckling</u>](https://www.duckling.co/), a storytelling device app which formed one of the workshops.

![]({{site.baseurl}}/break10.png)

### Kickstarting Narratives

Two days of the week were with Heather who opened up the world of Kickstarter and how to run a successful campaign and engage an audience. The reason why people choose to fund a particular project on Kickstarter ultimately boils down to whether they would like to see it in the world and become a reality. However creating an interesting story about the journey that you have been on and where you are going can really help take a project to the next stage of development. You need to give people a good reason for people to give you their money and an engaging narrative can serve this purpose.

<u>Moving Image</u>

I had seen various Kickstarter videos and projects but never really broken them down and analysed them and thought about the elements of a good campaign. The video is the centrepiece and critical element of a project as it is likely to be the first thing people go to when arriving on a project's page. As a result  it can make or break the chances of receiving funding so it has to be great to watch, informative and capture the essence of the project.

As a class we analysed some campaign videos which I found a helpful exercise. The Kickstarter style of creating videos is a tried and tested one. I thought it can perhaps appear a little polished and formulaic at times, but some of the examples we discussed showed some of the approaches and different styles of how to engage and audience. I repeated this exercise for some projects that loosely relate to my areas of interest as a way to see how projects within the realm of digital literacy are approached.

[Kano Computing](https://www.kickstarter.com/projects/alexklein/kano-a-computer-anyone-can-make) and [Hackaball](https://www.kickstarter.com/projects/hackaball/hackaball-a-programmable-ball-for-active-and-creat) which are both devices aimed at promoting computational thinking. They were both hugely successful projects that far exceeded the target which can be attributed to the quality of the product and but also the way it is presented on Kickstarter.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/sgCHUGt97To" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Kano’s video follows the typical Kickstarter style, with both of the creators talking about the project in an engaging, fun and informative manner. The overall narrative is about how they want to bring play to the world of computer science and make it ‘as fun and simple as Lego’. Part of what makes it a nice video is they describe how the project has developed, where the money will be going and what the actual experience of Kano is, which ranges from the customization of the case for the computer to creating your own games. They also use testimonials from a range of people including the main target of the product validating its quality as a product.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/iNc6NRX2JG4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

On the Kano’s Kickstarter page they also had a secondary video which I thought worked really well. The video is a mini-narrative of the out of the box experience of the Kano compute and adopts a style which offers something different. The video shows that in 90 seconds you can have your computer up and running which is a powerful message made by the fact it is so short. It also doesn’t need any words because the message is so clear and I think there is something nice about this universality, communicating that this is a computer that is for everyone and that anyone can learn to code.

From my analysis on this Kickstart video and the exercise in class I have distilled some key points for creating a narratives in this style for future reference:

* Communicate key idea of project at beginning of video.
* Show project being used in intended context with intended users.
* Background story and process (depending on the project).
* Why the project should exist in the world.
* Basic, clear language and editing style which fits with the style of the project.
* Keep short and concise ~ 3 mins, for attention span of viewer.
* Why the project needs money and where it will go.

<u>Overall Message</u>

The video is just one part of the overall Kickstarter campaign which includes the branding and of course the product itself. This additional content should complement the video and fit with the style and overall message that the project is communicating. Heather talked through the importance of the main image, title and subtitle on the page of a campaign and how to construct these elements. As a rule of thumb the title should be no more than 60 characters, the subtitle no more than 130 and the image should be something that captures whatever the project is about. As an exercise I developed a hypothetical title, subtitle and image for my area of interest. I carried out this exercise relating to art and machine learning. I used some of the references I have been looking at as a source of inspiration for an experience based around creating art with artificial intelligence.

<p>
<font size="5">MaquinArt: Human & Machine collaboration through art</font></p>

*Exploring the potential of combining human and machine creativity using art as a way to open up the world of artificial intelligence, algorithms and more*

![]({{site.baseurl}}/aiart.png)

![]({{site.baseurl}}/break10.png)

### Storytelling with duckling

Bjarke ran a workshop where I had the chance to try out the app he has been developing, [<u>Ducking</u>](https://www.duckling.co/) which is based around sharing stories and experiences. Bjarke has a lot of experience in the world of storytelling, having worked on various documentaries and now being a part of the [<u>Open Documentary Lab</u>](http://opendoclab.mit.edu/) at MIT. He talked about some of the principles of creating compelling stories which included how to structure content and camera techniques, and then applied this theory to Duckling directly. I then created my own story about something I had learned or that described a particular experience. I chose to tell a story about my experience on a design / build project in Mexico, and the impact it had on me as a designer. The story can be seen [<u>here</u>](https://duckling.me/olliej/5784652316945) or below.

<br>
<iframe src="https://e.issuu.com/anonymous-embed.html?u=oliverjuggins&d=mexico" width="640" height="360" frameborder="0" allowfullscreen="true"></iframe>

I found this exercise really useful and I think the style of storytelling Duckling is promoting is a nice style. The format makes you have to be concise and the advice from Bjarke really made me think about how to structure my story to try and capture the attention of the audience from the title. I think Duckling’s success as an app will lie on the content of the users, so translating the information that we had in the workshop about how to use the app and storytelling techniques in general could ensure that content uploaded is of a high quality.

![]({{site.baseurl}}/break10.png)

### AI & Art

AI has been my main theme of interest for the past couple of weeks, so I decided to create my narrative based around how I have learnt about this topic and the journey I have been on. My interest in the arts led me to research how artists are using AI to create their work which had a large impact on my own understanding of AI in terms of both technical knowledge and wider societal issues. I created a story using the Duckling format that describes my process of learning. (3 min read).

<br>
<iframe src="https://e.issuu.com/anonymous-embed.html?u=oliverjuggins&d=ai_arty" width="640" height="360" frameborder="0" allowfullscreen="true"></iframe>

<u>Creativity & AI Literacy</u>

Creating this story helped identify the key areas which I will focus on for my final project: artificial intelligence and creativity. I am interested in a pedagogical approach to these themes and how I perhaps change the perception of artificial intelligence and machine learning as something often associated with computer science, productivity and business to something more to do with creativity and self-expression. The idea of artificial intelligence enhancing the creative process really interests me and think if this subject could be approached from the arts it could potentially increase engagement and interest.

As a way to explore my own creativity with AI and immerse myself in this area I have began a machine learning for artists and musicians [<u>course</u>](https://www.kadenze.com/courses/machine-learning-for-musicians-and-artists-v/info) which uses [<u>Wekinator</u>](http://www.wekinator.org/), a piece of software dedicated to interactive real-time machine learning. Another resource and source of inspiration is the work of [<u>Gene Kogan</u>](https://genekogan.com/), an artist and programmer working at the intersection of code and art. He seems dedicated to promote a greater understanding of artificial intelligence and machine learning to the public and has written a free book, [<u>ml4a</u>](https://ml4a.github.io/index/) (Machine Learning for Artists). He also has contributed work to [<u>Experiments with Google</u>](https://experiments.withgoogle.com/), a series which has a section dedicated to [<u>AI</u>](https://experiments.withgoogle.com/collection/ai). This is a great resource for interactive tools which explore the world of AI but make it fun and engaging and is something I will be looking a lot for inspiration.

{% figure caption: "*Playing with ‘Keyboard’, an experiment from the [<u>Creatability series</u>](https://experiments.withgoogle.com/keyboard): an online keyboard which you can play with your body via a webcam*" %}
![]({{site.baseurl}}/experiment.jpg)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Reflections

The week had a large impact on my understanding of the role of narratives and storytelling in a project. In my previous architecture degree, there was a large emphasis on the role of narrative in a project but was often expressed in quite an abstract way. I feel I have another set of storytelling tools that I can now apply to my projects which can help them to develop in the rest of the masters. This is important as the style of story that is being told should suit the project so having this broader mindset about how to engage people about what you are working on is really useful.

The week also made me think critically about storytelling and how platforms that promote a certain way of telling stories could result in them becoming homogeneous. Whilst I enjoy and am engaged with many Kickstarter campaigns the way in which there seems to be a format and very clear style and approach is something I feel I would like to challenge in my own work if the opportunity presents itself. I understand there is a minimum level of information that should be conveyed during a video, but believe that there can be new and different approaches that can help one to stand out and engage an audience.

I also learnt how hard it is to actually make an engaging narrative in the first place, and the amount of care and consideration that is needed. The fact that we consume so much information online means a very high standard is necessary to keep the viewer's attention and finding a way to create engagement is needed right from the start to maintain any interest.

![]({{site.baseurl}}/break10.png)

### References

* [Kickstarter](https://www.kickstarter.com/)
* [Duckling](https://www.duckling.co/)
* [The Creative Independent](https://thecreativeindependent.com/)
* [Google Arts & Culture Experiments](https://experiments.withgoogle.com/collection/arts-culture)
* [Google AI Experiments](https://experiments.withgoogle.com/collection/ai)
