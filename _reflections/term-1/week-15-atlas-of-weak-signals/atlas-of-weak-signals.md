---
title: Atlas of Weak Signals
period: 08 March - 11 April 2019
date: 2019-04-11 12:00:00
term: 1
published: true
---
*Tutors: José Luís de Vicente & Mariana Quintero (Definitions), Ramón Sangüesa & Lucas Lorenzo Pena (Development)*

The Atlas of Weak Signals was an intensive seminar split into two complementary courses: [definitions](https://mdef.gitlab.io/landing/term2/04-Atlas%20of%20Weak%20Signals%20-%20Definitions.html) and [development](https://mdef.gitlab.io/landing/term2/05-AtlasofWeakSignals-Development.html). I will go into more detail about the contents of these courses later in this post, but as a brief summary the seminar was a crash course in trend research and forecasting in order to develop students’ projects and place them in their wider context relating to larger global themes and trends. It combined a range of technical, theoretical skills and activities including scenario building and data scraping. Analysis and documentation of the research carried out was a large part of the seminar and resulted in the creation of a [repository](https://kumu.io/sraza/materials6#aowsscraped)[1] of ‘weak signals’ that combines all of the material gathered during the seminar. The idea is that this research and identification of weak signals can be used to find new connections and relate it to the themes and research of student projects’.

### What is a Weak Signal?

Before discussing the relation of this seminar to my project and the weak signals and themes that relate, I will define what is meant by a 'weak signal'. I understand a weak signal to be an early indicator of change which has little or no impact in the present day, but has the potential to cause a large change triggering major events in the future. It took me a while to form an idea of what a weak signal is, and what it isn’t, and my understanding evolved throughout the duration of the course and the different activities in the classes. I found the notes from the first lecture Ramon and Lucas gave during the ‘development’ track on ‘characterizing weak signals’ useful to keep in mind:

* A weak trend (what) A trend before a trend
* A weak place (where). Local / global
* A weak conversation / controversy (how)
* An intermittent start (when)
* A soft causality between scales (why) connection
* A set of powerless initiators (who) outsiders / visionaries

Due to the fact that these signals are ‘weak’ in nature, the task of finding them is not easy. It was therefore important to look for fringe material and go deep into the different topics, searching for indications around a given topic. In the ‘definitions’ course 25 topics were identified within 5 groups which form the material for finding weak signals. These groups and topics were described as:

![]({{site.baseurl}}/topics.jpg)

The general process of searching for weak signals was done in these phases:

* Highlighting issues (definitions)
* Evaluating through research and analysis (development)
* Speculating through scenario building (definitions and development)

### Creating my Atlas

For the [final presentation](https://mdef.gitlab.io/oliver.juggins/masterproject/term-2-reflection/) of the main project at the end of the second term I identified the weak signals that most strongly related to my project and area of interest, which is based around learning and artificial intelligence. I have since added some additional subsidiary topics:

![]({{site.baseurl}}/main-topics-mapping.jpg)

As the mapping suggests, there are some main topics that link to my intervention (fighting AI bias, tech for equality and human-machine creative collaboration) and some subsidiary topics (long-termism and imagining new jobs). As an exercise on speculating on the impact of these themes on my intervention from the present day to the year 2050, I created a timeline. The aim of the timeline is to not only recognise the different topics that relate to my project, but their significance in the next 30 years.

![]({{site.baseurl}}/timeline.jpg)

The question I asked which drove the development of this timeline was ‘how will learning about AI now help a 13 year old in the year 2050?’.

I will briefly describe the justification for the inclusion of these topics and their place on the timeline:

<u>Fighting AI Bias</u>

This is a topic that is relevant for my project in the present day, as it is a problem that is currently facing the field of artificial intelligence[2]. One of the aims of my project is to combat this issue through increasing interest and engagement with AI, which could have the possibility of reducing the effect of AI bias through the creation of a more diverse workforce in computer science and tech in general.

<u>Tech for Equality</u>

I believe this is going to be an issue that is still going to be present in the year 2050, as inclusion of tech in all parts of society will continue to be important.

<u>Human Machine Creative Collaboration</u>

Although there are [people](https://sougwen.com/)[3] who I believe are creatively collaborating with machines now, this is not common practice and requires a very high level of specialist knowledge few people currently have. Speculating on a future whereby the only meaningful activity is to creatively collaborate with machines in order to create a living, my project aims to be able to create the interest in intelligent machines from a young age in order to achieve the necessary level of expertise in order to work with machines effectively and creatively.

<u>Long-termism</u>

Long-termism was presented in the context of climate change and the 'Designing for the Anthropocene' lecture, but I believe it relates to my project through the idea of lifelong learning.

<u>Imagining New Jobs</u>

Imagining new jobs links to my reasoning for the inclusion human-machine creative collaboration: the jobs in the future will have a higher presence of intelligent machines which will change the employment landscape in the years to come.

This exercise helped me think about the issues that my project is engaging with and when they are relevant. It was useful to think about the themes in the future because weak signals are something which are not evident in the present day and only reveal themselves upon reflection at a future date. This kind of speculation is something I plan to continue in the third term with the work carried out during the seminar in mind.

### Defining Weak Signals

The definitions classes consisted of a series of scenario building exercises based around the 25 identified themes. Each week following the lecture we worked in groups to develop a mini project tackling one of the issues that were presented in the lecture. I participated in the following topics:

* Manipulation (additional topic within Surveillance Capitalism)
* Long-termism (Designing for the Anthropocene)
* Human-Machine Creative Collaboration (Life after AI - The End of Work)
* Refugee Tech (After the Nation-State)
* Human-Machine Interaction (Final Presentation)


It was really challenging to develop a proposal for each of the projects in the timeframe of a week, but by the end of the course felt that my confidence grew significantly in doing these speculative exercises. It was a good experience to engage with issues that were not directly linked to my area of interest and the knowledge I gained in doing these exercises and seeing the work created by the class as a whole informed my work and thinking in life beyond the masters. Furthermore, in getting a complete overview of the topics and working with themes not directly linked to my project, I was able to make a more informed decision about the placement of my project within the atlas. The final presentation was also a really useful exercise in trying to link together the different topics within the atlas as this is something I am doing for my own project and will continue to do in the third term.

These exercises will help me continue the speculative work I am doing on my project, and can apply the same techniques to build a scenario for the future impact of my project in the year 2050, which is one of the activities I have identified to carry out following the feedback from the end of term presentation. I plan for this speculation to take the form of an imaginary object / voice assistant for the year 2050. I would also like to create a short story based around a child who is learning about artificial intelligence, and how their life will be affected by this knowledge as they grow up. I would like to link the story to the timeline I have created as well as the atlas through referencing different themes within the narrative.

### Developing Weak Signals

The development course provided the technical skills and some theory relating to the topics and work carried out in the definitions classes. One of the main activities included researching the 25 topics and as a class creating a database of links for all of the topics. As part of this series of classes we were taught about how to carry out keyword analysis of the web pages we collected, using python within Google’s [collaboratory](https://colab.research.google.com/notebooks/welcome.ipynb)[4] coding environment. This was done through a process called ‘data scraping’ that uses the [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)[5] library which is a package for python that I had used before as part of the [Code Club](http://fab.academany.org/2019/labs/barcelona/local/clubs/codeclub/pythoninternet/) classes, and was a good chance to learn more about this process and how it can be used for the purposes of research.

Through employing data scraping, as a class we were able to analyse a lot of information very quickly and make connections between the different topics and search for weak signals more effectively. The idea of automation was key to the development class, as using large amounts of data was really important and carrying out the keyword analysis and making connections manually would not be possible. It is a technique I plan to continue carrying out for the research material I am gathering as it saves a significant amount of time, and use it to complement my further desk research planned for the beginning of the third term.

For the final presentation of the main studio project I used some of the techniques from the development course for making links between my topic and the relevant weak signals. The diagram below shows word clouds of some of the main themes that relate to my project (fighting AI bias and human-machine creative collaboration) and my area of interest (education / AI literacy). I then drew lines between repeated words in the atlas as a way to find connections between these different topics. The larger the words are in the cloud, the higher the frequency that the word appeared in the set of keywords from the spreadsheet the class created.

![]({{site.baseurl}}/atlas-with-connections.jpg)

I would like to continue this exercise incorporating the revised set of topics I have identified and develop of new ways of representing this information, perhaps using word clouds but in a virtual space where the connections can be shown in three dimensions.

I also used this technique for the results I gained from a workshop I carried out for my [final project](https://mdef.gitlab.io/oliver.juggins/masterproject/) which was about teaching 13 year olds about machine learning. At the end of the activity I created an evaluation form to give to the students, and then put the answers for the different questions into a spreadsheet and carried out the same process of creating word clouds from keywords.

![]({{site.baseurl}}/evalution-results-edit.jpg)

From this exercise I am able to see which words the students wrote the most times, and then can use this information to inform the design of future workshops. The dataset used for this particular word cloud was quite small, so this process could have been done by simply reading the answers manually, but as I plan to carry out more workshops and gather more data from students and teachers this could be a way to analyse the data quickly and even find connections between the different datasets I might not otherwise be able to make.

As a result this course has been really important for my project in multiple ways and has made me realise the potential of data collection, analysis and automation. I did not anticipate being able to link the methodology from the course in such a direct way for my own project and I hope to develop these skills in the third term. Something I am also keen to develop a lot further in the third term is how to visualise the results of the data analysis more effectively. I have recently learnt about [kumu](https://www.kumu.io/)[6] which I plan to use for my project but would also like to learn about and discover ways of visualising data in python.

### Final Reflections

As a whole the seminar has been really valuable for my project and it is through reflecting on all of the work carried out that I have been able recognise how it has informed my project. The learnings will continue to inform my work in the third term due to the activities I have planned for speculative design exercises as well as the data analysis / visualisation studies. Being able to view the bigger picture my project sits in and think about the trends that might influence my project has made me able to think about how I can use the content of the workshops and courses I am developing to try and engage the students I am hoping to work with in these issues too.

In the scenario building exercises during the definitions course the feedback received on the projects often mentioned that the speculation presented was too close to the present day and not radical enough. This is the main challenge I will have when building the speculation for my own project and is something I will keep in mind. Continuing to place my projects within the atlas and applying some of the thinking from other courses in the year such as [Living with Ideas](https://mdef.gitlab.io/oliver.juggins/reflections/be-your-own-system/) should also help in creating more convincing speculations for my project moving forward.

### References

* [1] [Mapping](https://kumu.io/sraza/materials6#aowsscraped) by [Saira Raza](https://mdef.gitlab.io/saira.raza/)
* [2] [AI Bias Study](https://news.mit.edu/2018/study-finds-gender-skin-type-bias-artificial-intelligence-systems-0212)
* [3] [Sougwen Chung](https://sougwen.com/)
* [4] [Collaboratory](https://colab.research.google.com/notebooks/welcome.ipynb)
* [5] [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [6] [kumu](https://www.kumu.io/)
