---
title: Design Dialogues
period: 17-23 December 2018
date: 2018-12-23 12:00:00
term: 1
published: true
---
*Tutors: Tomas Diez, Oscar Tomico and Mariana Quintero*

*Reviewers: James Tooze (Royal College of Art), Markel Cormenzana (HOLON Design), Elisabet Roselló (Center for Postnormal Policy and Futures Studies), Lucas Peña (Cognitive Technologist), Guillem Campodron (IAAC Fab Lab BCN), Jonathan Minchin (IAAC Fab Lab BCN), Ingi Freyr (IAAC Fab Lab BCN), Santiago Fuentemilla (IAAC Fab Lab BCN)*

Design Dialogues was the last week of the term which was about discussing and presenting the individual areas of interest and intervention for everyone in the masters. It was unlike any other final presentation I had taken part in before due the format being more like that of an end of year exhibition. This setup meant that conversations and dialogues were possible about everyone's work with the different reviewers and tutors simultaneously, who were a mixture of people from the Fab Lab BCN, tutors from previous courses and external professionals in related fields. I think it was a really good way to run the sessions as it meant the conversations and feedback could get to a level of depth not possible through a more traditional style of presentation. Following the exhibition style format a discussion as a group took place which was useful to hear about the general impressions of the work produced and advice on moving forward with projects and areas of research.

The wide range of interests and expertise of the jury made for some really interesting and varied conversations, and it was also a great chance to talk to tutors about how ideas had developed throughout the term and importantly how to develop them in the following two terms. This week’s documentation will serve as a record of these conversations as well as the work I presented for the presentation.

![]({{site.baseurl}}/break10.png)

### AI Literacy & The Creative Process

The themes are and areas of interest I presented were a result of my ongoing interest in AI. Over the past couple of weeks I realised that my interest in AI was very linked to its expression in the arts, and became interested in the idea of ‘AI Literacy’. This got me thinking about how learning about AI could become more engaging through the arts, as in my own research I found a lot of the material around AI often was quite technical and not very accessible. This investigation into the arts and AI led me led me to question the influence of AI on the creative process more generally. My interest in music and research carried out in [<u>An Introduction to Futures</u>](https://mdef.gitlab.io/oliver.juggins/reflections/a-introduction-to-futures/) about the future of music resulted in me focusing in on this area of the arts for my most recent exploration.

The work I presented consisted of four elements: reflection, research, a speculative exercise and a demo which aimed to draw everything together. I will briefly run through each of these elements.

<u>Reflection</u>

<img
 class="zoomable_image" src="{{site.baseurl}}/reflectionsmall.jpg" data-zoom-image="{{site.baseurl}}/reflectionlarge.jpg"
 />

 <script>
 $(".zoomable_image").elevateZoom({
   zoomType				: "inner",
   cursor: "crosshair"
 });
 </script>

 When trying to think about how my interests have developed and evolved through the term to arrive at my current area of interest, I revisited the documentation of each week so far during the masters and identified what my interests were at that time, and what the direction I wanted to go in was. I then converted this reflective exercise into a diagram which gives an overview of the development of these interests and the introduction of the different themes relating to the courses each week. The aim was to communicate how the arrival of my current areas of interest have been a result of the vast amount of new information consumed this term, how some interests have merged and others trailed off and how each week has made a contribution to my current thinking.

 <u>Research</u>

 <img
  class="zoomable_image" src="{{site.baseurl}}/researchsmall.jpg" data-zoom-image="{{site.baseurl}}/researchlarge.jpg"
  />

  <script>
  $(".zoomable_image").elevateZoom({
    zoomType				: "inner",
    cursor: "crosshair"
  });
  </script>

  I presented the research I had done over the term and compiled through the form of a document which covered the reasons I think it is important to know about AI, what is currently happening in the field relating to AI and creativity, and lastly some case studies and state of the art projects I have been using as references.

  <u>Speculation</u>

  <img
   class="zoomable_image" src="{{site.baseurl}}/speculationsmall.jpg" data-zoom-image="{{site.baseurl}}/speculationlarge.jpg"
   />

   <script>
   $(".zoomable_image").elevateZoom({
     zoomType				: "inner",
     cursor: "crosshair"
   });
   </script>

   I carried out a speculative exercise inspired by the back-casting technique presented during An Introduction to Futures which investigated the future of music and how AI could influence the creative process of musicians. The output is a timeline between the years 1950 and 2050 and documents and imagines the technological, experiential, social and stylistic changes of music between these years.

<u>Interactive Demo</u>

{% figure caption: "*Instructions for creating music with your body*" %}
![]({{site.baseurl}}/bodymusic.jpg)
{% endfigure %}

As part of some research into the accessibility of available tools for people to learn about machine learning from a non-technical background, I have been learning a piece of software called [<u>Wekinator</u>](http://www.wekinator.org/) from an online course aimed at people in the creative industries. I thought it would be nice to have an interactive element as part of my exhibition stand and the piece serves as a first iteration of what I imagine my final exhibition to be. The demo 'Body Music' consisted of a gestural controlled drum machine which people were invited to interact with. The experience aimed to bring together my areas of interest through providing information on how to train data for a machine learning algorithm (AI Literacy) and through speculating on the future of music production and performance (AI's influence on the creative process).

{% figure caption: "*Screenshot of demo on laptop, face position controlled music*" %}
![]({{site.baseurl}}/demo.jpg)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Exhibition Trial

<br>
![]({{site.baseurl}}/stand.jpg)

Design Dialogues was a good chance to test some ideas for exhibiting ahead of the final show at the end of the year. Overall I had some good feedback on how I presented my work and ideas, but there are some things I would change and have some ideas for exhibiting my work in the future. The research element for instance could have been condensed a lot more, showing only the main points and references. A little booklet with everything else I had looked at with a more complete overview of my research could have supplemented the board and given people a choice of how much information they wanted to interact with. Having too much text on a board is something I will try and avoid where possible.

The interactive element was a really valuable learning experience. I had an inclination that the experiment wouldn’t go as planned and this proved to be true. The instructions for the demo I had written were a bit too long, and people did not really want to read them. What I learnt from this is that the instructions could form a part of the experience itself, so they are unavoidable, or the experience itself is so intuitive that no instructions are needed. This also made me think about what exactly the demo was communicating, and that I might have been trying to fit in too many ideas. I also took for granted how much I had been using the Wekinator software and had expected too much of others using it for the first time. As a result next time I will test my demo with people who haven’t used it before ahead of any exhibition or presentation and also use in different environments to make sure it works as planned.

Having said all of this, I think people generally seemed interested in what the demo was showing. Having something interactive, playful and educational in some way is something I really want to incorporate into my work and final exhibition and the Fab Academy will be a great opportunity to test and develop my ideas and skills in this area.

![]({{site.baseurl}}/break10.png)

### Feedback

The presentations and conversations took place over a period of 3-4 hours which resulted in some long and stimulating discussions. The reviewers worked in pairs, and it was a nice dynamic to have a conversation in groups of three people and resulted in a lot of feedback and ideas for developing my project. It worked out that each of my conversations all took quite different directions and it was interesting to see what different members of the jury picked up on. As a result the feedback I received covered a really wide scope and in itself was a good experience as it brought home how the direction of a conversation can be dictated by what you start saying or the order you describe your thought process. I have tried to synthesize the main points from each conversation as a record and reference for developing my project.

<u>Technology - Creativity | Creativity - Technology</u>

In the conversation with Lucas and Elisabet the general advice was to select parts of the research I had done and bring them together, focusing in on a more concrete area of interest. We also spoke about how I could research further into how the creative field has adapted to technology over time, and that I can think about how music is able to shape technology, not only be shaped by it. Elisabet gave some more insights into the various future projection techniques and that the four axis exercise could be a good place to start. She also said doing the same exercise various times to narrow down and explore different avenues could be a worthwhile activity when speculating about my area of interest.

<u>Interaction</u>

The conversation with Santi and Ingi was more based around the demo in my presentation and the idea of gestural controlled music was pushed. They suggested some different pieces of equipment I could begin to use such as the Microsoft Kinect sensor and that the webcam in general was a good input to use as it gives you real-time feedback and that its nice to see your movements. We talked about the body as an instrument and was recommended to research a project developed in the Fab Academy Barcelona called [<u>Cinestesia</u>](http://www.cinestesia.io/proyecto/) which incorporates dance and music through sensors on the body. The software [<u>Supercollider</u>](https://supercollider.github.io/) was also recommended to look at in reference to algorithmic composition.

<u>Speculation - Reality</u>

Talking with Jonathan and James was the longest conversation of the afternoon for me and focused on the speculative part of my presentation. The general theme was how to push this speculation whilst trying to make it real and create experiments to test in real contexts. We talked about how I could create a technological pipeline for the question *Will thoughts create music by the year 2050?* in the timeline I created as a way to map out what the necessary technological developments would be in order to transmit information from neurons to soundwaves. The next step would then be to think of other applications for this technology. James suggested a technique of asking questions as a way to generate ideas. For speculating on a technology that does not yet exist, you can ask the question ‘If _ exists, then what?’ and as a way to generate ideas about a given speculative technology you could ask ‘what can be designed to explore _ ' . It is then crucial to then bring this speculation back to a context and design a set of experiments and tests to do with real people to gain insights from concrete research.

<u>Intervention & Extended Intelligence</u>

The lack of area of intervention, not having my terms defined and my of interest area specific enough was discussed when talking with Tomas and Markel. The way I talk about and consider AI needs to be different and I need to think of it more as extended intelligence, being explicit in presence of humans when I talk about it. I also need to be specific and give a lot more thought as to what exactly I am trying to communicate about AI and then speculate on this. What would an AI literate society look like, what does this even mean, and would this society actually lead to empowerment are questions I need to investigate. The conversation also really made me reconsider the message I am trying to get across and brought to light the fact I need to give more thought to the term ‘AI Literacy’ and see it as a two way process, as AI is always evolving and learning more about humans. One of the main points of feedback was having to think about the context and area of intervention for my project, and to start small with experiments and build up thinking about the different levels of context in which my intervention could be situated in.

![]({{site.baseurl}}/break10.png)

### Next Steps

At the end of the conversations with the reviewers a group discussion took place with everyone over the two days. There was a lot of advice on how to move the projects forward, and this list will serve as a reference to compliment the individual feedback I received from the reviewers as well as some points I have added myself:

* Define the area of interest and start exploring one of many possible directions
* Need to get in the field and start carrying out experiments as a way of learning
* Create a series of questions to set up a series of explorations for the next couple of months
* Break the core ideas into a series of small, achievable tasks which form these explorations
* Design in small steps
* Follow my interests and what I enjoy
* Proper foundations are necessary, this means researching area of interest thoroughly
* Through this research, look for opportunities for continuing what others have done, there is no point repeating work that is already out there
* Think about how the whole narrative of project can be framed, and how to communicate this to my audience
* What really are the intentions of the project and exploration?
* Identify technical skills to develop, as this will be important for prototyping
* Continue research into artist and musicians that interest me who are incorporating and considering AI in their work
* Begin conversations with artists and musicians about the themes I am interested in and create some experiments with them for researching
* Create experiments I am able to carry out on my own, over a long period of time
* Explore multiple variations of the same idea at the same time as a way to compare outcomes and results

![]({{site.baseurl}}/break10.png)

### Reflections

In the weeks leading up to the presentation I had been thinking about it a lot, weary of the fact I had a lot to develop in a short amount of time. Having the deadline forced me to make some decisions and put my ideas onto paper. It was hard to balance this work with the class time in other courses, but now feel in a much better position to think about things over the break, being able to take into account the term as a whole and the feedback from Design Dialogues.

I found the discussions during Design Dialogues really useful and have lots to material and methods to help move my project forward and develop my ideas. I think the setup of the presentations worked really well and the conversations could be taken in directions that would not really be possible standing in front of an audience. It was also really nice to be able to see what everyone else had been working on and their project proposals as during the classes it was only possible to get little snippets of information here and there of what people's interests were. It was also really useful as often people have recommendations and references to look at, and there was also the opportunity to see overlapping interests in the class which could help form collaborations in the rest of the masters.
