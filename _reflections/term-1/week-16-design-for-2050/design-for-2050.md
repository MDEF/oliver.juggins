---
title: Designing for 2050
period: 25 April - 03 May 2019
date: 2019-06-01 12:00:00
term: 1
published: true
---
*Tutors: Andrés Colmenares*

## Prismatic Minds

The three classes were based around created a *critically optimistic* design fiction series inspired by Black Mirror. We formed groups and as a collective created 5 trailers and episode concepts for the series. The name of the series was 'Prismatic Minds' and we carried out a series of activities in each of the groups to propose themes to engage with in order to maintain consistency across the series. We each needed to choose a megacity (we chose Tokyo) for the setting of the film and integrate the areas of interest of each of the projects of the people within the group. We were given some tools, techniques and guidelines such as using gifs as content, a backing track to set the mood and tone of the episode and narration to tell the story.

## Good Kenko

The episode of the group I was in was called 'Good Kenko' as the main theme around the episode was health and 'kenko' means health in Japanese. The story integrated Biology, AI, sensing data and pollution and communal living which covered all of the topics of the people in the group (Sylvia, Jess, Fifa, Ryota and myself). We built a narrative around a character called Taeko who was suffering from pollution in the city and used an AI assistant to not only sense the pollution, but tell them the parts of the city where they could go with better air quality. This assistant improved the characters physical health, but still struggled with loneliness in a big city like Tokyo. It is here that we touched on Ryota's theme, about how to foster a sense of community through using AI to make connections with people from the past. Here is the trailer we created:

<br>
<br>
<iframe src="https://player.vimeo.com/video/341238982" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
## Final project

The methods in the seminar and the way the whole class was based around thinking about our projects in the year 2050, it provided inspiration for creating a future scenario for my project. The projection of my intervention will be framed around an imaginary character called Sonia, imagining how she grows up with AI.

*Sonia takes part in a present day version of the intervention and learns about AI in a workshop in her school. The future building exercises that follow will speculate on the impact AI has had on her and how learning about AI has transformed her life. Another element of this speculation will be formed around how AI learns about Sonia, recognising the human-machine relationship as interdependent, complementary and a creative collaboration. As a result, ideas about the future of learning, growing up and what it means to be human will tie into the projection of the future intervention.*

*Sonia is a facilitator of The Puerta Project and is planning the next workshop through analysing the data from participants’ last experience of the intervention. She is using the tenth iteration of the AI companion he has been using since the government-wide initiative in 2022 which gave every child access to artificial intelligence to help them learn about it through an open source device, inspired by the BBC giving every child in the UK a Micro:bit in 2015 to advance digital literacy skills. Sonia has been programming and building her own companion in the different Fab Labs in her city. Fab Labs, as in 2019, are a place of sharing knowledge, building and shaping new technologies but are now present in almost every neighbourhood due to the deadline of the Fab City Global Initiative which aims for cities to produce everything they consume by 2054.*

*The maker movement has had a big influence on Sonia and although she has
changed careers in the course of her life 6 times already, passing on what she has
learnt has been a continuous activity. The Fab Lab network has helped him achieve this spread of knowledge moving “bits not atoms” and the documentation she has provided over the years and the work of others has enabled her to be in control and shape the AI which she has grown up with through being able to program and reprogram her ‘assistant’ to her needs.*

*Making sense of the data and learnings of his AI and passing this knowledge
onto others is the main activity of The Puerta Project and Sonia in 2050. Sonia has a record of everything and how she has learnt over the last 30 years which she uses to help the development of others through data analysis and comparison. The amount of users has generated a vast amount of data through the evaluation exercises they take part in and through the data that is recorded by their assistant.*

*Machine learning can be used to analyse this data to generate novel, personalised
content for each student to make their learning process as effective and efficient
as possible. It is this relationship between the data humans create and the analysis that AI carries out on this data that gives reason for AI to continue its relationship with humans: a feedback loop of data means that there is always something for the AI to analyse, always helping humans to make improvements in the way they have programmed.*

*The use of AI to enhance both the teaching and learning process has led to
systemic changes in the way humans share and consume information and the evolution
and appearance of new types of physical and digital learning spaces. Schools are
barely recognisable from the year 2019 and serve as a connected network rather than
individual entities in competition with one another to achieve the highest grades for its students. Much like The Puerta Project, students have their learning objectives and methods of learning tailor-made by their AI so they can advance in the most effective manner for them.*

*Despite all of the technological changes over the last 30 years, the need to be in
the same physical space for learning as one another is even less of an issue than it was in 2019: through hyper-realistic virtual reality experiences, human-human interaction is still a key strand of the intervention in 2050. The development of soft skills, teamwork and being receptive and interested in the opinions of others are skills that students develop. Just as the AI can help humans reach their potential and goals more effectively, people interacting with one another can still help people intellectually and emotionally. An increase in the presence of machines does not mean that Sonia is unable to learn from his students and vice versa.*

## Teaching Style, and Gifs

I really enjoyed the teaching style of Andres and how he approached the classes. He created the perfect atmosphere for the period of the masters we were in and had moments of watching films and telling us about interesting things interspersed within the activities. He shared many references of interesting people which serve as a record for future reading.

Andres also did an entire presentation only using Gifs, and showed how powerful they can be when presenting and communicating ideas. As such I searched for a gif relating to my project and this is what I found:

![]({{site.baseurl}}/ml.gif)
