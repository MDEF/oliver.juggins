---
title: Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---
*Tutors: Ingi Freyr Guðjónsson & Francesco Zonca*

Design for the Real Digital World was an introduction to digital fabrication through using the facilities and tools in the [Fab Lab](https://fablabbcn.org/) to transform the MDEF classroom into a studio. Before the week began, the classroom had a standard layout with tables and chairs arranged in rows and whilst it served as a good space for lectures and seminars, it didn’t offer much as a place to work in. The week was also about applying the principles of the [Fab City](https://fab.city/) at a micro scale, using found items or rubbish left on the street in the surrounding area as the building material for the weeks outcomes through the [Barcelona Trash Days](https://www.google.com/maps/d/viewer?mid=1l2VAhplHwkWYhNi6WOcDeqnxPoE&ll=41.38109650649682%2C2.2172637808228046&z=13) initiative (every day between 8pm and 10pm people are allowed to leave unwanted items in the street). In the presentation given by Ingi the question which formed the brief was asked to the class: how can digital fabrication tools be used in creative ways to work with these resources? We were also encouraged to think about what happens at the end of the year applying the thinking of the circular economy to the project.

{% figure caption: "*Some initial references from designers who have worked with discarded items including Studiomama, Curro Claret and Piet Hein Eek*" %}
![]({{site.baseurl}}/initialref.png)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Hunting for Scraps

{% figure caption: "*1550 Chairs Stacked Between Two City Buildings, Doris Salcedo*" %}
![]({{site.baseurl}}/doris.png)
{% endfigure %}

At the start of the week we realised that the materials we had collected from week 1 might limit us to what we would be able to build, so we went on another trash hunt but this time in Gràcia in search of some higher quality scrap. We also had a van which made things a lot easier to take larger items and ended up filling it up with a collection of doors, tables, and some fabric.

![]({{site.baseurl}}/break10.png)

### Group Presentations

I was in a group with Saira, Veronica, Barbara, Laura and Alexandre for the week. We had a day and half to develop a concept for the classroom, think about its layout and start making proposals of what we could build with the found materials and digital fabrication tools on offer. We started by listing everything we thought should be in the classroom and then categorized them into different groups, with each group becoming a different zone or theme within the room.

![]({{site.baseurl}}/keyareas.png)

Once we had identified these areas and what we wanted in them, we developed the floor plan, working individually to come up with our own versions and then combining the key ideas from each. The concept of the plan was to have a mixture of fixed and flexible spaces, with the fixed areas containing desks for individual work and the mini fab lab and the flexible zone for presentations, group work and some communal feature element to be made from the found materials.

![]({{site.baseurl}}/zones.png)

We then drew up the floorplan working out the best way to fit in the desks in the classroom so everyone could have a desk, whilst keeping the central zone free and flexible. This means however that for presentations everyone would have to move their chair to the central zone, and that there would be no desks, which could be a problem for some people who like to take notes on laptops and have something to lean on.

![]({{site.baseurl}}/ga.png)

Once we had a working floorplan we split up and looked at the different key areas we had identified and worked up some designs and found references. The main areas which we looked at were a side table / addition of a table to the existing chairs, terrace as a space to relax, integrating the pillars into the room through giving them a function, a communal feature / room divider and fab lab station.

![]({{site.baseurl}}/keyareasref.png)

![]({{site.baseurl}}/break10.png)

### The Brief

Following the group presentations a discussion led by Ingi helped identify the main ideas which would form the projects for each group for the rest of the week. The 4 projects were Fab Lab on wheels, storage / room divider, coffee station / mushroom growing lab and side table / desk organizer. The brief for our group was to develop the side table concept and integrate the pillars in to the room. The table had to function as something for individual use which is easy to use as a table and move around the classroom, but also something that could be used collectively, forming a series of smaller tables, or a large table for the entire class. One idea we had during the initial presentations was that we could customize or ‘hack’ our desks and add shelving or some storage, so we decided that the side table could also be something that is used on desks to serve this purpose of storage.

Due to the uniformity and consistency needed for each element, we felt the plywood that was found (and supplemented by fab lab scraps) would be the most appropriate material for the project, and that CNC would be the main digital fabrication tool that we would use to manufacture the elements. I had designed some things to be made from CNC cut wood in the past, but had used basic joinery and was keen to try and use the machine to its potential, using techniques not possible by hand with power tools. We carried out some research on different CNC joinery as a way to understand how the pieces of plywood could come together

![]({{site.baseurl}}/cncjoinery.png)

![]({{site.baseurl}}/break10.png)

### Concept Development

In discussing the design of the element, we developed the concept and brief. The work of Ronan & Erwan Bouroullec provide a good reference for the modular aspect of the project as modularity is a key theme in their work as identical  elements often come together to form something much larger.

![]({{site.baseurl}}/modular.png)

We were also interested in the idea of individual customization, and the opportunity for individuals to adapt their own table in some way. [John Habraken](http://www.spatialagency.net/database/john.habraken), a Dutch architect interested in participatory design, developed a concept for housing where the architect would provide the structure, or ‘supports’ and that users could change their own environment adapting the ‘infill’. We thought that this could translate into our design and that we could provide the ‘support’, which would consist of the main plywood structure and that over time everyone in the class could ‘infill’ and customize their element through using found objects or making use of other digital fabrication tools such as the laser cutter and 3D printer.

{% figure caption: "*Some references for desk organisation that would serve as the ‘infill’ to the structure.*" %}
![]({{site.baseurl}}/infill.png)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Process - Sketching

A lot of time was dedicated to working out the size, location and number of supports for the table. The measurement of the supports was decided early on, as it is the height of a standard table. The size and shape of the table took quite a long time to develop, as we tried to find a good balance between making something that was big enough to be used as a table for individual use, but not so big that it would dominate the table and get in the way.

![]({{site.baseurl}}/sketching.png)

Sketching was a large part the process for the group, and was the main tool we used to quickly communicate and share ideas. We talked a lot about joinery and how the different pieces would fit together, and exactly where the supports should go on the face of the table. It wasn’t until we made a scale model using the laser cutter that we really made progress with this issue. We really needed to have a physical model in order to solve the problems we were having as sketching and talking only got us so far. We prepared the laser cutting files for some of the different options we were discussing to see which one was most effective.

![]({{site.baseurl}}/break10.png)

### Laser Cutter

I had used laser cutters before during my previous studies, so knew about file preparation but had forgotten a lot of the details for setting up the different parameters such as power and speed which need to be changed depending on the type and thickness of the material. We were using 4mm plywood for the prototype and had problems cutting the wood for a while, due to the laser being a too close to the bed. Once resolved however the cutting was very quick and we assembled the models and started testing different configurations and worked out the exact size a single table should be.

From the model we learnt that two angled supports and two flat ones were the most effective and suitable option. Through playing with the model we noticed that the element could be placed in three different directions on the table, and we explored different uses linked to desk organisation that it could have in each direction such as a laptop stand, pen storage and shelving for books.

{% figure caption: "*Testing how the module can come together and how the element could be used on a table.*" %}
![]({{site.baseurl}}/laser.png)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### CNC Milling

Having finalised the design through the prototypes and some further models in rhino, we were ready to make 1:1 elements. I had never prepared files for CNC milling, or used a CNC machine so I learnt a lot about how the machine works, CNC joinery and how to draw in a way that the machine can successfully cut the wood. The steps we had to take once we had our finalised design cut with the machine were file preparation, generating the G-code and setting up the machine for use with our piece of wood. I learnt how to draw the file in layers which relates to an order in the cutting of the wood. This order is pocketing, then small profiling, followed by large profiling.

Learning about pocketing informed the design of the element, as we decided to use this technique for the supports to fit into the table so that there wouldn’t be any holes on its surface. It also determined the depth of the pockets we designed in the supports, as the maximum depth the machine can pocket it the diameter of the drill. As a result we made the pockets 6mm, which would mean that the machine could carry out the pocketing in one pass which saved us a lot of time.

![]({{site.baseurl}}/boardlayout.png)

The layout of the elements is similar to the laser cutter, only on a larger scale. The distance between the elements had to be 15mm and there had to be enough space for some screws to be placed to keep the wood steady when being cut.

When preparing the files I learnt that you have to work with the diameter of the drill being used by the machine, which in our case was 6mm and that this measurement must be taken into account when designing how the pieces will fit together, especially in the case of pocketing on the table top. The files were drawn taking into account tolerance, making the pockets for the supports 0.4mm bigger overall (0.2mm each side). The profile of the supports was changed when cutting the second board of plywood in order to save time, so that the profile could be cut in one continuous motion.

![]({{site.baseurl}}/boardlayout2.png)

I went through the process of G-code generation a couple of times during the week, and feel I understand the key steps that have to be taken in RhinoCAM. Two files are necessary, one for the screws to attach the board to the bed, and the other for everything else (pocketing, small profiling and larger profiling). Once we had the G-code we then learnt how to set the origin for the machine and some of the things to look out for such as the density and quality of the sawdust to know that the machine is cutting the wood correctly. There are two different machines in the fab lab we used, which in principle are the same but operate differently, which was confusing but something I will learn more about as I use them more frequently.

{% figure caption: "*Manually calibrating the machine in the z axis.*" %}
![]({{site.baseurl}}/cncmachine.png)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### 3D Printing

An idea which developed over the week was incorporating different materials and techniques into the elements. We thought that 3D printing could be used for the personalised items that fit into the supports and designed a simple holder for smaller desk items to 3D print. I had 3D printed something before so was aware how long the process can take so the holder is quite small. I learnt that once the file is ready in Rhino it had to be exported to an stl file format in order to be used with the software for preparing the file with the printer. We were able to save some time with the printing as we could print the holder without supports. To do this we inverted the holder so that it printed with the rim first, and ended printing the base.

{% figure caption: "*3D printed element in situ.*" %}
![]({{site.baseurl}}/3d.png)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Pillar integration

We used a number of ways to make the pillars more usable based around them acting as storage and display. Shelving was added to one of the pillars and the other had some magnetic paint and cork added so they can now be used as a display or somewhere to store small items.

![]({{site.baseurl}}/pillars.png)

![]({{site.baseurl}}/break10.png)

{% figure caption: "*A single element in use on the a. As well as the 3D printed holder, there is also a fabric pouch which serves as storage space.*" %}
![]({{site.baseurl}}/model.png)
{% endfigure %}

{% figure caption: "*Image showing a fully customized elements and its different positions on a table, with possible uses.*" %}
![]({{site.baseurl}}/renderfinal.jpg)
{% endfigure %}

{% figure caption: "*Individual elements combining to form a larger table.*" %}
![]({{site.baseurl}}/gif-03.gif)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Reflection

I wanted to finish the week with improved knowledge about using the machinery on offer at the fab lab, and am really happy to have used a mixture of digital fabrication tools at different stages of the project. Although I am unable to use all of the machines independently I can now prepare files for them all which will be useful when I need to make something for future projects. The intensity and length of the week meant that the designing of the element was really short, and I think there are some things which could be improved which can be tested in another version.

I think we could have given some more thought about what happens to the elements at the end of the year. Hopefully the elements will be well used and at the end of the masters can be taken home to be used as a bedside table, desk organizer or left in the classroom for the following year.

I also thought that using plywood as our main material wasn’t necessarily in the spirit of the brief, and that it could have been interesting to try and do something similar with a range of the types of wood we had collected. Having said that plywood was by far the most suitable material for what were making, and it would have been really hard to create identical elements with lots of different types and thicknesses of wood. As a group however the class used a lot of the found materials and now the classroom now is a much nicer place to be, which I’m sure will be very well used for the rest of the year.

My learning process during the week was informed mainly through making, with the project based around a hands-on approach to learning. I learnt a lot from the expertise of the tutors and the assistants in the Fab Lab, not only through them telling me information directly about the different steps and processes when using the machines, but also in a more practical fashion which I found was the best way to learn in the Fab Lab. Being told why the files had to be drawn in a certain way for the CNC for example was useful, but actually drawing the files and being led through the process had a much bigger impact. Discussions with the members in my group also informed my thinking and I was able to learn from the many ideas, practical tips, techniques and references we shared over the duration of the project.

![]({{site.baseurl}}/break10.png)

### Useful Links

* Click [here](https://drive.google.com/open?id=1KH24SgWcwj2yaMqtDOlwTna45bNrRon3) to get the files to make your own table / desk organiser.
* <https://fab.city/>
* <https://fablabbcn.org/>
* <https://space10.io/made-again/>
* [Chip Load Calculator](https://www.guhdo.com/chipload-calculator)
