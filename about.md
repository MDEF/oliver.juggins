---
layout: page
title:
permalink: /about/
---
<br>
<br>
Oliver Juggins is a designer with an architectural background who graduated from the University of Cambridge (2014). He has worked at a number of architecture, interior design / product design studios in the UK (Lynch Architects, Universal Design Studio, Studiomama) and more recently in Colombia (Tu Taller Design) where he assisted in the installation and production of the Colombian pavilion for the London Design Biennale (2018).

Current areas of interest include artificial intelligence and machine learning, creativity and digital literacy.
