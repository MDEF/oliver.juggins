---
layout: home
---
<br>
<br>
<font size="5">This site is dedicated to the documentation of the <a href="https://mdef.gitlab.io/landing/">Master in Design for Emergent Futures</a> programme held at the Institute for Advanced Architecture of Catalonia and Elisava School of Design & Engineering, Barcelona. This documentation consists of the work towards my <a href="https://mdef.gitlab.io/oliver.juggins/masterproject/">final project</a>, the <a href="https://mdef.gitlab.io/oliver.juggins/fabacademy/">Fab Academy</a> and a series of <a href="https://mdef.gitlab.io/oliver.juggins/reflections/">reflections</a> which record learnings, activities and feelings captured from the additional courses throughout the year</font>



<!-- <u>Documentation Samples</u>

![](images/mapping.jpg)

*Mapping Poblenou (MDEF Bootcamp), October 2018*

![](images/bio.png)

*Biology Zero, October 2018*

![](images/render.jpg)

*Design For the Real Digital World, October 2018*

![](images/shoes.png)

*Visit to Sara De Ubieta's studio (Hybrid Profiles), October 2018* -->
