---
title: Applications & Implications
period: 10-17 April 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
This week of the Fab Academy was about project planning and defining the outcome of the final project and making a sound proposal defining the scope and what the project will do in as much detail as possible. The project I will outline will encompass elements of the proposal of my [<u>final project</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/) of the masters, but will focus on the parts most relevant to the Fab Academy final project. The documentation will consist of answering a series of questions that explain the project as a whole and its main aims, ambitions and deliverables.

## What will it do?

The project I am carrying out in the masters is about teaching the basic principles of artificial intelligence and machine learning to children through workshops in schools and the community with school children and teachers. There are many different elements that form the project including lesson plans, planned practical and reflective activities and prototypes. These elements will come together in the form of a ‘facilitator toolkit’ which teachers and schools can use to plan their own activities and lessons based around teaching machine learning to children. The context of the project and its aims are covered in more detail in the [<u>project overview</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/project-introduction/) section of the website and in the following video:
<br>
<br>
<iframe src="https://player.vimeo.com/video/331261904" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

For the mid-term presentation of the third term I made the first draft of what the design of the box for the facilitator pack might look like:

![]({{site.baseurl}}/starter-pack.jpg)

## Who has done what beforehand?

The project falls under the ‘AI Literacy’ umbrella which is a branch of the digital literacy field that has grown over the last couple of years. This movement in tech education for children can be traced back to Seymour Papert and the creation of Logo (a visual programming language) and his constructionsit ideas. There are numerous kits based around digital literacy that teach the basics of coding and electronics. In the [<u>State of the Art</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/state-of-the-art/) section includes an overview of some of the projects I have been looking at for inspiration that have helped the development of my project.

I will outline some of the main projects here that relating to AI Literacy and digital Literacy toolkits:

<u>Machine Learning for Kids</u>

[<u>Machine Learning for Kids</u>](https://machinelearningforkids.co.uk/) is a platform which has a website (machinelearningforkids.co.uk) and interface built on the Scratch platform (like Cognimates) where users can create and train their own machine learning models from a series of example projects. Labelled buckets can be filled with text, images numbers and sounds and after they have been filled the IBM Watson servers are used to generate a model for classification. Scratch is then incorporated through the Machine Learning for Kids extension where games can be created such as making a Tourist Information guide or playing Noughts and Crosses. The work of Dale Lane and Machine Learning for Kids has informed and inspired much of the development and work of my project.

<!-- add some images -->

<u>Cogniamtes</u>

[<u>Cognimates</u>](http://cognimates.me/home/) is a platform built on the Scratch interface for building games, training AI models and programming robots. The project offers a set of extensions providing access to speech recognition, object recognition and robot control APIs, to build a range of AI generated projects. The project was developed by Stefania Druga whilst studying at MIT and continues the work that has supported many of the projects both historically and contemporary that have influenced my project.

<u>PopBots</u>

[<u>PopBots</u>](https://www.media.mit.edu/projects/pop-kit/overview/) is another project developed at MIT in the Personal Robotics Group by Randi William, Cynthia Breazeal and Hae Won Park. The project is aimed at a slightly younger audience than The Puerta Project but has influenced the project in terms of incorporating physical devices into activities for children to play and experiment with.

<u>Experiments with Google</u>

Google is a big figure in the AI literacy world and experiments with Google is a platform with a library of experiments communicating ideas behind different technologies in an accessible manner, including AI. The [<u>website</u>](https://experiments.withgoogle.com/collection/ai) includes a section on ‘AI Experiments’ which showcases simple experiments that make it very easy for anyone to start exploring machine learning through pictures, drawings, music and more. ‘Teachable Machine’ is an experiment which teaches some concepts of machine learning in an in-browser environment using a webcam using the [<u>TensorFlow.js</u>](https://www.tensorflow.org/) library.

## What will I design?

The principle element of the project I will design will be the workshops themselves. I plan to design 3 different workshops based around the experiences I have had and reflections from the workshops I have been carrying out for my final project. I will also be designing the contents of the facilitator kit which will also include blueprints for different exercises and activities that combine machine learning, physical computing and different topics for workshops that will be decided on ideas and feedback from teachers and students. The different elements of the workshops that I have been designing include: introductory presentation, teacher’s guide, student’s guide, extra activity and evaluation exercise.

I will also be designing the exhibition space for the project which will exhibit the facilitator kit and provide a space for discussion about the future of learning and artificial intelligence. There will also be space for people to take part in the workshops themselves.

## What materials and components will be used and how much will they cost?

Materials used for the facilitator pack will be as follows (BOM):

* BBC Micro:bit - €20
* ESP32 - €10 (approx)
* Lesson Plans - n/a
* A combination of basic sensors and actuators including servo motors & LEDs - €20 (approx)
* Machine Learning Model - $0.0025USD/message

## Where will they come from?

The lesson plans will be made by myself. The other contents of the pack will come from [<u>Diatronic Barcelona</u>](https://diotronic.com/) and the prototypes will be developed using boards and components from the Fab Lab. Depending on the development of the workshops and the outcomes additional sensors may be added. The machine learning models themselves will come from the IBM Watson Assistant (image and text recognition) via the Machine Learning for Kids platform.

## What parts and systems will be made?

The parts that will have to be made include the different workshops which connect to machine learning through building and training different models depending on the type of project (the system). The delivery of the workshops themselves also forms part of a larger system that will have to be created and developed as the project evolves.

## What processes will be used?

In terms of the workshop design, various design thinking exercises will be used to develop ideas. The instructions for the activities will use different graphic design techniques for clear and easy to follow guides for students and teachers. The prototypes made will depend on the projects participants develop, but could include 3D printing, laser cutting and CNC milling.

There will also be some processes in the digital space including building and training a machine learning model. Some coding in the Scratch environment will also be required as well as some basic electronics using different sensors and microcontrollers such as servos and the BBC micro:bit. Interfacing will also be used in the design of apps using App Inventor to connect machine learning models to the physical world.

## What tasks need to be completed?

The production of the facilitator kit in full needs to be completed. The individual elements have been defined and are in the process of development but need to be tied together. I need to find a way to link the machine learning model created in machine learning for kids to projects in the physical world. Models can be exported to App Inventor so this is likely to be the best option, so this means getting familiar with App Inventor. I need to deliver more workshops as well, and then reflect on the outcomes of all of the workshops as a whole to inform the design of the final facilitator pack.

## What questions need to be answered?

Some research questions for the masters project in general that have been driving its development include:

* Question 1: What is the best way to prepare a generation that is growing up with AI?
* Question 2: What does an AI educational platform look like which communicates its basic principles in an engaging way for an audience with limited prior knowledge?
* Question 3: What is the best way to co-design with students and teachers to create engaging AI based activities?

More specific questions I have to answer in terms of the technical elements of the project refer to the task referred to in the previous section: how can I successfully link machine learning to the physical world in a way which children can easily replicate? Another is how can reflective exercises about the ethical questions raised by artificial intelligence be intertwined in the workshops which focus on more technical skills and abilities?

Another broader question that I am interested in answering is whether or not teachers are interested in this subject area and if they feel like they could teach their own students about machine learning. I am therefore very interested in how teachers respond to these themes.

## What is the schedule?

The schedule for the masters project and exhibition stand completion is June 14. However because the exhibition construction is a significant piece of work I am aiming for the June 4 to have all of the work completed for the masters and Fab Academy. I created a Gantt chart to map out my time and the different tasks that need to take place for the project’s delivery:

![]({{site.baseurl}}/gantt-chart.png)

## How will it be evaluated?

Evaluation is a critical part of the project and forms the closing part of the exercises in the workshops. This means that the workshop participants themselves (students and teachers) will be evaluating the project. I am also receiving feedback on a weekly basis about the project as part of the Design Studio course. These continuous evaluations form an iterative design process for the project with the facilitator pack adapting and developing from user input.

## Final Comments

I am keen to create a final project for the Fab Academy if time constraints allow me to make a project. I like the idea of being able to link the different skills that I have picked up over the year and make something which captures these learnings. I am thinking this could be an acoustic music machine which can be driven by machine learning and was one of the original ideas for the Fab Academy final project. The different skills that would be included in this project would include 3D printing, interfacing, laser cutting and electronics design and production.

## Useful Links

* [Machine Learning for Kids](https://machinelearningforkids.co.uk/)
* [Cognimates](http://cognimates.me/home/)
* [PopBots](https://www.media.mit.edu/projects/pop-kit/overview/)
* [Experiments with AI](https://experiments.withgoogle.com/collection/ai)
* [TensorFlow Library](https://www.tensorflow.org/)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week12/) (Applications & Implications)
