---
title: Project Management
period: 23-30 January 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
My involvement in the Fab Academy is as part of the Master in Design for Emergent Futures in which I am developing a main project which will be complemented by the weekly assignments and ultimately final project of the Fab Academy. This week was about Project Management of the Fab Academy, with the requirement of completing a git tutorial and entering the world of web development through producing a website to document the work which will be completed during the Fab Academy. As during the masters I already have a website, I had already gone through the main requirements for this week of the Fab Academy. As a result, the documentation this week will be retrospective in some ways, looking back at how I initially created my website a couple of months ago, the changes and customization I have made to the website since, and lastly a bit about my final project theme for the masters, the link I am making to the Fab Academy and some initial ideas about my final project and some key references.

## Git & GitLab

Git is one of the most common version control systems whereby work is ‘committed’ locally and then is synced with a repository (in my case Gitlab) with a copy on the server. As mentioned I had already gone through the process of completing a git tutorial and setting up a website, which was documented at the beginning of the masters program and can be found in the [<u>MDEF Bootcamp</u>](https://mdef.gitlab.io/oliver.juggins/reflections/mdef-bootcamp/) week. A project towards the end of the first term in [<u>From Bits to Atoms</u>](https://mdef.gitlab.io/oliver.juggins/reflections/from-bits-to-atoms/) involved using GitLab for a group project so additional documentation for this process can be found there. I also discovered a really great guide entitled [<u>Getting Started with Git</u>](https://www.taniarascia.com/getting-started-with-git/) which is a much more thorough guide on git going through the installation process, commands, using the terminal and pushing commands.

Once Git is installed on your computer, you can navigate the files on your computer and ‘push’ them to the GitLab repository when they are ready. I am using GitBash to access the terminal on my computer which looks like this:

![]({{site.baseurl}}/gitbash.png)

In order to access my repository I need to type 'cd' and then find the folder which corresponds to my website. The easiest way to do this is to drag the folder into GitBash, which says 'master' to say I am in the correct folder.

Once I have updated the changes to my website or 'pushed' them, I am able to check their progress in GitLab and add any files to my repository. In the 'pipelines' section under CI/CD on the menu I can check the status of my updates.

![]({{site.baseurl}}/gitlab.jpg)

## Atom

For the everyday coding and changes of my website I am using the text editor Atom as I find the interface nicer to use than GitBash, and I can update my changes to the website and also stage, commit and push all of my changes, so it means I can do everything within one program which I find a better workflow. I got used to using Atom fairly quickly and have found its interface really nice to use. This is what it looks like:

![]({{site.baseurl}}/atom.jpg)

The images above shows the different elements which Atom is organised. The red section to the right shows the different files which form my site, including the content, config file and css (or sass as I am using Jekyll to build my website). The main central window is where I add the content and format my site. After I make a change to my site, I save the file, and an 'unstaged' change appears in the yellow box (top right). These changes then need to be 'staged', and appear in the section below. These changes are then 'committed', and I add some text to keep a record of the change I made to my website for future reference. The final step is to 'push' the changes to the GitLab repository which is done through pressing 'push' (blue box bottom right).

## Jekyll

My website is currently built with [<u>Jekyll</u>](https://jekyllrb.com/), a static site generator that runs on the Ruby programming language that makes use of [<u>markdown</u>](https://daringfireball.net/projects/markdown/) which makes writing HTML a lot easier. There are some advantages of static site generators over dynamic sites such as speed (website will perform faster) and security (there is nothing that can be exploited on the server side). In order to build a site with Jekyll, you need to install it along with Ruby. This [<u>guide</u>](https://www.taniarascia.com/make-a-static-website-with-jekyll/) is a great resource for building a site with Jekyll from scratch and walks through all of the necessary steps and you can find some themes [<u>here</u>](https://jekyllthemes.io/free).

I like using Jekyll as it means I can host my website locally to review changes I make on the fly for really quick feedback and tests. In order to do this I need access the correct folder in my terminal (as above) and type 'bundle exec jekyll serve'. A local address is then generated which looks like this:

![]({{site.baseurl}}/gitbash2.png)

Since starting to build my website with Jekyll, I have made some changes to the template, added sections and installed a library (J Query). There was a lot of trial and error in this process of making changes, here are some of the main things I have learnt along the way:

* For installing libraries, create a main folder to group similar libraries (e.g. js for Java Script) and then add the code to the 'head' section of site.
* Changes to the config file require running the server locally to update the changes (typeing 'bundle exec jekyll serve').
* New sections of the website are added through creating a new .md file, which can then be linked to different layouts (this is what I did to create the Fab Academy section to the website).

Adding the Fab Academy section to this site was done through reverse engineering the reflections part of the website. I needed to initially create a new file, create a new section in the header and create a new layout for the Fab Academy. The main difficulty came through adapting the config file. This is related to adapting the 'collections' in Jekyll. Information on this can be found [<u>here</u>](https://jekyllrb.com/docs/collections/). In the end I needed to repeat some code in the collections and defaults section, which looks like this:

![]({{site.baseurl}}/config.jpg)

## Final Project ideas

For my final project as part of the masters I am exploring how the creative process of musicians and how creativity related to the creation of sound and music can be enhanced by artificial intelligence and machine learning. More information about my project and the process of arriving at my area of interest can be found in the [<u>Design Dialogues</u>](https://mdef.gitlab.io/oliver.juggins/reflections/design-dialogues/) section of my website which consisted of a final presentation and set of discussions about the work carried out in the first term of the masters. A general aim of my project is to open up the field of AI to people in the creative industries with the aim of trying to make the development of AI more inclusive and to merge the arts with computer science, softening the binary nature of AI development with the differing approaches of artists and musicians.

I am interested in how humans can work with AI to enhance creativity, not to replace human activity. As a result the role of AI in the creation of music is something that I am interested in. What does it mean when AI can produce all the music we need, and what this means for musicians and human creativity is a question that has informed some of my research so far. My current thinking is that creativity can come through the performance of music and the creation of experimental custom made instruments, which machine learning can make more easily achievable due to different tools that have been developed and aimed at people in the creative fields.

As a result, initial ideas for my final project for the Fab Academy is the creation of a robotic instrument that can be trained through machine learning to be played through gestures and movement. The creation of sound will be produced in an analogue way, with the robot taking the form of a hybrid acoustic instrument with sound coming from found objects or anything that produces interesting sounds. I am interested the final piece being customizable in some way, with user input and involvement part of the creation of the instrument which I plan to tie into my main project for the masters.

I would like to make the instrument I create OSC or MIDI compatible so it can be used with a DAW such as [<u>Ableton</u>](https://www.ableton.com/en/) and the machine learning software I have been using, [<u>Wekinator</u>](http://www.wekinator.org/).

## Project inspiration

I have found a number of references as inspiration for the final project, ranging from robotic instruments, gloves that control music and DIY kits for building synthesizers. I aim to incorporate some different ideas from the references below and in the next week will develop a more specific idea of what my final project for the Fab Academy will be.

<u>Mortiz Simon Geist</u>
<br>
<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/wHrCkyoe72U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<iframe src="https://player.vimeo.com/video/261625441" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<u>Niko Ladze</u>
<br>
<br>
<iframe src="https://player.vimeo.com/video/255543731?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<u>Mi.Mu Gloves</u>
<br>
<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/ci-yB6EgVW4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Additional projects I have found relating to music and gestures:

* [Meka Synth](http://fabacademy.org/archives/2015/sa/students/schutze.wilhelm/fproject.html)
* [Cinestesia](http://www.cinestesia.io/proyecto/)
* [Maker Hart](https://makerhart.com/product.php?lang=en&tb=1&cid=48) music kits
* [littleBits Korg Synth Kit](https://hk.firstcodeacademy.com/en/products/4)
* [DJ Touch](http://fab.cba.mit.edu/classes/863.17/CBA/people/George%20Sun/_final/index.html)

## Useful links

* [https://jekyllrb.com/](https://jekyllrb.com/)
* [https://www.markdownguide.org/](https://www.markdownguide.org/)
* [https://daringfireball.net/projects/markdown/]((https://daringfireball.net/projects/markdown/)
* [https://www.taniarascia.com/getting-started-with-git/](https://www.taniarascia.com/getting-started-with-git/)
* [https://jekyllthemes.io/free](https://jekyllthemes.io/free)
* [http://www.wekinator.org/](http://www.wekinator.org/)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week1/) (Project Management)
