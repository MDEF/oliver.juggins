---
title: Computer-Controlled Machining
period: 06-13 March 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
<!--

include some background info and research on the types of mill etc
CNC guideline - look at Design for the Real Digital World
Talk about the different technical info here - look at notes / other students
Talk about the problem when it moved - had a piece recut
Include the files and the G-Code one - in email?
going to finish and sand and coat it properly and make some foam seats.

-->

This week of the Fab Academy was all about learning about and using the CNC machine and the assignment was to make something big. I had some experience using the CNC, designing files for milling and using RhinoCAM during [<u>Design for the Real Digital World</u>](https://mdef.gitlab.io/oliver.juggins/reflections/design-for-the-real-digital-world/) in term 1 but was looking forward to gaining more experience and making something else. The task was a group task in pairs for this week and I worked with [<u>Emily</u>](https://emilywhyman.gitlab.io/fab-academy/) to make a chair. The [<u>documentation</u>](http://fab.academany.org/2019/labs/barcelona/local/wa/week7/#cnc-concepts) from the Barcelona Fab Academy page has all of the resources I used for this week's assignment including learning about principles of CNC and how to design for the CNC machine. The process described in the documentation that we followed for the week was as follows:

* Measure material
* Produce design
* CAM & G-code generation
* G-code is checked by instructor
* Design is milled (including machine and material setup)
* Design is assembled

## Design Process

Emily and I decided that we wanted to make an armchair style seat with a very simple aesthetic which could be easily assembled and taken apart not using any glue or screws. To achieve this we had to use as few pieces of material as possible and set about finding some references looking at the geometry of the chair and the types of joints we could use.

![]({{site.baseurl}}/references.png)

Once we had an idea of the kind of style, we did some sketching of various designs and started to work in 3D quite quickly to work out the proportions and geometry. Having recently visited the Barcelona Pavilion I used the geometry from Mies van der Rohe's [<u>chair</u>](https://www.knoll.com/product/barcelona-chair) as a guideline for the design and developed a series of different options.

![]({{site.baseurl}}/mies.jpg)

![]({{site.baseurl}}/chairiterations.jpg)

We decided on a design which would be quite low to the ground and ideal to sit and read in. The image below shows the design and its various dimensions.

![]({{site.baseurl}}/chairdims.jpg)

At this point we were quite happy with the design but wanted to quickly prototype a scale model to check the proportions. We made a 1:5 lazer cut model to do this which was a good exercise as we decided that we were happy with the design and could test the joinery we had designed.

![]({{site.baseurl}}/laser1.jpg)

![]({{site.baseurl}}/laser2.jpg)

Now that the design was ready we had to create the file ready for being cut on the milling machine. We had measured out material to be 15.3mm using the callipers, so the holes in the design we already the correct size. To lay out the material I rotated the various parts of the design to be in the same plane and used the 'Make2D' command to convert the parts of the 3D model into curves, laying them out on the size of the material, trying to be as efficient as possible with the nesting . I then created layers for the different elements of the design as follows:

* Blue: Cut profiling
* Red: Pocketing
* Green: Engraving (Screws)

![]({{site.baseurl}}/layout.jpg)

The final step in Rhino was to design for tolerance and create t-bones for the different sections of the design that fit together. Both of these aspects were very important as the aim was to not use any glue or screws, so having joints that fit together well was a necessary part of the design which also makes the chair more easily transportable. T-bones are required due to the diameter of the end bit order to create enough space for certain joints to fit together. This is more easily explained by the image below:

![]({{site.baseurl}}/t-bone.jpg)

The black drawing on the right shows the design without accounting for the end mill, which is how the design was for the laser cutting file. The pink line on the black drawing shows what would happen if the file was sent to be cut in this state (the corners would be slightly curved). This is why the design has to be changed to what it is like on the left hand side.

Designing the tolerance correctly can also save a lot of time as if you do not design properly for tolerance then you might have to sand down every joint so that they fit together.

I used the experience from [<u>Design for the Real Digital World</u>](https://mdef.gitlab.io/oliver.juggins/reflections/design-for-the-real-digital-world/) to inform the design of the tolerance as the values used for this project worked well and would not require additional glue or screws. The values I used were 0.2mm offset for the holes so 0.4mm overall, so the gaps would increase to 15.70mm given that the material was 15.30mm in thickness.

![]({{site.baseurl}}/tolerance.jpg)

The size of the tabs however stayed the same (15.30mm) as this value is not affected by tolerance in the same way.

![]({{site.baseurl}}/tabdims.jpg)

## RhinoCAM

Once the file is ready, it has to be able to be opened in Rhino, as the software the Fab Lab Barcelona uses to prepare files for milling is [<u>RhinoCAM</u>](https://mecsoft.com/rhinocam-software/). Our file was created in Rhino, so we could just open our .3dm file on the computer in the Fab Lab and start preparing the file. The instructors gave workshops and lectures on how to prepare the files for cutting, so we went through process checking our notes then had Eduardo check our files.

The general process of setting up the file is as follows (for in-depth tutorials visit the [<u>documentation</u>](http://fab.academany.org/2019/labs/barcelona/local/wa/week7/#rhino) from the Fab Lab Barcelona):

* Make sure the right machine is chosen in RhinoCAM.
* Input the values of the material being used (x,y & z).
* Prepare the file for engraving (screws).
* Prepare the file for pocketing.
* Prepare the file for profiling (interior cut).
* Prepare the file for profiling (exterior cut).
* Check the files by 'simulating' in the software to make sure everything looks correct.
* Ask an instructor to check your files.
* Generate the g-code for using with the machine: engraving in one, and 'everything else' in the other (pocketing and both sets of profiling).

## Milling the Chair

The machine in the Fab Lab is a Raptor X-SL which is fairly new to the lab as in term 1 we used a different CNC machine. It was a lot easier to set up the files with this machine and setting the zero for the x,y and z. We sent the engraving first which went as planned, attached the material to the board on the machine using the screwdrivers and then sent the g-code for everything else which would cut the board.

![]({{site.baseurl}}/cutting.jpg)

The interface for the Raptor is really nice and is easy to operate and gives you visual feedback on how far through the job the file is.

![]({{site.baseurl}}/cnccomputer.jpg)

Once the job is finished we moved the end mill away so we could remove the screws safely and took the material of the bed of the machine. The pieces were still connected to the board due to the bridges so used a chisel to remove them and then sanded them as the edges were quite rough.

![]({{site.baseurl}}/edges.jpg)

## Final outcome

Once we had sanded the pieces we tested the joints and put the chair together. The tolerance worked well and no additional sanding was required to put the chair together.

![]({{site.baseurl}}/chairfinished1.jpg)

![]({{site.baseurl}}/chairfinished2.jpg)

The chair is functional as it is, but we will do some additional sanding and finishing with some oil as well as create some fabric cushions so that it is more comfortable to sit on for longer periods of time.

## Useful links

* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week7/) (Computer-Controlled Machining)

## Files

* [3D Model]({{site.baseurl}}/3D-model-final-oj-ew.zip) (3dm)
* [2D Layout]({{site.baseurl}}/2D-layout-final-oj-ew.zip) (3dm)
* [G-Code](({{site.baseurl}}/g-code.zip)
