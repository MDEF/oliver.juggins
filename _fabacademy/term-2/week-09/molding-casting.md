---
title: Molding & Casting
period: 20-27 March 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
Molding & Casting built on the work carried out in 3D design and making during the Fab Academy. This week there was not anything directly related to my final project so I decided to mold and cast an image my sister drew when she was a child which has now become the logo for my project. The original drawing she did was of a gate or gateway which translates as 'puerta' in Spanish which is the name of my [<u>final project</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/), The Puerta Project.

The task for the week involved designing a 3D mold, creating a silicone mold and then casting from that mold. This means going from something hard (3D printed material) to something soft (silicone) to something hard (casting material). We could either use the 3D printer to create the shape or the Roland milling machines that were being used for the PCBs. I opted for the 3D printer due to time constraints and the fact that the quality of the item I was planning to mold did not have to be that high. I do however plan to machine a mold in the future in order to learn 3 axis milling more extensively.

## Designing the Mold

I decided I wanted to mold a coaster or something which you could put hot pans on whilst having dinner, which the logo I mentioned above within the main volume. I designed the Rhino, first designing how I wanted the final mold to be and then adding an enclosure around it which the silicone would sit in. The design is very simple as it is a one part mold which was all I needed and makes things simpler as the item will sit flat on the table.

I traced the original drawing from my sister, joined these lines and then created a surface which I then subtracted using the BooleanSplit command.

![]({{site.baseurl}}/scan.jpg)

The overall size of the mold including the enclosure is about 150mm and the size of the object which will be cast is around 100mm.

![]({{site.baseurl}}/dims.jpg)

The depth of the shape made from the image is roughly 4mm deep, and the whole item 15mm.

![]({{site.baseurl}}/iso.jpg)

## Printing the Mold

I used the RepRap 3D printer in the Fab Lab. Although the files are prepared in the lab I do it on my computer in the Cura software to get an idea of how long the printing will take and make any changes. The printing was between 8-9 hours so I left it overnight to print.

![]({{site.baseurl}}/file-prep-cura.jpg)

![]({{site.baseurl}}/rep3.jpg)

![]({{site.baseurl}}/mold.jpg)

I was happy with the quality of the print of the mold and is a good comparison to the [<u>3D Scanning & Printing</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/3d-scanning-&-printing/) assignment where I used the Prusa. In this case I would say that the quality of the Prusa was a little better, although the objects I printed were not identical so it's not a direct comparison.

## Silicone Mold

The next stage was to make the silicone mold from the 3D printed shape. This proved to be a lot more difficult than I anticipated. First you had to measure the volume of the amount of liquid in the mold (within the enclosure of my 3D printed shape). I did this by pouring water into my mold and then into a cup. I weighed this, but this was not necessary due to water and silicone not being the same weight. The volume of my mold was just under a small cup of water.

![]({{site.baseurl}}/volmue-measure.jpg)

Now I knew the volume, I could start mixing the material to create the silicone. I chose the [<u>Formsil 25</u>](https://www.formx.eu/molding--casting/tin-silicones/form-sil-series/formsil-25-silicone---1kg-set.php) and in order to create the silicone mold it is required to mix part A (the casting rubber) and part B (the silicone catalyst). The number (in this case 25) refers to the hardness the silicone will be once set (a lower number will be more flexible).

![]({{site.baseurl}}/silicone-and-catalyst.jpg)

Before mixing together however you should read the instructions in order to make sure you have the quantities correct or part A and part B and the mixing instructions (i.e. how long to mix together the material). In this case part B was 5% of the mixture and stirring was required for 5 mins.

Because stirring the material took some time I sprayed the 3D printed mold with release spray (so the silicone would come out) as the instructions require it to be left 5-10 mins before pouring the silicone.

I then calculated the correct quantities of part A and part B, put them in the same cup and then stirred for 5 mins. When ready, I poured the mixture starting in the corner to try and keep a continuous flow of material which covered the whole mold evenly.

![]({{site.baseurl}}/poured.jpg)

Once the casting rubber and catalyst are mixed and poured into the mold there might be bubbles in the mixture. This will decrease the quality of the final object as the surface it will cast against will not be flat. I tried to get rid of bubbles through tapping the mold on the side to get the bubbles to rise, or the vacuum in the lab which was more effective.

![]({{site.baseurl}}/vacuum.jpg)

After using the vacuum there were fewer visible bubbles on the surface of the mold.

![]({{site.baseurl}}/no-bubbles.jpg)

![]({{site.baseurl}}/failed-mold.jpg)

The instructions said that the silicone would cure in 16 hours, but after over 30 the silicone was exactly the same as I left it. This meant I had to clean the mold and repeat the process of casting the silicone. Cleaning the mold was an extremely difficult and long task that was very frustrating as the silicone is really hard to get rid of. I had to use my hands and paper towels to get the majority of the material out and then it was a process of getting as much out as I could scraping with pieces of wood, plastic and cardboard. When there is a little bit of silicone left alcohol helped a lot in removing the material. The detail in mold (the logo) that was embedded was the hardest to clean.

I used the same brand of casting rubber and catalyst the second time around and thought about why the mold did not work the first time. I used a different set of scales and double checked the quantities and repeated the process again and poured the silicone. I realised that the scales I had used previously gave a different reading each time I weighed the material, so this is where the error may have occurred, although if the scales were consistently incorrect with part a and part b then this shouldn't have affected my figures.

Unfortunately I had the same result and the silicone had not set after 24 hours. Some other members of the class were having the same issue and it turned out the catalyst was out of date and this was most likely the reason why my silicone molds did not set properly.

As a result the third and final time I tried to make the mold I used a different brand, [<u>Mold star 30</u>](https://www.smooth-on.com/products/mold-star-30/) and read the new instructions and watched the [<u>video</u>](https://www.smooth-on.com/products/mold-star-30/) on the website.

![]({{site.baseurl}}/casting-material.jpg)

![]({{site.baseurl}}/third-attempt.jpg)

The process was a lot simpler this time in working out the quantities as part A and part B were equal parts. To measure my quantities I poured 2 half cups of part a and part b and put them on the table and measured by eye as this was what the instructions recommended. By this time I had learnt that some material gets left behind in the cup do used slightly more than the volume I measured from the 3D printed mold.

![]({{site.baseurl}}/instructions.jpg)

![]({{site.baseurl}}/silicone-mold.jpg)

The silicone mold set this time which was very rewarding and was really pleased with the quality as I managed to avoid having air bubbles.

## Casting Final Object

I then cast the mold using the same brand of material which had very similar instructions. I used the [Smooth Cast 305](https://www.smooth-on.com/products/smooth-cast-305/) liquid plastic in the lab. There were 2 parts that had to be mixed of equal volume, the only main difference being that the amount of material needed was a lot less due to the size of the final object being a lot smaller. The mixing process was also a lot easier as part A and part B were both thin liquids unlike the casting rubber.

![]({{site.baseurl}}/casting-material-plastic.jpg)

The material set quickly (approximately 30 mins) and as you can see from the result below the quality could be improved.

![]({{site.baseurl}}/final-mold.jpg)

The quality of the final product could be improved through using the milling wax process which I would like to do in the future, as upon closer inspection the quality of the 3D printed section on the surface of the object could be higher. But now that I understand the whole process and how to cast the silicone properly I would find the whole process a lot quicker and straightforward.

I would also like to try a two part mold to try this process now that I have understood and used the basic one part mold method. Learning about molding and casting in general has been useful for me in providing an alternative method of producing objects than the 3D printer which although I think is an extremely powerful tools, still has its limitations in terms of finishes and materials.

As an outcome I was left with three objects going from hard (3D printed mold) to soft (silicone) to hard again (cast plastic) which shows the whole process I went through:

![]({{site.baseurl}}/pos-neg-pos.jpg)

## Useful Links

* [Formsil Silicone](https://www.formx.eu/molding--casting/tin-silicones/form-sil-series/formsil-25-silicone---1kg-set.php)
* [Mold Star Silicone](https://www.smooth-on.com/products/mold-star-30/)
* [Smooth Cast Liquid Plastic](https://www.smooth-on.com/products/smooth-cast-305/)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week9/) (Molding & Casting)

## Files

* [3D Model]({{site.baseurl}}/mold-final.zip)
