---
title: Networking & Communications
period: 24-30 April 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
The theme of this week of the Fab Academy was about the different types of communication and networking and the assignment was to read a sensor value and to send it to a classmate. I worked with [<u>Gabor</u>](https://mdef.gitlab.io/gabor.mandoki/) for this week's project initially but have also included documentation on a project that I have been working on as part of my [<u>final project</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/).

The introduction to the world of network and communications came in the first term’s [<u>The Way Things Work</u>](https://hackmd.io/ys7lcCDJSeq69meSeGnIXg?view) course where we set up a server in the class using Raspberry Pis and the MQTT protocol to make different inputs and outputs communicate over WIFI using [<u>Node-RED</u>](https://nodered.org/). There were many types of communication covered in the different classes which I will give a broad overview of in this documentation. However I will focus on Bluetooth communication and WIFI, as these are the types of communication that I have carried out projects for this week’s assignments.

## Serial Communication

The types of communication covered in the class fell into two categories, ‘wired’ and ‘OTA’ (over the air). In the projects during the Fab Academy thus far I had, many communicated using wired communication, through connecting the different boards I have made and programming them for example. For example, programming the board during the [<u>Embedded Programming</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/embedded-programming/) week, the microcontrollers were talking to each other via serial. With serial communication ‘bits’ are sent one at a time.

Another way that microcontrollers can talk to each other is in parallel where data is sent across multiple wires at the same time, which increases the speed at which is sent but with more wires. Find out more about parallel communication in the [<u>documentation</u>](https://hackmd.io/s/B15x5dn9V) for the Networking and Communications class.

Two terms that I had heard in a previous class were ‘synchronous’ and ‘asynchronos’ which refer to the timing at which data is sent, by means of a ‘clock signal’. Asynchronous means that there is no support from an external clock signal when data is transferred whereas synchronous data is aligned with the clock signal, which can result in a faster transfer of data. These two images from the class documentation explain the differences:

{% figure caption: "*Synchronous (right) & Asynchronous (left) communication. Image source: Sparkfun*" %}
![]({{site.baseurl}}/serial-communication-types.jpg)
{% endfigure %}

Learning more about asynchronous communication more of the electronics design made sense. The links between the RX/TX (or Serial) protocol, the baud rate and what was happening when the serial monitor within the Arduino IDE is opened were connected. The learnings about software serial from [<u>Input Devices</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/input-devices/) also made more sense.

## Bluetooth Communication (Project 1)

As I have already explored serial communication during the Fab Academy on a couple of projects I wanted to explore OTA communication. The first project of this weeks documentation was carried out with [<u>Gabor Mandoki</u>](https://mdef.gitlab.io/gabor.mandoki/) where we read the serial values of a potentiometer and sent it to each other via Bluetooth using an ESP32. We had bought these boards and potentiometers during a visit to Huaqiangbei’s electronics market as part of the [<u>research trip</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/china/) to Shenzhen so it was nice to be able to use them on a Fab Academy project.

ESP32s communicate via BLE which stands for Bluetooth Low Energy which is a power-conserving version of Bluetooth. It's ideal for short distance communication of small amounts of data differs from 'traditional' Bluetooth by not being on all of the time. BLEs resting state is in sleep mode until a connection is made, resulting in power consumption being a lot lower.

Requirements for project:

* x1 10K Potentiometer
* x2 ESP32 boards
* x1 Breadboard
* X 3 Cables

The ESP32 boards can be used within the Arduino IDE, but in order to use them you have to install them in the board manager if you haven’t already set this board up. I had used the ESP8266, a similar microcontroller that is WIFI enabled but does not have Bluetooth. Before installing the board from the board manager however, I had to go to preferences and add an ‘Additional Board Manager URL’. I followed the steps from [<u>this documentation</u>](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md), pasting the link into the URL field:

![]({{site.baseurl}}/installation-ESP32.png)

![]({{site.baseurl}}/link-URL-network.png)

Once this is done, in the board manager, the ESP32 should appear and you can install it and then you can select 'ESP Dev Module' from the list of boards in the tools menu.

![]({{site.baseurl}}/installed-ESP32-board.png)

![]({{site.baseurl}}/ESP32-board.png)

## Analog Read with ESP32

The first things we did was set up the connections. The setup was very simple as the potentiometer was just connected to the GND, 3V3 and one of the analog read pins (in our case this was called VP which responded to analog pin A0). The code has to be adapted according to the pinouts for the board and we followed this [<u>tutorial</u>](https://circuits4you.com/2018/12/31/esp32-wroom32-devkit-analog-read-example/) on reading analog signals with an ESP32 to make sure we had everything correct and cross referenced the image:

![]({{site.baseurl}}/ESP32-Pinout.jpg)

{% figure caption: "*Image source: circuits4you*" %}
![]({{site.baseurl}}/ESP32-Pinout.png)
{% endfigure %}

Initially we uploaded a sketch to make sure that the board was connected correctly and we could read the values from the potentiometer on the serial monitor. Make sure that the correct board and port is selected so the sketch uploads.

![]({{site.baseurl}}/ESP32-port.png)

This is the code we used:

![]({{site.baseurl}}/initial-code-network.png)
<br>
<br>
<iframe src="https://player.vimeo.com/video/337792098" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

![]({{site.baseurl}}/board-setup-potentiometer.jpg)

## Bluetooth Communication

I had used Bluetooth communication with the [<u>BBC Micro:bit</u>](https://microbit.org/) in some workshops I have been doing for my [<u>main project</u>](https://mdef.gitlab.io/oliver.juggins/masterproject/), where machine learning models can be connected to physical computing in the [<u>Scratch</u>](https://scratch.mit.edu/) programming environment. However I had not sent senor values from one device to another so had to read some tutorials to find out what the steps were:

* [How to Use Bluetooth(BLE) With ESP32](https://www.instructables.com/id/How-to-Use-BluetoothBLE-With-ESP32/)
* [ESP32 Arduino Serial over Bluetooth: Client connection event](https://techtutorialsx.com/2018/12/09/esp32-arduino-serial-over-bluetooth-client-connection-event/)
* [ESP32 Arduino Serial over Bluetooth: Receiving data](https://techtutorialsx.com/2018/03/13/esp32-arduino-bluetooth-over-serial-receiving-data/)

Some of the important learnings included having to important the BluetoothSerial library and the syntax related to reading Bluetooth such as 'SerialBT.begin()' and 'SerialBT.print()' for instance. This is the code for adding Bluetooth to the initial sketch to read the value of a potentiometer:

![]({{site.baseurl}}/bluetooth-code-network.png)

The next step was to read the values on a different computer. I connected the board via Bluetooth in the device manager and read the values on the serial monitor, using a baud rate of 115200.

![]({{site.baseurl}}/serial-monitor-bluetooth.png)

## Using 2 ESP32s

Having sent data from an ESP32 to a computer, we set about connecting and sending data from one ESP32 to another. This required some further reading and understanding the client / server relationship. We referred to this [<u>tutorial</u>](https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/) and learnt that one ESP32 had to be set up as the client and the other as the server. The server is the device that shows that it exists, and the client is the device that scans and searches for the device it is looking for.

An important step is to make the server / client connect to each other correctly, which is done through generating a UUID (Universally Unique Identifier) which is used to identify the Bluetooth device (in our case the ESP32). We looked at the example sketches in the Arduino IDE for the ESP32 BLE to setup the code:

![]({{site.baseurl}}/BLE-examples-network.png)

To set up the server we used the BLE_server sketch and added the UUIDs, and the code from the previous sketches. The next step was to setup the client so used the example sketch called BLE_client and changed the UUID to make sure there was communication between the devices. Once we established a connection and everything was working we adapted the code to read and print the serial values from the potentiometer.

![]({{site.baseurl}}/reading-values-network-communications.png)

Gabor created an animation which shows the process of reading the values from the potentiometer and displaying it on the serial monitor and plotter:

![]({{site.baseurl}}/network-communications-gif.gif)
<br>
<br>
<iframe src="https://player.vimeo.com/video/337794190" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Useful links

* [The Way Things Work](https://hackmd.io/s/B15x5dn9V) MDEF Class notes and documentation
* [Node-RED](https://nodered.org/)
* [ESP8266 & MQTT Documentation](https://hackmd.io/ihNOXMFGQTOTLYwlz8_Y4w?view#)
* [Installation instructions using Arduino IDE Boards Manager](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md)
* [Reading Analog with ESP32](https://circuits4you.com/2018/12/31/esp32-wroom32-devkit-analog-read-example/)
* [How to Use Bluetooth(BLE) With ESP32](https://www.instructables.com/id/How-to-Use-BluetoothBLE-With-ESP32/)
* [ESP32 Arduino Serial over Bluetooth: Client connection event](https://techtutorialsx.com/2018/12/09/esp32-arduino-serial-over-bluetooth-client-connection-event/)
* [ESP32 Arduino Serial over Bluetooth: Receiving data](https://techtutorialsx.com/2018/03/13/esp32-arduino-bluetooth-over-serial-receiving-data/)
* [Getting Started with ESP32 Bluetooth Low Energy (BLE) on Arduino IDE](https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/)
* [Networking & Communication](https://hackmd.io/s/B15x5dn9V) MDEF Class notes and documentation
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week13/) (Networking & Communications)

## Files

* [Serial Read with ESP32]({{site.baseurl}}/serial-read-bluetooth-potentiometer-ESP32.ino)
* [Serial Read with ESP32 (server)]({{site.baseurl}}server-bluetooth-potentiometer-ESP32.ino)
* [Serial Read with ESP32 (client)]({{site.baseurl}}client-bluetooth-potentiometer-ESP32.ino)

## WIFI Communication (Project 2)

As well as the Bluetooth communication I wanted to document and have a record of a project I had worked on during a workshop for my final project. The workshop was carried out with Santi & Xavi from the Fab Lab's [<u>Future Learning Unit</u>](https://twitter.com/futurelearningu?lang=en). I will focus on the part of the project that included WIFI communication but to give some context, the project was part of a series of workshops entitled 'ComPHOrtable School'. Full documentation of this project can be found [<u>here</u>](https://hackmd.io/s/ry9dAIhqE#About). This is the introductory presentation given to staff and students to explain the projects and the first activity of the workshop series:
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQwY5ftLXEqPrm_4rlDasV_HTdHVTBIzqJSrvZIRWV0V8PWzBYn6IDC7OPzOUyiWX6RsLkzQqQSm7r1/embed?start=false&loop=true&delayms=3000" frameborder="0" width="640" height="360" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

The aim of the project was for a group of teachers and students to use photonics to make the school environment more comfortable. My project tied into the series through applying machine learning, which the projects could also incorporate. There will be no documentation concerning machine learning this week, but there will in the [<u>Interfaces & Applications</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/interface-applications/) week which will include further documentation of the ComPHOrtable school project.

Requirements:

* x1 ESP8266 (or ESP32)
* x1 NeoPixel Ring (8) LED ring
* x1 Breadboard
* x3 Cables

For this particular project, teachers developed a project that would be used to measure the group dynamic of an activity by students. At the end of an activity, students would write a message on a mobile application to describe how they found the activity and the lights would change colour according to the response. The communication between the App and the sculpture uses WIFI through connecting via WIFI.

## Setup (Electronics)

The electronics involved like in the previous project was very simple as there were only 3 connections necessary from the NeoPixel to the ESP8266: the GND, 3,3V and the connection to pin D1 on the ESP8266.

![]({{site.baseurl}}/ESP8266-setup.jpg)

The use of the NeoPixel was the element used that fulfilled the 'photonics' requirement for the project. I had never used one before in a project and there are many [<u>variations</u>](https://www.adafruit.com/category/168) of different shapes and that can be used in a ranage of uses. The one used in the project was a 8 RGB LED ring. I knew a little about programming RGB LEDs already from [<u>Output Devices</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/output-devices/). [Adafruit](https://www.adafruit.com/) manufactures the NeoPixel and full documentation on its library can be found on its [GitHub page](https://github.com/adafruit/Adafruit_NeoPixel).

## Setup (Code)

Xavi Domínguez of the Fab Lab BCN and Future Learning Unit created the code for the project and once the project was completed I went through it to try and understand what was going on. Xavi also gave a class on WIFI communication, so this helped my understanding of the different terms and relationships that were happening. The fact that I had also done the Bluetooth project with Gabor also helped a lot as some of the principles and steps were very similar. To get started however I referred to the documentation provided on the Fab Academy BCN page and as well as the following pages for some further information on setting up the board:

* [ESP8266WiFi library](https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/readme.html)
* [Using Arduino IDE with ESP8266](https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/using-arduino-ide)
* [Build an ESP8266 Web Server](https://randomnerdtutorials.com/esp8266-web-server/)

I will go through the some of the steps and then highlight the different parts of code, focusing on the most important parts in order to explain how the NeoPixel ring is able to have its colour changed via WIFI.

![]({{site.baseurl}}/NeoPixel-setup-electronics.jpg)

Just like with the ESP32, the board needs to be added in the preferences through pasting the board managers link [<u>found here</u>](https://github.com/esp8266/Arduino):

![]({{site.baseurl}}/ESP8266-install.png)

Then the board needs to be installed so it can be selected within the Arduino IDE:

![]({{site.baseurl}}/ESP8266-installed.png)

There were different options to select for the board under the ESP32, but the one that we needed was the 'NODEMCU 1.0 (ESP32-12E Module)':

![]({{site.baseurl}}/NODEMCU.png)

The next steps were to install the libraries for the ESP32WIFI and NeoPixel. This is done in through navigating tools > manage libraries, and searching for the necessary libraries and installing them. This is indicated by the red box on the image below. The blue box defines the pin out for the board and the number of pixels on the ring. The purple box defines the WIFI network (this is important as the ESP8266 is set up as a server).

![]({{site.baseurl}}/libraries-install-wifi.png)

The next section of code sets up the serial monitor, so you know which IP address to navigate to in order to control the board:

![]({{site.baseurl}}/serial-port-wifi.png)

Then the NeoPixel has to be setup. In this example the lights are green which indicated 'ON' or a positive reaction from the student or red, indicating 'OFF' and a negative response.

![]({{site.baseurl}}/NeoPixel-setup-wifi.png)

The final step is creating a simple html page which indicated the response given. Note that the code uses the syntax 'client.println' to do this.

![]({{site.baseurl}}/client-html-wifi.png)

## Using ESP8266 as a Server

Once the code is ready, compiled and uploaded the ESP8266 can be set up as a server through opening the serial monitor. By pressing the top right button on the board an IP address should be generated.

[screenshot of serial monitor with IP address]

The IP address can then be copied into a browser on a computer or mobile phone. The device must be on the same network that is setup in the code however. The address needs be altered slightly to 'http' rather than 'https' and the last part should be either 'LED=ON' or 'LED=OFF' for the NeoPixel to turn either green or red.
<br>
<br>
<iframe src="https://player.vimeo.com/video/338427476" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
The idea is that the light is part of a 'totem' type structure which should be independent of a laptop so I tested it with my portable phone battery pack.
<br>
<br>
<iframe src="https://player.vimeo.com/video/338428132" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Modifying the Code

Because I didn't create the code myself I was keen to play around a bit, changing the speed at which the LEDs changed colour and the colour of the LEDs. I did this by changing the values of red, green and blue in the corresponding piece of code (download link at bottom of page).

Next step will be to separate each of the LEDs so that they can be controlled individually, each LED would represent a group so the feedback from students could be visualised simultaneously.

## Useful links

* [ComPHOrtable School](https://hackmd.io/s/ry9dAIhqE#About) Project (WIFI communication)
* [ESP8266WiFi library](https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/readme.html)
* [Using Arduino IDE with ESP8266](https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/using-arduino-ide)
* [Build an ESP8266 Web Server – Code and Schematics](https://randomnerdtutorials.com/esp8266-web-server/)
* [Adafruit NeoPixel](https://www.adafruit.com/category/168)
* [Adafruit NeoPixel GitHub](https://github.com/adafruit/Adafruit_NeoPixel)
* [ESP-Arduino GitHub Documentation](https://github.com/esp8266/Arduino)
* [ESP-Arduino GitHub Libraries](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi)

## Files

* [NeoPixel WIFI]({{site.baseurl}}ML_Neopixels_AppInventor.ino)
* [NeoPixel WIFI]({{site.baseurl}}ML_Neopixels_AppInventor_colour_change.ino) Colour change

## Final Reflections

This is one of the weeks in the Fab Academy which I think I have learnt the most as it combined a lot of different skills and topics I had scratched the surface of but not understood properly. Both wired and OTA communication became a lot clearer and now feel a lot more confident with these topics. This is especially true for Bluetooth and WIFI communication with the ESP boards in the Arduino IDE which I am really happy about. I am now able to apply these skills directly to my final project through my learnings from the ComPHOrtable school project and prototype more activities that combine machine learning and different sensors and actuators in the physical world.

I plan to use the NeoPixel again as it has so many possibilities for different types of projects. Through learning about how the 8 LED ring was setup I should be able to now apply this knowledge to prototype projects with more complex LED configurations. I found the exercise of going through the code Xavi created step by step to try and understand what was going on really useful and plan to carry out this 'case study' type activity for future projects when I am faced with code I am unfamiliar with.
