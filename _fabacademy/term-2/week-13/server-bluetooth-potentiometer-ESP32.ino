#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include "BluetoothSerial.h"

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;
int outputValue = 0;

BluetoothSerial SerialBT;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "49082f8a-479c-47fa-9246-6188dd7d495a"
#define CHARACTERISTIC_UUID "f5f83421-28a0-4aaa-979b-8dfd6731e120"

void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");
  SerialBT.begin("ESP32");

  BLEDevice::init("Long name works now");
  BLEServer *pServer = BLEDevice::createServer();
  BLEService *pService = pServer->createService(SERVICE_UUID);
  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic->setValue(sensorValue);
  pService->start();
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  Serial.println("Characteristic defined! Now you can read it in your phone!");
}

void loop() {
  // read the analog in value:
   sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);

  SerialBT.print("sensor = ");
  SerialBT.write(sensorValue);
  SerialBT.print(sensorValue);
  SerialBT.print("\t output = ");
  SerialBT.println(outputValue);
  delay(500);
}
