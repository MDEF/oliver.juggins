#include "BluetoothSerial.h"

BluetoothSerial SerialBT;

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to


int sensorValue = 0;
int outputValue = 0; // value read from the pot


void setup() {
//give your device a name you like
  SerialBT.begin("long names work now");
}

void loop() {
   // read the analog in value:
   sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);

  SerialBT.print("sensor = ");
  SerialBT.print(sensorValue);
  SerialBT.print("\t output = ");
  SerialBT.println(outputValue);
  delay(500);
}
