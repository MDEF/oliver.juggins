---
title: Input devices
period: 27 March 2019 - 03 April 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
For Input Devices the task consisted of recreating one of [<u>Neil's boards</u>](http://academy.cba.mit.edu/classes/input_devices/index.html) through milling and soldering it and then programming it to read a sensor of our choice. There were many different input devices to choose from and we chose a [<u>PIR sensor</u>](https://learn.adafruit.com/pir-passive-infrared-proximity-motion-sensor/how-pirs-work) as our sensor of choice to work with and take readings from for the week.

## The Sensor

The sensor we used was one I had from a previous project which I had purchased from the internet a couple of years ago so am unsure of the manufacturer. I found the [<u>documentation</u>](https://cdn-learn.adafruit.com/downloads/pdf/pir-passive-infrared-proximity-motion-sensor.pdf) for an Adafruit PIR sensor which includes links to the datasheets of some of the different sensors and gives some general information about the sensor.

![]({{site.baseurl}}/pir-sensor.jpg)

## Making the PCB

All of the files for making and programming the PCB were included in the [<u>documentation</u>](http://academy.cba.mit.edu/classes/input_devices/index.html) provided by the Fab Academy (under the subheading 'motion'). I used [Fab Modules](http://fabmodules.org/) to create the rml file and discovered I had learnt how to prepare this file off by heart. I used the pngs provided in the documentation, the one below shows the traces with the different components:

![]({{site.baseurl}}/board-and-components.png)

As in the previous weeks which required milling a board I used the [<u>Roland SRM-20</u>](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine) and by this time felt quite confident milling the board. I still however ran into some issues - I seem to learn something new every time I use this piece of equipment.

The problem this time was that after setting the x,y and z points relative to my piece of material for milling, when I sent the file it would either mill in the air or very lightly, not cutting deep enough only etching the surface. I rebooted the computer, recreated the rml files multiple times and the same thing continued to happen. At this point I asked the instructor Eduardo who told me the z axis was maxxed out and was preventing the mill from cutting the board properly. He showed me a little trick to get around this issue by elevating the mill slightly, then relowering the end mill and resetting the z axis. This worked and I was able to mill the board nicely.

I then collected the components from the lab and updated the google docs and stuck in my sketchbook.

![]({{site.baseurl}}/sketchbook-input.jpg)

I felt a lot more confident now soldering the board and managed to do all of the soldering relatively quickly compared to my other soldering efforts. I started with the ATtiny 45 microcontroller and worked outwards.

![]({{site.baseurl}}/soldering-input.jpg)

When it came to soldering the connection for the sensor I realised that I had a bit of a problem. Due to the fact that the sensor had wires for the 3-pin ground/ out/ power pads I needed to make a connection in order to solder it to the board. I could not however directly solder this connection as there were some traces on the board that would be touched by the extension and affect the circuit. To get around this I used some plyers and bent the connection roughly 90 degrees on one half to get around this issue.

![]({{site.baseurl}}/bending-pins-sensor.jpg)

I then could connect the sensor successfully and made it easier to transport as I could easily attach and detach the sensor.

![]({{site.baseurl}}/completed-soldered-board.jpg)

## Programming the Board

Once the board is soldered, as always it needs to be programmed. The last time I programmed my board in week 8 during [<u>Embedded Programming</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/embedded-programming/) I ran into some issues but felt like I learnt a lot and would find programming the board this week quite straightforward and would manage to do it a little faster. It wasn't as straightforward as I had envisaged, but learnt a lot in the process which definitely helped my understanding of some of the concepts introduced in week 8.

The first step I took to make sure that the soldering was OK was a visual inspection, checking that I had not made a short in the circuit and had connected any of the traces with solder by accident. I also checked the quality of the soldering but was quite happy with it since I had done this throughout the time I soldered. I then connected the board with the FTDI cable the correct way around to my computer and to the programmer in the lab (AVRISP MKII) and a green light showed which was a good sign.

![]({{site.baseurl}}/green-light.jpg)

I then tried to program the board using the files in the documentation. I decided I wanted to try something different from week 8 so tried to program the board using the makefile. I refreshed myself with the [<u>tutorial</u>](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/makefile.html) I previously looked at and saved the file to my desktop. I followed the instructions in the tutorial and typed the following code into the terminal to try and create the makefile:

* make -f hello.ADXL343.make program-avrisp2

However this did not work after multiple attempts and kept getting this error message:

![]({{site.baseurl}}/failed-make.png)

Due to timeconstraints I returned to the method I knew of programming the board within the Arduino IDE but I am determined to use this method at some point to learn it.

In order to program the board I needed to find some code for a PIR sensor and Arduino. I searched online and found [<u>this</u>](http://www.circuitstoday.com/interface-pir-sensor-to-arduino) code which suited my needs:

![]({{site.baseurl}}/original-code.jpg)

However, there were some steps I had to take before I could use this code. Firstly I had to select the correct board etc in the tools:

![]({{site.baseurl}}/tools-setup-input.png)

I then had to adapt the code, changing the pin of the sensor in the original code from 7 to 4, because the pin outs on the ATtiny 45 are different from that of the Arduino:

![]({{site.baseurl}}/ATtiny45-85.png)

I also had to change the original part of the code which relating to the serial communication. Due to the microcontroller I was using I had to use the [<u>SoftwareSerial</u>](https://www.arduino.cc/en/tutorial/SoftwareSerialExample) library so that the correct type of communication was used. I read the documentation on the Arduino website and added this library and change the parts of the code which included 'serial' to 'mySerial'. It took me a while to work this out however and discovered I had to write 'mySerial' on this [<u>forum</u>](https://www.reddit.com/r/arduino/comments/3xf712/failing_at_trying_to_write_to_serial/).

I then burned to bootloader which seemed to work and I didn't get any error messages.

![]({{site.baseurl}}/burn-bootloader.png)

I uploaded the code and but did not get any readings when I opened the serial port.

![]({{site.baseurl}}/no-readings.png)

There was another part of the code I had to change which also related to the serial communication which was the RX TX pin numbers in the code. I thought I had read the pin outs correctly but had the 2 and 3 the wrong way around as shown below:

![]({{site.baseurl}}/rxtx.png)

I was then left with the following final code, which enabled me to take readings from the serial monitor successfully:

![]({{site.baseurl}}/final-correct-code.png)
<br>
<iframe src="https://player.vimeo.com/video/332663625" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Useful links

* [How PIRs Work](https://learn.adafruit.com/pir-passive-infrared-proximity-motion-sensor/how-pirs-work)
* [Makefile for Embedded Programming](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/makefile.html)
* [SoftwareSerial Documentation](https://www.arduino.cc/en/tutorial/SoftwareSerialExample)
* [Writing to SoftwareSerial](https://www.reddit.com/r/arduino/comments/3xf712/failing_at_trying_to_write_to_serial/) forum help
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week10/) (Input Devices)

## Files

* [Motion Sensor Code]({{site.baseurl}}/input-code-motion-sensor.ino)

<!-- Install the software serial library (link). Talk a bit about this.

I also had to change the RX TX library pins - this relates to the serial.


software serail library - needed to do this for the input becasue of the chip I was using. To slow it down.

Include the original code - and circle the parts I had to change.

Talk about how 2 and 3 were the wrong way around for the TX RX.

Include the images I had to look at for the pinout number change.

Put in my video


http://forum.arduino.cc/index.php?topic=167635.0 - myserial code

https://www.reddit.com/r/arduino/comments/3xf712/failing_at_trying_to_write_to_serial/

https://www.arduino.cc/en/tutorial/SoftwareSerialExample

Code I adapted - http://www.circuitstoday.com/interface-pir-sensor-to-arduino

pin outs - http://highlowtech.org/?p=1695

http://academy.cba.mit.edu/classes/input_devices/index.html

## What is an Input Device? -->
