---
title: Output devices
period: 03-10 April 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
Output Devices was another week based around the electronics part of the Fab Academy. It built on the work done last week during [<u>Input Devices</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/input-devices/). The task was very similar: to recreate one of Neil’s boards (milling, soldering in the usual method) and then program it. Neil had included many different examples including LED arrays, different types of motors, solenoids but I chose the RGB LED board.

The code and files were all included in the [<u>documentation</u>](http://academy.cba.mit.edu/classes/output_devices/index.html). I also had a look in the Fab Academy [<u>archive</u>](http://archive.fabacademy.org/) to look at the documentation of other students that had already done this assignment with the RGB LED as an output to try and anticipate any potential issues. I found the following [<u>page</u>](http://archive.fabacademy.org/archives/2017/fablabcept/students/177/rgb-led.html) and read the documentation.

## RGB LEDs

The name RGB refers to red, blue and green and an RGB LED combines these three colours in order to produce a wide range of hues, by adapting the intensity of the different LEDs. I chose to use the RGB as an output as I had not worked with sort of LED before. This sort of LED can be useful for projects where you need different colours on a device to indicate something for example. As I am working with machine learning in my [<u>main project</u>] and am keen to incorporate physical computing into the workshops I have been working on, the colour of an RGB LED could be used as the output for analysing the sentiment of text (the input).

{% figure caption: "*Simple colour mixing chart to give an idea how to produce different colours. Source: Random Nerd Tutorials*" %}
![]({{site.baseurl}}/color-mixing.png)
{% endfigure %}

## Making the Board

Just like last week, making the board was relatively straightforward as it was the fourth board I milled as part of the Fab Academy and knew the machinery and the different tips and tricks along the way. I encountered the same issue as [<u>last week</u>]() of the board not cutting properly due to the problem with the z-axis with the [<u>Roland SRM-20</u>](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine) machine, but the debugging process was a lot quicker since I knew how to reset the axis properly. As with all of the other boards I have milled I used [<u>fabmodules</u>](http://fabmodules.org/) and used the png files for the traces and interior provided on the [<u>documentation</u>](http://academy.cba.mit.edu/classes/output_devices/index.html). Preparing the rml files was straightforward as I had done this many times by now and the board milled quite nicely and had to rub it a little to make the traces neater.

I made a list of the components using the board components on the file:

![]({{site.baseurl}}/hello.RGB.45.png)

* x1 6 Pin Header
* x1 ATtiny 45
* x1 10K Resistor
* x1 1uF Capacitor
* x1 5V Regulator
* x1 4 Pin Header (Power)
* x1 499 Ohm Resistor
* x2 1K Resistor
* x1 RGB LED

I then collected the different components in the electronics lab, stuck them in the sketchbook and updated the google docs. The microcontroller on the board is the ATtiny 45, the same as with the board for input devices.

![]({{site.baseurl}}/sketchbook-output.jpg)

The list of the components used is as follows:

I found the soldering quite straightforward. I followed the technique that I have developed over the course of soldering different boards where I work outwards from the microcontroller.

![]({{site.baseurl}}/starting-solder-output.jpg)

I had soldered most of the components before on my other boards apart from the RGB LED, which was the only component that was polarized so has to be careful which way around I soldered it. I check the indentation on the LED to check the direction and cross-referenced the photograph provided in the documentation. Once I had the board soldered I carried out a visual inspection and added some more solder to some components so that they all looked smooth and shiny. The other component I had not soldered before was the 5V regulator which I found quite hard because it was very small and the way it sat on the traces.

![]({{site.baseurl}}/soldered-board-complete.jpg)

![]({{site.baseurl}}/hero-shot.jpg)

## Programming the Board

Last week I learnt a lot programming the input board and thought that I would find the process straightforward as my soldering has improved a lot over the course of the Fab Academy and I have picked up a lot of skills in the debugging process the past 5 months. However reaching the point of having a programmed board was easily the longest part of this week’s assignment for me. The following documentation will explain the problems I faced and how I went about solving them.

To begin with I used the Arduino to program the board. I used Neil's [<u>code</u>](http://academy.cba.mit.edu/classes/output_devices/RGB/hello.RGB.45.c) and setup the board and programmer in the Arduino IDE.

![]({{site.baseurl}}/arduino-programmer-settings-output.png)

I tried to burn to bootloader before uploading the code but received an error message.

![]({{site.baseurl}}/bootloader-error-output.png)

I was expecting that I would not be able to program the board straightaway as with electronics and programming the board there are so many points where errors can occur. As I was sure the components were in the correct location I set about using the multimeter to check the connections. I checked the connections between from the 6 pin header and the 8 connections on the microcontroller and found an error with the LED. Although there was a beep, it cut out a bit and upon visual inspection the component was not touching properly and was hovering above the board slightly. I then re-soldered the LED by using the long side of the soldering iron to heat up the pads and alternating quickly whilst pressing down with the tweezers to create a stronger connection.

I then tried to re-program the board and received the same error. (timeout thing). I searched on some forums copying the error message I had but could not find anything to solve the problem. I then checked that I had the cables connected between the 6 pin headers on my board and the Arduino the correct way around using this diagram and everything was OK. This is known as the 'Arduino icsp pinout':

![]({{site.baseurl}}/arduino-icsp-pinout.jpg)

I then returned to the multimeter to check the soldering and found a problem with the regulator so re-soldered one of the connections again to have more solder.

![]({{site.baseurl}}/multi-meter-debug.jpg)

I tried to burn to bootloader again but it still didn't work. The LED did turn blue for a brief period before sending the error message.
<br>
<iframe src="https://player.vimeo.com/video/336554578" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

I then asked the instructors what the problem might be and Eduardo explained that the board's regulator is not able to behave the way it should as the power is imbalanced. There are two ways of getting around this, to add a diode to stop the flow of current to the regulator or to add an external power supply. I chose to add external power as there was very limited space as the board was already very small.

The extra power added means that it can't be connected for too long as there is a risk of damaging the microcontroller as it can reach around 140 degrees Celsius. This means just programming the board and disconnecting the programmer and then LED will remain lit.

I then added a battery pack to the power header checking the schematic for the correct orientation. I still had the same problem so decided to use a different device to program the board. I used the AVRISP in the lab, and when I plugged it in the light turned green which was a good sign.

![]({{site.baseurl}}/programmer-green-light.jpg)

I changed the settings in the IDE to change the programmer but when I tried to burn to bootloader it still did not work.

![]({{site.baseurl}}/bootloader-error-output-2.png)

At this point I really was not sure what the problem was as I had tested the board with the multimeter various times and had tried programming the board with different programmers and added extra power. I had not tried the FABISP to program the board however so thought I would try this too.

I changed the settings in tools to select the correct programmer (USBtingISP).

![]({{site.baseurl}}/tools-fab-isp.png)

I then tried to 'Burn Bootloader' and this time it worked successfully.

![]({{site.baseurl}}/bootloader-success.png)

I then was able to upload the sketch provided in the documentation successfully and the LED lit up.
<br>
<br>
<iframe src="https://player.vimeo.com/video/336556898" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

Once the board is programmed, it no longer needs the programmer so I could remove the FABISP.
<br>
<br>
<iframe src="https://player.vimeo.com/video/336557159" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Reflection

In general I have found the electronics part of the Fab Academy the most challenging because as I mentioned earlier in the documentation there are so many places where errors can happen, from the milling, the soldering, programming and that isn't even including actually designing the board from scratch. Having said this, I have also found this part of the Fab Academy the most rewarding because making something work which has been really difficult is a nice feeling. The learning curve has been very steep but feel I have learnt a lot in terms of electronics production but there is still a lot more on the electronics design and programming side (especially regarding C code) that I have to learn about.

## Useful links

* [How RGB LEDs work](https://randomnerdtutorials.com/electronics-basics-how-do-rgb-leds-work/)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week11/) (Output Devices)

## Files

All links to the files used for this assignment can be found on the [<u>Output Devices</u>](http://academy.cba.mit.edu/classes/output_devices/) Fab Academy documentation page.

* [Neil's Code](http://academy.cba.mit.edu/classes/output_devices/RGB/hello.RGB.45.c) (Link)
* [Neil's Code]({{site.baseurl}}/RGB_neil_s_code.ino) (File)

<!-- Maybe add some further notes from the class etc and modify the code? This is true for all of the assignments. Also try to program it from the terminal! -->
