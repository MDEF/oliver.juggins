---
title: 3D Scanning & Printing
period: 20-27 February 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
The theme of this week in the Fab Academy was all about 3D scanning and printing. I had some experience 3D printing, but no experience scanning objects. There were a range of classes which covered these different areas and lots of hands on demonstrations to understand the machinery and processes of scanning and printing. The task was to design and 3D print something that could not be made subtractive and to scan an object and make it into a 3D model.

## 3D printing

For the 3D printing task I decided to make something related to the [<u>Material Driven Design</u>](https://mdef.gitlab.io/oliver.juggins/reflections/materialdrivendesign/) module that is part of the master programme. I designed and printed a mold for a small bowl / cup that I could use for the material I was developing in this class which is the spent grain waste from the beer making process. I decided to make the mold in two parts, with a 3mm gap between them so the bowl would have a thickness of 3mm. I decided to make the walls of the mold 2mm, as I had experience printing objects before and knew that this value was a good trade off in terms of strength and printing time.

![]({{site.baseurl}}/rhino.jpg)

The model is relatively simple, with one of the bowl shapes having a rectangular element in the centre of it so I could lift it once the material I am molding has dried. Once I finished the model I exported it as an stl file which is the file necessary for the 3D printing software. I also placed the rhino file in the bottom right region relative to the origin, and exported the two bowl shapes separately as this is better practice. They are then put in the same file together to be printed.

![]({{site.baseurl}}/rhinotop.jpg)

There are many processes in the Fab Lab that can achieve results in three dimensions, through an additive process and a subtractive process. My design has to be made using the 3D printer because it could not be made subtractively through using a piece of equipment like the CNC milling machine for example. This is due to the edge between the bottom of the curve in the two mold, which a router would not be able to achieve. Another reason the molds could only by made through an additive is due to the part in the middle of one of the molds which is there to help you lift the mold out of the material.

At this stage you have to select a printer in the Fab Lab to print you object. In my case the available printer was the [<u>Prusa i3 Mk3</u>](https://shop.prusa3d.com/en/3d-printers/180-original-prusa-i3-mk3-kit.html), which is supposedly one of the better printers in the lab. We had some instructions on the types of printers in the lab and types of printer in general and the Prusa i3 Mk3 is known as a FDM (Fused Deposition Modelling) printer. The printers in the Fab Lab normally print PLA and the area of the printer is 25x21x20 (x,y,z) and has a nozzle size of 0.4mm (like all of the printers in the lab) with a filament diameter of 1.75mm. I found this [<u>article</u>](https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/) which gives a good overview of the types of 3D printers and some terminology.

To prepare the stl file for printing you have to use slicing software which tells the printer how to physically print the object where there are a number of parameters to set. Most of the printers in the lab use the [<u>Ultimaker Cura</u>](https://ultimaker.com/en/products/ultimaker-cura-software) software, but the Prusa printer uses [<u>Prusa Control</u>](https://prusacontrol.org/) but are both very similar although the Prusa printer needs to recieve the file via an SD card. Mikel, the Fab Lab manager gave a run through of the Cura software and I found this [<u>Youtube tutorial</u>](https://www.youtube.com/watch?v=eUNTlb5pEWA) which runs through all of the important settings in a lot of detail. It is important to note that the parameters depend on the type of design you have and the desired quality of the print. As such, every print job will have its own specific settings but here are some general points to have in mind:

* Initial layer height needs to be thicker (0.3) so it sticks to the base.
* The layer height is a time / quality trade off with 0.3 a 'rough' quality and 0.1 a 'high' quality print.
* The line width relates to the nozzle being used (in the Fab Lab normally 0.4)
* Shell relates to the outer walls, and how thick they should be (determined by wall line count setting).
* You always need more layers on the top than the bottom, and at least two on the bottom, but five on the bottom and eight on the top is a good rule of thumb.
* In practice, infill density does not need to surpass 50%. This depends on what you are printing as higher infill means the object will be stronger.
* View layers and change with slider to see how the parameters are affecting the file for printing.
* Enable retraction ticked to avoid 'spiders web' printing situation.
* Speed depends on the material, but 60mm/s is a good rule of thumb.
* Turn on fans after first layer so the first layer has a chance to stick properly.
* Build plate adhesion is necessary for the print to stick down properly.

I used the Prusa software in the Fab Lab to prepare the file, but as an exercise did it in Cura as well to go through the process. Once you have put in all of the settings, go to layer view and hit 'prepare' in order to see how the print will be. It will tell you the amount of material required and the overall print time.

![]({{site.baseurl}}/cura.jpg)

The overall print time took around 5 hours and I was very happy with the overall quality of the print.

![]({{site.baseurl}}/printing1.jpg)

![]({{site.baseurl}}/printing2.jpg)

I then sanded the mold so that it would be more suitable for the purpose for which it was intended, and the material would not stick to it.

![]({{site.baseurl}}/moldinuse.jpg)

![]({{site.baseurl}}/moldinuse2.jpg)

In the end the mold worked well, and can be used multiple times for future casts and tests I develop with my material. I also plan to be more ambitious with the types of molds I plan to make for future projects as I know have a better understanding of the capabilities of the 3D printers in the lab.

![]({{site.baseurl}}/finalmold.jpg)

Although I only used one of the printers during this week, I used one in the first term during [Design for the Real Digital World](https://mdef.gitlab.io/oliver.juggins/reflections/design-for-the-real-digital-world/) I feel I learnt a lot more about how the different parameters affect the time and quality of the print. I plan to continue to use the different printers to see how each of them is different in terms of performance and quality and further my knowledge of this process.

## 3D scanning

There are many different ways and available pieces of software to 3D scan objects and larger scale things such as buildings and even areas of landscapes. I tried to keep things simple initially by using Autodesk Recap photo to make a 3D model of a banana using a number of photographs however problems with the software meant that this did not work.

As a result I used the Microsoft Kinect with the [<u>Skanect</u>](https://skanect.occipital.com/) software to do my 3D scan. The software is very intuitive and is simple to use as you just have to hit record and move the camera around what you would like to scan and then export the model. The hard part is the moving of the camera or the object. This has to be done at the correct distance (so that the desired scanned object is green which is about one metre) and the correct speed so that everything is scanned properly.

As I really wanted to get a scan of a banana, I tried again with the Kinect however the results were not great and think that maybe the banana was too small an object, or that the setup I had holding the banana wasn't very good and that the way I moved the banana wasn't controlled enough and me being the background probably wasn't a good idea. I put the files into Rhino to have a look at them in some closer detail, importing them as an stl.

![]({{site.baseurl}}/banana.jpg)

In order to try and achieve results of a higher quality, I scanned something larger which the Kinect is more suited to, myself. I also adapted the way that I took the scan. This time I chose to have the Kinect stationary and put it on a stool which was on a table to elevate it properly. I then slowly rotated myself trying to keep a constant speed to capture my torso

![]({{site.baseurl}}/bodyscan.png)

The result is a lot better than the scan of the banana but still has some flaws, although arguably does bear some resemblance to myself.

## Useful links

* [3D Printer Overview](https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/)
* [Cura Tutorial](https://www.youtube.com/watch?v=eUNTlb5pEWA)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week5/) (3D Scanning & Printing)

## Files

* [3D File Mold]({{site.baseurl}}/3D-file-mold.zip)
* [3D File Scan]({{site.baseurl}}/3D-body-scan.zip)
