---
title: Electronics Production
period: 13-20 February 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
<!-- include files -->
The task in this week of the Fab Academy was to make and program a PCB using the method Neil described in the class through milling and soldering. My background in electronics and soldering is very limited so I knew this would be a challenging week but was one I knew I would be able to learn a lot from and proved to be a week which was a crash course is becoming acquainted with the equipment, tools and machines in the electronics lab in the Fab Lab.

The task required milling a board that had already been designed, the FabISP. Even though this board was already designed, there were still a lot of processes to follow which included preparing the file for the milling machine, milling the board, collecting the components from the electronics lab, soldering the components onto the board and then finally programming the board. I used this [<u>tutorial</u>](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html) on the Fab Academy website on the production and programming of a FabISP incredibly useful and outlines all the steps that have to be followed to successfully make a PCB.

The first step is to download and prepare the files which look like this:

![]({{site.baseurl}}/tracesinterior.png)

The part on the right is milled first, the 'traces' using a 1/64" bit and then the outline is cut (confusingly called the 'interior' part) with a 1/32" bit. Each file has to be prepared in the [<u>Fab Modules</u>](http://fabmodules.org/) browser correctly for the different machines in the lab, where you input the type of bit that has to be used, the machine, the number of offsets the speed and the x, y and z coordinates.

![]({{site.baseurl}}/fabmods.jpg)

Once the file is prepared, you hit 'calculate' and an image is generated which describes how the board will be cut which you then have to save as an .rml file (as this is the correct file type for the machines in the Barcelona Fab Lab). This is what the image should look like before saving the file on Fab Modules:

![]({{site.baseurl}}/fabmods2.jpg)

In theory, cutting the board should not be too complicated if the settings are correct, the machines are functioning well and the files are generated in the right format. However when I cut my board I encountered many difficulties through initially a problem with the milling machine and then with the software on a different machine in the lab. As a result I got to use both of the machines which are the [<u>Roland MDX-20</u>](https://www.rolanddga.com/support/products/milling/modela-mdx-20-3d-milling-machine) and the [<u>Roland SRM-20</u>](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine).

Once the file is ready to be uploaded to the machine the following process is required to mill the board (with the software for the SRM-20):

* Select the correct mill for the part of the board being cut (1/64" or 1/32" in my case).
* Put in the mill in the machine using the hex key, but don't tighten too much.
* Using the controls on the software for the machine, set the x, y coordinates (bottom left of board in my case).
* Set the z coordinates using control, and then taking the bit out of the housing and placing gently onto surface of the board, making sure to not let it drop.
* Once all of the origin points are set, press 'cut' and delete any existing files.
* Add the file you want to cut, and press 'output'.
* The board should start cutting, creating a nice fine amount of sawdust.
* If anything looks like it is not working or the board is not cutting very well, press pause or open the cover of the machine to stop the job.

I preferred using the Roland SRM-20, this is what the panel looks like:

![]({{site.baseurl}}/srm20.png)

The first problem I encountered was that the z axis in one of the Roland SRM-20 machines in the lab was not working properly, so that when this value was set it was not accurate and the mill snapped immediately when cutting. As a result this machine was out of action in the lab and I moved onto the MDX-20.

![]({{site.baseurl}}/brokenmachine.jpg)

As a result I used the Roland MDX-20 for the second attempt as the other SRM-20 was in use. Setting up the file was the same process in Fab Modules, but setting the x, y and z coordinates was different, and was done through using a processing sketch where you placed the cursor on the screen to where you wanted the mill to go to.

![]({{site.baseurl}}/machine2screen.jpg)

![]({{site.baseurl}}/machine2.jpg)

This worked quite well, and when I sent the file to mill the 'traces' part the board was cut well.

![]({{site.baseurl}}/board01.jpg)

The problem came when I sent the file to cut the 'interior' part as when I sent it to be cut, the mill cut across the part which had just been milled. This was due to an error in the processing sketch which meant that the coordinates were not saved from the initial setup and started the milling from the last x and y location which the job finished.

![]({{site.baseurl}}/brokenboard.jpg)

![]({{site.baseurl}}/interiorwrong.jpg)

Finding out what went wrong took some time to work out with Eduardo, one of the instructors and in the end we milled a board with 1 offset as a test for speed to see what was happening. We then set the x and y coordinates by eye for the 'interior' part to cut the board out which worked OK, although was a little hard to setup.

![]({{site.baseurl}}/smallpcb.jpg)

Having only one offset meant that it would have been very difficult to solder, so I returned to the machine I used first, the Roland SRM-20 and cut another board. This time everything went smoothly, and milled and cut the board with no problems.

![]({{site.baseurl}}/rolandmachine.jpg)

![]({{site.baseurl}}/finalboard.jpg)

Now that I had the board cut I was ready to find and solder the components. As I had very limited soldering experience I thought that some practice would be a good idea and used some old test boards and components in the Fab Lab to solder and de-solder. The components were a lot smaller than I was expecting so found it quite difficult at first.

![]({{site.baseurl}}/solderpractice.jpg)

Once I was a little more confident, I sourced the components using the list on the [<u>tutorial</u>](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html) and stuck them in my sketchbook to not loose them and keep track of them as they all looked very similar and very small. Once this is done it is important to update the document which keeps track of all of the components so the Fab Lab knows when to order more when supplies are running low. This is a list of the components that were required:

* 1x - ATTiny 44 microcontroller
* 1x - Capacitor 1uF
* 2x - Capacitor 10 pF
* 2x - Resistor 100 ohm
* 1x - Resistor 499 ohm
* 1x - Resistor 1K ohm
* 1x - Resistor 10K
* 1x - 6 pin header
* 1x - USB connector
* 2x - jumpers - 0 ohm resistors
* 1x - Crystal 20MHz
* 2x - Zener Diode 3.3 V
* 1x - USB mini cable
* 1x - ribbon cable
* 2x - 6 pin connectors

![]({{site.baseurl}}/soldering.jpg)

In order to solder I used one of the stands to place my PCB to keep it still, which is really important as otherwise it would be very difficult to be accurate. I struggled and got frustrated with the first couple of components but got into a rhythm. I found it hard because it felt like I needed two right hands, as both hands needed to do tasks which required accuracy. In order to overcome this I carried out the soldering in stages, using the following method:

* Apply a little solder to the component, holding the component in my right hand.
* Then swap the soldering iron for the tweezer with my right hand and pick up the component.
* Place the component near the correct location on the PCB, and using the soldering iron in my left hand, heat the solder attached to the component.
* Place component on board, and solder the remaining parts of the component to the board.

It took about an hour to get to this stage of the soldering process. The diodes and USB connector is missing which I have to add, and then can program the board.

![]({{site.baseurl}}/finalpcb.jpg)

Adding the final components was the most difficult part, especially the USB connector.

![]({{site.baseurl}}/finalboard2.jpg)

Once the board was soldered I had to program the board as required. I carried out the 'smoke test' and everything was OK so I proceeded to program the board using [<u>the tutorial</u>](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html#install) from the Fab Academy documentation for this week's task. I am using a Mac so carried out the steps and followed the guidelines downloading the firmware and Crosspack AVR software.

Once I had unzipped the firmware and was going through the steps putting the code into the terminal I received an error message when carrying out the 'make hex' command.

![]({{site.baseurl}}/failed_programming.png)

As it was not working I knew something must be wrong with my PCB so returned back to the electronics lab to start the debugging process. I had never done this before so needed some guidance about how to use the multimeter to check the connections on my board. Going through this process I realised the following components and connections were not connected properly:

* PA 6 (on microcontroller)
* PA 4 (on microcontroller)
* PA 3 (on microcontroller)
* Resistor 2
* Resistor 4

I then spent some time re-soldering these connections, making sure they were smooth and shiny until I checked them with the multimeter and everything seemed ok.

I then connected the FabISP to my laptop again to try and program it again and this time was successful.

![]({{site.baseurl}}/successprogramming.png)

## Useful links

* [FabISP Production and Programming tutorial](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html)
* [Fab Modules](http://fabmodules.org/)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week4/) (Electronics Production)
