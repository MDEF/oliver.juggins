#02. BIOLOGY ZERO

Biology Zero was introduced in the context of the Fab Lab movement: what has been happening over the past decade with the democratization and accessibility of technology and digital fabrication has started to happen in the world of biology. The week was therefore about providing some of the fundamental knowledge, both theoretical and practical as a solid starting point for being able to carry out further work and experimentation in the field.

At the beginning of the week my knowledge of biology was very limited, having forgotten most of what I had learnt from school. The intensity of Biology Zero and the amount of information consumed was vast and I ended the week with knowledge of the concepts and processes in the key areas of Biology. Due to the volume of information consumed and to try and present it as coherently as possible I have chosen to loosely adopt the format of a scientific journal with the following sections:

![](contents.jpg)

[comment]: <> (maybe change all sub-headings)

***

### 1.0 Hypothesis

**1.1 Context**

In order to understand the scientific method, and the principles behind adopting a scientific methodology to a hypothesis, the task called for presenting a hypothesis from a theme of individual interest with some relation to Biology.

The Bio Academy’s motto of ‘How to Grow (almost) Anything’, along with my interest in the future of architecture were the motivations behind devising my hypothesis. How buildings will be conceived and constructed in the coming decades will develop and become radically different to how they are constructed at present, and I wanted to explore this theme through posing the hypothesis:

*You can plant a building like you plant a seed.*

**1.2 Process**

I began thinking of the possibility of buildings becoming living organisms through the work and research of Thora H. Arnardottir, an IaaC alumnus currently carrying out a Ph.D at Newcastle University. Her research investigates the potential of living materials which led me to imagine a synthesis of different materials which could form an entire building.

The idea of a seed containing the necessary information for a building to grow converts buildings as something which is static, into something that is constantly moving. It also changes what we would think of as a ‘finished’ building. It is not uncommon now for buildings’ functions to evolve over time. A prime example being the IaaC campus (industrial to educational).

***

### 2.0 Methodology

**2.1 Context**

In order to test this hypothesis, an experimental method will be adopted. Before setting out which steps will be taken in order to being my evaluation, it is necessary to highlight the fundamental elements of a building which will be taken into consideration. Rem Koolhaus identified the elements of architecture during his role as curator of the 2014 Venice Biennale under the theme *Fundamentals* as:


* Floor
* Ceiling
* Roof
* Door
* Wall
* Stair
* Toilet
* Window
* Façade
* Balcony
* Corridor
* Fireplace
* Ramp
* Elevator
* Escalator

Whether or not these elements would be present in an architecture which is grown is unknown, but it provides a useful starting point for thinking about what to take into consideration for devising my methodology. Further points of consideration would include the building’s structure, thermal attributes and material choice. For the scope of this study however I will be mainly focusing on the growth of building’s structure.

**2.2 Method**

- Investigate possible materials for buildings primary and secondary structure.
- Create list of 5 possible materials, and for each material find out the following:
    - Structural efficiency
    - Availability in different continents
    - Cost
    - Environmental impact
    - Traditional life cycle
- Analyse results and select first material for in-depth analysis for suitability of structure.
- Identify bamboo as first material for structural analysis.
- Test the elasticity and breaking point of bamboo.
- Input data into computational modelling using parametric design software such as Rhino and Grasshopper.
- Debug Grasshopper scripts and trial different forms adopting principles of biomimicry.
- Develop computational models simulating growth of structure.
- Using simulations test for torque and redundancy in structure and increase structural efficiency.
- Provide 5 realistic structural options and build a series of 1:1, 1:5 and 1:10 prototypes and select most suitable structural system.
- Grow bamboo seeds and analyse growth using the following parameters:
    - Amount of water
    - Level of sunlight
    - Soil type
- Enhance growth through use of organic fertilizers.
- Study genetic make up of bamboo.
- Identify gene or genetic material which relates to strength.
- Using CRISPR-Cas9 edit the DNA of bamboo to increase elasticity and strength.
- Share findings and research on open source platform to enable others to carry out further research and development.

**2.3 Reflection**

The above points would just be the beginning of the exploration and attempt to prove / disprove the above hypothesis. Further steps would include development of computational models latest programming languages in the field of synthetic biology which appears to be advancing at a rapid pace.

The results of testing a hypothesis should open up new avenues of enquiry. Exploring the idea of buildings growing without testing the hypothesis has already made me think of more questions such as if a building is grown, is it alive, and does it ever stop growing? Furthermore if a building is grown, could an entire city be grown?

The notion of buildings being able to move is not a new one. Archigram, a radical collective formed by a number of architects including Peter Cook and Ron Herron challenged this idea through a number of speculative projects, for example *Walking City*.

![](walking.jpg)

*Archigram, Walking City (Project 064), 1964. Image Source Deutsches Architekturmuseum*

**2.4 References**

<http://oma.eu/projects/venice-biennale-2014-fundamentals>

The Visions of Ron Herron (Architectural Monographs No 38) by Reyner Banham

***

### 3.0 Reflection

### 3.1 Introduction

As set out previously, I have broken down some different themes explored during the week into sections as a way of classification. Because everything was so new to me, this felt like the best way for me to process the information taken in from the week.

My reflections consists of a series of the topics covered in the week which I felt important to communicate, were of particular interest to me and led to further research and exploration or I had a particular personal response to. These interpretations are driven by an ongoing interest set-out in the MDEF Bootcamp of the democratization and accessibility of information. At times throughout the week I found myself frustrated, unable to understand a concept or finding specific scientific language a barrier to my understanding as well as diagrams that appeared abstract and hard-to-read. As a result my reflections consist of a mixture of attempts to relay some key concepts and information back in a more relatable way for others, or in a way I thought of a topic to make it understandable for myself.

Due to the difficulty I had understand some of the diagrams used, I began using drawing and graphic design as a way of coming to terms and communicating a concept which was new to me during the week and making science more accessible. As I started producing these diagrams and images I imagined them coming together to form a magazine, and thought imagining a scientific journal in the format of a magazine ties in with the idea of making scientific knowledge more accessible in general.

I will cover each topic of interest with the responses, which will be followed by the responses combined in a fictional magazine *Biology Zero*.

![](cover.jpg)

***

### 3.2 Starting from Zero

**3.2.1 Context**

This passage on the opening page from Yuval Noah Harari’s *Sapiens: A Brief History of Humankind* contextualises the areas of biology I will be discussing in the following sections of reflections:

*"About 13.5 billion years ago, matter, energy, time and space came into being in what is known as the Big Bang. The story of these fundamental features of our universe is called physics.*

*About 300,000 years after their appearance, matter and energy started to coalesce into complex structures, called atoms, which then combined into molecules. The story of atoms, molecules and their interactions is called chemistry.*

*About 3.8 billion years ago, on a planet called Earth, certain molecules combined to form particularly large and intricate structures called organisms. The story of organisms is called biology.*

*About 70,000 years ago, organisms belonging to the species Homo Sapiens started to form even more elaborate structures called culture. The subsequent development of these human cultures is called history."*

(Harari, 2011)

**3.2.2 Reflection**

The diagram below page visualises this and highlights the transitions that took place originating from the Big Bang, where life began, until the beginning of culture:

![](zero.jpg)

***

### 3.3 Synthetic Biology

**3.3.1 Context**

Synthetic biology is an interdisciplinary and rapidly changing area that encompasses applying engineering principles to biology. It involves designing and fabricating biological systems and elements that do not currently exist in the natural world.

**3.3.2 Reflection**

Although I had heard the term synthetic biology before, I knew very little about this field and the introduction during the class led me to some further research. I came across a company which works at the intersection of biology and artificial intelligence which are two fields I had no idea were being connected. This company is called LabGenius, based in London and their profile is summarized below:

![](labgenius.jpg)

**3.3.3 References**

<https://medium.com/@james_field>

<https://www.labgeni.us/>

[What if machines could engineer life?](https://www.youtube.com/watch?v=FNK8ii35sHQ)

***

### 3.4 CRISP-Cas9

**3.4.1 Context**

CRISPR-Cas9 is technology that allows the removable and editing of parts of the genome through adding or altering sections of the DNA sequence. It is cheaper, faster and more accurate than previous methods of editing DNA and the potential applications are vast.

**3.4.2 Reflection**

There were multiple discussions in classes about ethical issues throughout the week, genetic modification and engineering a notable example. I was interested in the potential of this technology and where it might lead in the coming decades. I began imagining a future where one could improve any aspect of their being such as intelligence, strength and so on. In an interview between Sam Harris and Jennifer A. Doudna, a researcher internationally recognised as an expert on CRISPR biology and genome editing, they discuss  such a scenario. Whilst it may be a long way off being able to increase intelligence using CRISPR, as Doudna argues, I believe debate should be happening about this technology so that the general public have some level understanding before the technology were to be available to have open discussions about regulation.

In this future scenario of being able to increase intelligence I thought about how CRISPR-Cas9 would be advertised to the masses and turned to the iconic apple advertisement for inspiration, reworking their *Think Different* slogan:

![](think.jpg)

**3.4.3 References**

<https://samharris.org/podcasts/humanity-2-0/>

***

### 3.5 Material-Driven Design

**3.5.1 Context**

A talk given by Mette Bak-Andersen of the Material Design Lab at KEA challenges traditional models of design and production through advocating material experimentation and exploration. Mette’s research lies between biology, design and education and argues for materials exploration to be brought back into design education but also for the gap between research and industry to be reduced in terms of new and more sustainable materials.

**3.5.2 Reflection**

My first reaction to Mette’s presentation was that I did not entirely agree with the state of design education in terms of material exploration. In my own undergraduate studies making physical models and exploring materials were always encouraged to be an integral part of the design process, and when visiting graduation shows at various universities I believe there is a lot of research into new sustainable materials.

However what Mette discussed about the design process in terms of industry really resonated with me. She argued that the focus on designing forms with no consideration of material choice is resulting in designers losing their understanding of how a material works and responds. The increase in CAD and 3D modelling provides some explanation as to why this has happened. A revisiting of an analogue way of working with materials in tandem with 3D modelling is required as well a more experimental approach. This is important not just for the increased use of sustainable materials but for the possibility of the unexpected to occur when working with a material directly.

For such a shift in the design profession to happen, a new approach is needed. I thought that the way a child plays might provide a new methodology for designers to experiment with materials:

![](material.jpg)

**3.5.3 References / Further information**

<http://www.rehogar.net/>

<https://www.instituteofmaking.org.uk/>

<http://www.aeropowder.com/>

[The Secret Life of Landfill: A Rubbish History, BBC](https://www.bbc.co.uk/programmes/b0bgpc2f)

***

### 3.6 The Scientific Method

**3.6.1 Context**

The scientific method is a method which has been devised, applying deductive logic in an attempt to reach truth in a rigorous manner.

When using the scientific method to answer a hypothesis, specificity is vital and the starting point should be asking a yes or no question.

**3.6.2 Reflection**

The way that the scientific method is presented as a process appears to be cyclical, with new findings being the starting point for a new hypothesis or further investigation. My interpretation of the scientific method is that a hypothesis is more like the start of a reaction, which could be explored in multiple directions once the initial hypothesis is investigated. The diagram below attempts to visualise this:

![](method.jpg)

***

### 3.7 Metabolism

**3.7.1 Context**

Metabolism encompasses all of the chemical reactions and processes that happen within a living cell that happen for life. For humans the simplified process of metabolism can be broken down into glycolysis, the citric acid cycle and the electron transport chain.

**3.7.2 Reflection**

Whilst it may be possible to break down metabolism into some major processes, this is far from the reality of what is really happening in the body. There are in fact many process happening simultaneously in a very complicated system.

I think there is a comparison between circuit boards found in electronic items and human metabolism. I was looking at my Arduino board and the PCB layout reminded me of the human metabolic processes diagram due to the complexity, number of pathways and routes, and in the same way a break in a circuit board would affect a component, a break in a metabolic pathway would negatively impact the human body.

![](metabolism.jpg)

***

### 3.8 Proteins: Form and Function

**3.8.1 Context**

The biochemistry class covered the four functional groups in relation to genetics and molecular biology which include: lipids (form membranes),  proteins (provide structure), nucleic acids (information storage) and carbohydrates (energy).

Much of the discussion focused on proteins and the way in which amino acids come together to form proteins, and how the shape and movement of a protein has a relationship to its function.

**3.8.2 Reflection**

The relationship between form and function in proteins made me connect architecture to this topic due to the well known phrase ‘form (ever) follows function’ coined by the American architect Louis Sullivan which highlights one of the key principles of the modern movement. The diagram below shows how an amino acid comes together to form a protein alongside a building I believe reflects the principle of form follows function,  the Engineering Building at Leicester University by James Stirling and James Gowan:

![](function.jpg)

***

### 3.9 Cellular Biology

**3.9.1 Context**

The study of a cell’s structure and function falls under the area of study of cellular biology under the premise that cells are the building blocks of life. Understanding how a cell enables functions leads to more knowledge about the organism which the cells compose.

**3.9.2 Reflection**

There have been previous comparisons made between cells and cities, with the different activities in the city reflecting the different roles of the elements of a cell. In some ways I agree with this comparison, but in other ways I don’t as although some cities are planned they are often not and what is meant to work well (transport for example) may not. As a result a cell is more like an ‘ideal’ city, of which there have been many proposed over the years by various architects and planners. One example is Ebenezer Howard’s Garden Cities plan, which is formed in concentric rings where the city's main functions are set up around connected nodes. Below is an adapted diagram of Howard’s plan for a city with the parts of a cell labelled:

![](gardenc.jpg)

***

<iframe style="width:525px; height:371px;" src="//e.issuu.com/embed.html#35105822/65279552" frameborder="0" allowfullscreen></iframe>

***

### 4.0 Conclusion

Biology zero was about opening up the world which cannot be seen and making it more understandable and relatable to us as a group of designers and how it could be useful to us. I was curious as to how the week would pan out, what would be covered and how this connection to non-scientists would be made. I am still processing the information from the classes, but believe I now have a good base knowledge and the tools to be able to further my understanding in both theoretical and practical terms, and know where to look and who to ask to carry out my own experiments and research.

The practical experiments that were carried out during the week did not fall into the scope of this writing, but it is important to highlight that they were designed and presented in a way that could be carried out at home using ‘hacked’ equipment or cheaper alternatives to expensive scientific apparatus. I did not know to what extent the open source mentality had transferred into the field of biology and the experiments carried out in the week were prime examples of DIY biology.

There was also some interaction with some IaaC alumni where we learnt about their current work and interests This made the link between biology and design a lot more relatable and gave some useful background into the kind of work currently being carried out in this intersection of disciplines. This idea is a key component of the masters and being open and curious in the way I was during Biology Zero is an attitude I will take forward to the coming weeks.

![](experiments.jpg)

*Some of the experiments carried out during the week including: cultivating a medium, using a spectrometer, and using a mini PCR*

### Useful links and Resources

<http://igem.org/>

<https://diybio.org/>

<http://www.hackteria.org/>

<https://waag.org/en/home>

<http://biocurious.org/>

<https://biologigaragen.org/>

<https://www.genspace.org>

<https://lapaillasse.org/>

<http://biohackacademy.github.io/>

<http://bio.academany.org/>

<http://www.thelabrat.com/>
