#01. MDEF BOOTCAMP

Following the first week of the MDEF ‘Bootcamp’ I feel I am beginning to settle into life at IAAC. The week was varied, and was focused around getting to know the other students and their interests, tutors and their areas of expertise, as well as the surrounding area and ourselves. There was a range of activities including the introduction to some tools such as GitLab and GitHub, visits to local studios in the area, and the introduction of ways of thinking such as the importance of documentation to help set up the rest of the year. I will discuss these activities, along with insights, what I learned and how I think this is relevant to me.

[comment]: <> (maybe change all sub-headings)

***

**Reflections: My Future Self**

Day 1 was about discovering the backgrounds and areas of interests of everyone on the course and subsequently forming groups of shared interests. The wide ranging individual interests was reflected in the diversity of the group and being within a group of people from such different backgrounds was totally new to me in both an academic and professional setting. I found it interesting to see a number of common themes developing throughout the workshop and lots of overlap in ideas and interests. A key motivation for applying for the course was the chance to work with a diverse group of people so to learn about everyone right from the beginning of the course was really useful.

The interest group I am part of now is called ‘Emancipatory Processes’, the core idea being that we have a shared interest of creating platforms which empower people in some way whether it is through involving them in the design process or providing an educational service among others. These groups might serve as a support system as we progress our projects, and leave the possibility of collaboration open.

How do I see myself in 10 years?

A question for consideration as part of the session was to think about ourselves in 10 years time. I struggled with this question, and gave it more thought throughout the week. I realised that in order to think about myself 10 years from now needed to reflect briefly on my past and present.

I begin MDEF with an architectural background and a varied experience of working in the physical world, from buildings, interiors, products and installations. This experience has been shaped by both my academic and professional experiences not only in the UK where I am from, but also in Central and South America. I always had the feeling I did not want to only design buildings and working in ‘traditional’ architecture / design studios confirmed this belief. Exposure to new methodologies, and context has brought me to IAAC, in the present.

An important and formative experience was taking part in a design / build project in Mexico where members of the community were involved in the process. This sparked an interest in participatory design and led to further academic research and professional experience. Recent reading has led me to the idea of ‘Open Source Architecture’ (a book by Carlo Ratti and a number of other contributors), and to consider the (possible) futures of architects and designers.

Anxieties, exploration and subsequent excitement about new technologies have also guided my interests. At times over the past couple of years I have held a pessimistic view on the implementation of new technologies in society, and I think it is easy to feel this way given the ever increasing rate of change. It was through finding out about the ideas and work of Kevin Kelly which had a large impact on developing my thinking. The notion of having a deep and meaningful knowledge of technologies to be able to then shape them to use to one's advantage (to control rather than be controlled) is an idea that resonates with me strongly.

Articulating my current areas of interest has been an important process for me, and these are more easily represented through images rather than text alone. They serve as a record for myself trying to make connections between my thoughts. It something I aim to refer back to, reflect on and develop throughout the year:

<img
 id="zoom_05" src="../greymap1small.jpg" data-zoom-image="../greymap1.jpg"
 />

[comment]: <> (This should be in header but working out where it should go)

<script src="../../../js/jquery-3.3.1.js">
</script>

<script src="../../../js/jquery.elevateZoom-3.0.8.min.js">
</script>


<script>
$("#zoom_05").elevateZoom({
  zoomType				: "inner",
  cursor: "crosshair"
});
</script>

*Initial mapping of all interests and their connections. Hover mouse over image to see map in more detail.*

The initial map served as a way of establishing lots of interests and possible lines of enquiry for the year. I felt it was necessary to focus my thinking, identifying keys areas of interest and some relevant processes:

![](greymap2.jpg)

*In-depth look at primary interests and how they relate.*

I discovered from these mapping exercises is that my two primary interests strongly relate to one another. Participatory design and making technology more accessible both have the common goal of 'user empowerment' and starting point of 'engagement'. It has also highlighted the need to define my terms more clearly and something I will have in mind moving forward.

Having established current interests broadly and in a more focused manner, I can begin to reflect on how I see myself in 10 years.

I see myself situated between the designing and building spaces, objects, platforms, using new and ever-changing methodologies. In this scenario I am imagining the architect as a designer of frameworks to be interpreted and adapted by future users of architecture. I am therefore working within a space connecting what is currently often referred to as the 'user' to their own environment, for them to take ownership of where they live. The implications of virtual and augmented reality on what architects do could be great, possibly changing and increasing their responsibilities as facilitators of the design of virtual worlds as well as the physical.

I also see myself working in an exploratory manner, seeking to understand new technologies for my own curiosity and understanding but also communicating with others. As such I would like to have involvement and connections to educational institutions where this kind of work is potentially encouraged and explored more than in industry.

***

**Building my narrative**

Something which has been made clear from the beginning of the course is the importance of documenting work carried out. There was an introduction to GitHub and GitLab and a workshop based around setting up an individual website and space for this documentation. The key concepts of these tools were explained and I learned that they are essential for collaborative working and open source projects. As I will be working with others not only during the course but also professionally, becoming familiar and confident with GitHub and GitLab will be essential. The day consisted of a practical workshop which provided the knowledge to get a website up and running, but also why I have a digital space in the first place and why I should be documenting.

In addition to learning the concepts behind of these tools I feel my technical knowledge improved, through using the computer’s terminal, adding content to my website and the beginnings of some personalisation using markdown and basic html. This week was my first exploration into web development so the learning curve has been steep, with trial and error, mistakes and frustrations. Having said that I was successfully able to install a library and create a bit of interaction with the webpage through a zoomable image. The process of reaching this point consisted of identifying what I wanted the webpage to do (mouse hover with zoomable sections of image), researching how this is possible (forums, youtube videos), and a lot of testing and trial and error. I identified early on I needed to incorporate javascript and a plugin called jquery into the webpage, and this proved to be the main stumbling block. The breakthrough came when I learnt how to install javascript into the website (into the .yml section) which meant I could then could successfully use the jquery code in the different pages of my website, and achieve the desired effect with my image.

This was really important to me as the image otherwise would not have been readable, and therefore not useful to whoever is on the webpage. Another important reason was because I wanted to try and reflect my areas of interest in the design of the website through including some kind of interaction. As my programming skills develop I hope to develop this idea of being able to participate and interact with my website and its content.

During the workshop Mariana's presentation communicated not only the importance of documentation, but the importance of how to do this and to view it as something larger than just a personal website, but as a way of adding transparency and accessibility to information. Therefore being aware and critical of any information I come across is crucial as well as considering how I present it as a part of my story. This includes thinking about meta-information, the importance of referencing and investigating sources thoroughly. Mariana also spoke about the need to think about how information is translated between humans, from human to machine and machine to machine. Starting to build my website and has made me think about the precision needed when a human interacts with a machine.

***

**Exploring Poblenou**

Through the City Safari workshop (a tour of Poblenou) I become more familiar with Poblenou, and got a better understanding of the immediate context in which IAAC is situated. The activity consisted of a tour in which there were visits to different organisations and design studios. This was followed by a ‘scavenger hunt’, making the most of the opportunity to collect rubbish left out on the streets on Thursday nights in the area. The idea is to be able to use the found materials to start building a more personalised studio space in our space in IAAC. I was able to find a chair (in surprisingly good condition), a brick and some blue foam, which I thought could be used as an acoustic wall panel.

Before the City Safari there was an introduction to the Fab Lab and Fab Cities project and their history, core ideas and the next stages of development through a presentation given by Tomas. A circular economy model underpins the Fab Cities initiative where materials (or atoms) stay within cities and information (or data) is what is transferred out of cities. The idea of thinking about multiple scales adopting a holistic approach was also presented. An interesting discussion followed, and what I found really interesting to think about are the implications of producing everything in this new model, how this will affect supply chains, and how the identity and character of cities may change and develop as a result.

Poblenou is a testing ground for the Fab City prototype so understanding the area should help understand the project more thoroughly. The ‘City Safari’ tour that followed exposed a fraction of the wealth of organisations active in the area including an upcycling furniture design studio (TransfoLAB) and a community bike repair workshop (Biciclot) among others. A common theme with these organisations is their involvement and connection to the community. Learning about the types of organisations in the area for possible collaborations, and have knowledge about organisations that undertake projects involving the community will be extremely valuable as I develop my project. Starting to think about how links can be made between organisations will also be important for how a project to make a larger impact.

I was interested in keeping a record of the tour in some way, so I used a geo tracking app to record the tour. I made a collage as a record of the City Safari using data but also some of the less tangible qualities of Poblenou. The landscape architect James Corner and his various mapping studies have been the inspiration behind this exercise. The image shows photographs of points of interest and observations, with the route walked in red, the city grid in the background as well as a speed chart.

![](collage.jpg)

Selected Data:

* Total length: 1.79 miles
* Maximum speed: 6.83 mph
* Average speed: 0.72 mph
* Altitude difference: 84 ft

Discovering and learning about the area during the tour was a really useful exercise and a good starting point for getting to know the immediate context and to help place our space as part of a network which can be expanded.

***

**Poblenou Reflections**

The first week of MDEF ended reflecting on the experience of the city safari and the week as a whole. In the common interest groups we discussed our expectations, impressions and some reflections / insights about Poblenou and the first week. Having to be forced to think why certain activities were done made them even more valuable and we discussed how our expectations of Poblenou were quite different from reality and how already it is really encouraging as a group we are sharing knowledge.

The workshop began with Oscar giving a presentation about his work and the importance of relating a project to its context really came across, highlighting the significance of the previous day’s activities. As a designer with expertise with wearables and technology, it was interesting to hear about his work as it is an area of design I knew very little about. Embracing the glitches created when the digital world meets textiles (as demonstrated in the Textales project) was an idea I found really interesting. Trying to make the most of the unexpected is an attitude I will try and bring to my own work as well as getting out of comfort zones. With his work the importance of bringing the prototype onto the body, and testing it in the real world was emphasized, as it is impossible to imagine results, and that’s where unexpected outcomes are made possible.

I have already discussed some insights in relation to the different activities throughout the week, but for my own reference I am listing them as follows for something to keep in mind throughout the year:

* Importance of placing a project in the real world, with a strong relationship to the immediate and local context.
* Design interventions.
* Consider multiple scales.
* Document everything, and think about documentation as something to share, not only a personal record.
* Maintain a critical approach.
* Make use of all available resources.

The workshop ended with an exercise looking at map of Poblenou where places of interest have started to be highlighted. I have identified Trinijove foundation and a number of community groups as potential places of interest to visit. The map will serve as reference for the group to look at and organisations to potentially collaborate with. I think a digital version would also be useful, where people could highlight and mark any groups they are working with, with any findings and notes.

To bring the reflections on the first week to a close I have some final comments on my overall learning process. This is a process that is always evolving and developing through experience and interactions with people but can be summarized as:

* Exchanging ideas, sharing of knowledge.
* Trial and error, making mistakes.
* Chance occurrences, the unexpected.
* Having preconceptions challenged.
* Being open to change of outcomes.

This process is relevant to me as a designer and my work as I believe that now I am entering a phase where I will be constantly learning. Being aware of how I learn will be important to move forward and be accepting of new ideas, be able to defend my own and be challenged.

***

**Dérive Poblenou**

I have spent a week getting to know Poblenou and although this is not a lot of time I thought I would have had a better understanding of the area, especially given the tour during the week. I am already using the same route to walk to IAAC, and my reliance on google maps has stopped me from taking in my surroundings. To address this, I carried out a ‘dérive’, a concept from the Situationists whereby you are encouraged to explore the city in an unplanned manner, and be led by whatever you encounter. I decided to let myself get lost in Poblenou for an hour, with one simple rule of not looking at my phone. I took photos and a series of graphite rubbings from surrounding buildings and pavings. It was interesting to wander the area during the weekend where there was very little to be attracted in terms of activity which made me focus a lot on the buildings and materials.

![](site1.jpg)

*A collection of photographs taken during walk.*

![](rubbings.jpg)

*A series of rubbings from walls and the ground around Poblenou.*

![](derive.jpg)

*Recording dérive through collage, using same visual language as previous mapping exercise.*

Selected data:

* Total length: 2.18 miles
* Maximum speed: 6.21 mph
* Average speed: 2.86 mph
* Altitude difference: 50 ft

***

**Life outside of MDEF**

At the weekend there was the opportunity to visit the IAAC campus in Valldaura and the Green Fab Lab. It was nice to get out of the city and learn about the projects happening on the campus and talk to students that were living there. I plan to return to see more of the projects and how the students living their get on with the aim of achieving self-sufficient living.

![](valldaura.jpg)

Other activities included visiting the auto-màtic exhbition at Arts Santa Mònica, which explored the link between drawing and robotics, and attending the roundtable discussion organised by the MRAC programme where the construction industry and robotics was discussed. I aim to try and keep up with activities happening within IAAC to learn from the other courses and disciplines.

![](extrac.jpg)

Outside of the context of the school I visited the Rehogar exhibition at Espai Zero. Many of the organisations will provide good inspiration and resources for my own projects.

![](rehogar.jpg)

***

**References**

For my reference here are a collection of books which have informed some of my current thinking, and some others I wish to read which will be relevant for future workshops.

![](books.jpg)
